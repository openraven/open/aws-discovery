/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.cloudsearch;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.cloudsearch.model.DomainStatus;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = CloudSearchDomain.TABLE_NAME)
public class CloudSearchDomain extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::CloudSearch::Domain";
    protected static final String TABLE_NAME = "awscloudsearchdomain";

    public CloudSearchDomain() {
        this.resourceType = RESOURCE_TYPE;

    }


    public CloudSearchDomain(String account, String regionId, DomainStatus domain) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = domain.arn();
        this.resourceId = domain.domainId();
        this.resourceName = domain.domainName();
        this.configuration = PayloadUtils.update(domain);
        this.resourceType = RESOURCE_TYPE;
    }
}

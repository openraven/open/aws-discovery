/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import com.fasterxml.jackson.databind.node.NullNode;
import io.openraven.data.interfaces.AWSResource;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.regions.RegionMetadata;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = RegionResource.TABLE_NAME)
public class RegionResource extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Region";
    protected static final String TABLE_NAME = "awsregion";

    @SuppressWarnings("unused")
    public RegionResource() {
        this.resourceType = RESOURCE_TYPE;
    }


    public RegionResource(String accountId, Region region) {
        this.arn = "arn:aws::%s:%s".formatted(region.id(), accountId);
        this.awsAccountId = accountId;
        this.awsRegion = region.id();
        this.resourceId = region.id();
        final RegionMetadata metadata = region.metadata();
        if (metadata != null) {
            this.resourceName = metadata.description();
        }
        this.resourceType = RegionResource.RESOURCE_TYPE;
        this.configuration = NullNode.instance;
    }
}

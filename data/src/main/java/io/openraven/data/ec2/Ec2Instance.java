/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.Reservation;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = Ec2Instance.TABLE_NAME)
public class Ec2Instance extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2::Instance";
    protected static final String TABLE_NAME = "awsec2instance";

    @SuppressWarnings("unused")
    public Ec2Instance() {
        this.resourceType = RESOURCE_TYPE;
    }


    public Ec2Instance(String accountId, String region, Instance instance, Reservation reservation) {
        this.resourceName = instance.instanceId();
        this.resourceId = instance.instanceId();
        this.awsRegion = region;
        this.awsAccountId = accountId;
        this.resourceType = RESOURCE_TYPE;
        this.arn = "arn:aws:ec2:%s:%s:instance/%s"
                .formatted(region, reservation.ownerId(), instance.instanceId());
        this.configuration = PayloadUtils.update(instance);
        this.createdIso = instance.launchTime();
    }

}

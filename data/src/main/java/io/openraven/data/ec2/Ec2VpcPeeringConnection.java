/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.VpcPeeringConnection;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = Ec2VpcPeeringConnection.TABLE_NAME)
public class Ec2VpcPeeringConnection extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2::VPCPeeringConnection";
    protected static final String TABLE_NAME = "awsec2vpcpeeringconnection";

    @SuppressWarnings("unused")
    public Ec2VpcPeeringConnection() {
        this.resourceType = RESOURCE_TYPE;
    }


    public Ec2VpcPeeringConnection(String accountId, String region, VpcPeeringConnection vpcConnection) {
        this.arn = "arn:aws:ec2:%s:%s:vpc-peering-connection/%s".formatted(region, accountId, vpcConnection.vpcPeeringConnectionId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = vpcConnection.vpcPeeringConnectionId();
        this.resourceName = vpcConnection.vpcPeeringConnectionId();
        this.resourceType = Ec2VpcPeeringConnection.RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(vpcConnection);
    }
}

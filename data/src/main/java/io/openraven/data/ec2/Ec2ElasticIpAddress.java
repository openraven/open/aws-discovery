/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.Address;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = Ec2ElasticIpAddress.TABLE_NAME)
public class Ec2ElasticIpAddress extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2::EIP";
    protected static final String TABLE_NAME = "awsec2eip";

    @SuppressWarnings("unused")
    public Ec2ElasticIpAddress() {
        this.resourceType = RESOURCE_TYPE;
    }


    public Ec2ElasticIpAddress(String accountId, String region, Address address) {
        this.arn = "arn:aws:ec2:%s:%s:eip-allocation/%s".formatted(region, accountId, address.allocationId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = address.allocationId();
        this.resourceName = address.publicIp();
        this.resourceType = Ec2ElasticIpAddress.RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(address);
    }
}

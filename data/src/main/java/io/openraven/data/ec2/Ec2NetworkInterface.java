/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.NetworkInterface;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = Ec2NetworkInterface.TABLE_NAME)
public class Ec2NetworkInterface extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2::NetworkInterface";
    protected static final String TABLE_NAME = "awsec2networkinterface";

    @SuppressWarnings("unused")
    public Ec2NetworkInterface() {
        this.resourceType = RESOURCE_TYPE;
    }


    public Ec2NetworkInterface(String accountId, String region, NetworkInterface networkInterface) {
        this.arn = "arn:aws:ec2:%s:%s:network-interface/%s".formatted(region, accountId,
                networkInterface.networkInterfaceId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = networkInterface.networkInterfaceId();
        this.resourceName = networkInterface.networkInterfaceId();
        this.resourceType = RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(networkInterface);
    }
}

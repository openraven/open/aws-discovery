/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.Vpc;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = Ec2VpcResource.TABLE_NAME)
public class Ec2VpcResource extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2::VPC";
    protected static final String TABLE_NAME = "awsec2vpc";

    @SuppressWarnings("unused")
    public Ec2VpcResource() {
        this.resourceType = RESOURCE_TYPE;
    }


    public Ec2VpcResource(String accountId, String region, Vpc vpc) {
        this.arn = "arn:aws:ec2:%s:%s:vpc/%s".formatted(region, accountId, vpc.vpcId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = vpc.vpcId();
        this.resourceName = vpc.vpcId();
        this.resourceType = Ec2VpcResource.RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(vpc);
    }
}

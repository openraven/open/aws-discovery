/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.SecurityGroup;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EC2SecurityGroup.TABLE_NAME)
public class EC2SecurityGroup extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2::SecurityGroup";
    protected static final String TABLE_NAME = "awsec2securitygroup";

    @SuppressWarnings("unused")
    public EC2SecurityGroup() {
        this.resourceType = EC2SecurityGroup.RESOURCE_TYPE;
    }

    public EC2SecurityGroup(String accountId, String region, SecurityGroup securityGroup) {
        this.arn = "arn:aws:ec2:%s:%s:security-group/%s".formatted(region, accountId,
                securityGroup.groupId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = securityGroup.groupId();
        this.resourceName = securityGroup.groupName();
        this.resourceType = EC2SecurityGroup.RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(securityGroup);
    }

    public static class OwnerCIDR {
        public String cidr;
        public String owner;
        public String ownerNetrange;
        public boolean isBadCidr;

        @SuppressWarnings("unused")
        public OwnerCIDR() {
        }

        public OwnerCIDR(String cidr, String owner, String ownerNetrange, boolean isBadCidr) {
            this.cidr = cidr;
            this.owner = owner;
            this.ownerNetrange = ownerNetrange;
            this.isBadCidr = isBadCidr;
        }
    }

    public static class WhoisLookupException extends Exception {

        public WhoisLookupException(String message) {
            super(message);
        }
    }
}

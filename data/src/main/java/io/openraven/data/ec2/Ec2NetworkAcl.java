/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.NetworkAcl;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = Ec2NetworkAcl.TABLE_NAME)
public class Ec2NetworkAcl extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2:NetworkAcl";
    protected static final String TABLE_NAME = "awsec2networkacl";

    @SuppressWarnings("unused")
    public Ec2NetworkAcl() {
        this.resourceType = RESOURCE_TYPE;
    }


    public Ec2NetworkAcl(String accountId, String region, NetworkAcl networkAcl) {
        this.arn = "arn:aws:ec2:%s:%s:network-acl/%s".formatted(region, accountId,
                networkAcl.networkAclId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = networkAcl.networkAclId();
        this.resourceName = networkAcl.networkAclId();
        this.resourceType = RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(networkAcl);
    }
}

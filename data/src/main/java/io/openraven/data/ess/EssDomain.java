/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ess;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.elasticsearch.model.ElasticsearchDomainStatus;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EssDomain.TABLE_NAME)
public class EssDomain extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Elasticsearch::Domain";
    protected static final String TABLE_NAME = "awselasticsearchdomain";

    public EssDomain() {
        this.resourceType = RESOURCE_TYPE;

    }


    public EssDomain(String accountId, String regionId, ElasticsearchDomainStatus domainStatus) {
        this.arn = domainStatus.arn();
        this.awsAccountId = accountId;
        this.resourceId = domainStatus.domainId();
        this.resourceName = domainStatus.domainName();
        this.awsRegion = regionId;
        this.configuration = PayloadUtils.update(configuration, domainStatus);
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.eb;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.elasticbeanstalk.model.EnvironmentDescription;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EbEnvironment.TABLE_NAME)
public class EbEnvironment extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::ElasticBeanstalk";
    protected static final String TABLE_NAME = "awselasticbeanstalk";

    public EbEnvironment() {
        this.resourceType = RESOURCE_TYPE;

    }


    public EbEnvironment(String account, String regionId, EnvironmentDescription cluster) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = cluster.environmentArn();
        this.resourceName = cluster.environmentName();
        this.configuration = PayloadUtils.update(cluster);
        this.resourceType = RESOURCE_TYPE;
    }
}

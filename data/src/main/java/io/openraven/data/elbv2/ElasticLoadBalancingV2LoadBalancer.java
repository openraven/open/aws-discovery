/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.elbv2;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.LoadBalancer;

// v2 of ELB AWS SDK supports application and network load balancers
@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = ElasticLoadBalancingV2LoadBalancer.TABLE_NAME)
public class ElasticLoadBalancingV2LoadBalancer extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::ElasticLoadBalancingV2::LoadBalancer";
    protected static final String TABLE_NAME = "awselasticloadbalancingv2loadbalancer";

    public ElasticLoadBalancingV2LoadBalancer() {
        this.resourceType = RESOURCE_TYPE;

    }


    public ElasticLoadBalancingV2LoadBalancer(LoadBalancer loadBalancer, String region, String accountId) {
        this.resourceName = loadBalancer.dnsName();
        this.resourceId = loadBalancer.loadBalancerName();
        this.awsRegion = region;
        this.awsAccountId = accountId;
        this.resourceType = RESOURCE_TYPE;
        this.arn = loadBalancer.loadBalancerArn();
        this.configuration = PayloadUtils.update(loadBalancer);
        this.createdIso = loadBalancer.createdTime();
    }
}

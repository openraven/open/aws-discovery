/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.backup;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.backup.model.BackupVaultListMember;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = BackupVault.TABLE_NAME)
public class BackupVault extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Backup::BackupVault";
    protected static final String TABLE_NAME = "awsbackupbackupvault";

    public BackupVault() {
        this.resourceType = RESOURCE_TYPE;

    }


    public BackupVault(String accountId, String region, BackupVaultListMember backupVaultListMember) {
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.configuration = PayloadUtils.update(backupVaultListMember);
        this.resourceType = RESOURCE_TYPE;
        this.arn = backupVaultListMember.backupVaultArn();
        this.resourceName = backupVaultListMember.backupVaultName();
        this.resourceId = backupVaultListMember.backupVaultName();
        this.createdIso = backupVaultListMember.creationDate();
    }
}

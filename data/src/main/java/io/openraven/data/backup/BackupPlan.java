/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.backup;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.backup.model.BackupPlansListMember;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = BackupPlan.TABLE_NAME)
public class BackupPlan extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Backup::BackupPlan";
    protected static final String TABLE_NAME = "awsbackupbackupplan";

    public BackupPlan() {
        this.resourceType = RESOURCE_TYPE;

    }

    public BackupPlan(String accountId, String region, BackupPlansListMember backupPlanListMember) {
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.configuration = PayloadUtils.update(backupPlanListMember);
        this.resourceType = RESOURCE_TYPE;
        this.arn = backupPlanListMember.backupPlanArn();
        this.resourceName = backupPlanListMember.backupPlanName();
        this.resourceId = backupPlanListMember.backupPlanId();
        this.createdIso = backupPlanListMember.creationDate();
    }
}

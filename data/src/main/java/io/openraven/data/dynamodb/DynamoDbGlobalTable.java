/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.dynamodb;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.dynamodb.model.GlobalTableDescription;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = DynamoDbGlobalTable.TABLE_NAME)
public class DynamoDbGlobalTable extends AWSResource {
    protected static final String TABLE_NAME = "awsdynamodbglobaltable";
    protected static final String RESOURCE_TYPE = "AWS::DynamoDB::GlobalTable";

    public DynamoDbGlobalTable() {
    }

    public DynamoDbGlobalTable(String account, String region, GlobalTableDescription table) {
        this.awsAccountId = account;
        this.awsRegion = region;
        this.resourceName = table.globalTableName();
        this.resourceId = table.globalTableName();
        this.arn = table.globalTableArn();
        this.createdIso = table.creationDateTime();
        this.configuration = PayloadUtils.update(table);
        this.resourceType = RESOURCE_TYPE;
    }
}

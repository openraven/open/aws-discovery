/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.dynamodb;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.dynamodb.model.TableDescription;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = DynamoDbTable.TABLE_NAME)
public class DynamoDbTable extends AWSResource {
    protected static final String TABLE_NAME = "awsdynamodbtable";
    protected static final String RESOURCE_TYPE = "AWS::DynamoDB::Table";

    public DynamoDbTable() {
    }

    public DynamoDbTable(String account, String region, TableDescription table) {
        this.awsAccountId = account;
        this.awsRegion = region;
        this.resourceName = table.tableName();
        this.resourceId = table.tableId();
        this.arn = table.tableArn();
        this.createdIso = table.creationDateTime();
        this.configuration = PayloadUtils.update(table);
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.route53;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.route53.model.HostedZone;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = Route53HostedZone.TABLE_NAME)
public class Route53HostedZone extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Route53::HostedZone";
    protected static final String TABLE_NAME = "awsroute53hostedzone";

    public Route53HostedZone() {
        this.resourceType = RESOURCE_TYPE;

    }


    public Route53HostedZone(String account, String regionId, HostedZone hostedZone) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.resourceId = hostedZone.id();
        this.arn = "arn:aws:route53:::hostedZone/%s".formatted(hostedZone.id());
        this.resourceName = hostedZone.name();
        this.configuration = PayloadUtils.update(hostedZone);
        this.resourceType = RESOURCE_TYPE;
    }

}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ecs;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ecs.model.Cluster;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EcsCluster.TABLE_NAME)
public class EcsCluster extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::ECS::Cluster";
    protected static final String TABLE_NAME = "awsecscluster";

    public EcsCluster() {
        this.resourceType = RESOURCE_TYPE;

    }


    public EcsCluster(String account, String regionId, Cluster cluster) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = cluster.clusterArn();
        this.resourceName = cluster.clusterName();
        this.configuration = PayloadUtils.update(cluster);
        this.resourceType = RESOURCE_TYPE;
    }
}

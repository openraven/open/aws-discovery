/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.accounts;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.iam.model.User;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = IamUser.TABLE_NAME)
public class IamUser extends AWSResource {
    public static final String RESOURCE_TYPE = "AWS::IAM::User";
    protected static final String TABLE_NAME = "awsiamuser";

    public IamUser() {
        this.resourceType = RESOURCE_TYPE;

    }

    public IamUser(String accountId, String region, User user) {
        this.arn = user.arn();
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = user.userId();
        this.resourceName = user.userName();
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = user.createDate();
        this.configuration = PayloadUtils.update(user);
    }
}

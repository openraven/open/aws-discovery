/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.accounts;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.iam.model.Policy;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = IamPolicy.TABLE_NAME)
public class IamPolicy extends AWSResource {
    public static final String RESOURCE_TYPE = "AWS::IAM::Policy";
    protected static final String TABLE_NAME = "awsiampolicy";

    public IamPolicy() {
        this.resourceType = RESOURCE_TYPE;

    }


    public IamPolicy(String accountId, String region, Policy policy) {
        this.arn = policy.arn();
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = policy.policyId();
        this.resourceName = policy.policyName();
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = policy.createDate();
        this.configuration = PayloadUtils.update(policy);
    }
}

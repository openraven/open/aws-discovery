/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.accounts;

import io.openraven.data.interfaces.AWSResource;

@jakarta.persistence.Entity
@jakarta.persistence.Table(name = UserCredentialReport.TABLE_NAME)
public class UserCredentialReport extends AWSResource {
    public static final String RESOURCE_TYPE = "AWS::UserCredentialReport";
    protected static final String TABLE_NAME = "extendawsiamusercredentialreport";

    public UserCredentialReport() {
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.accounts;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.model.Group;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = IamGroup.TABLE_NAME)
public class IamGroup extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::IAM::Group";
    protected static final String TABLE_NAME = "awsiamgroup";

    public IamGroup() {
        this.resourceType = RESOURCE_TYPE;

    }


    public IamGroup(String accountId, String region, Group group) {
        this.arn = group.arn();
        this.awsAccountId = accountId;
        this.awsRegion = Region.AWS_GLOBAL.id();
        this.resourceId = group.groupId();
        this.resourceName = group.groupName();
        this.resourceType = IamGroup.RESOURCE_TYPE;
        this.createdIso = group.createDate();
        this.configuration = PayloadUtils.update(group);
    }
}

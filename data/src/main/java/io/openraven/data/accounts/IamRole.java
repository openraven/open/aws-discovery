/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.accounts;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.iam.model.Role;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = IamRole.TABLE_NAME)
public class IamRole extends AWSResource {
    public static final String RESOURCE_TYPE = "AWS::IAM::Role";
    protected static final String TABLE_NAME = "awsiamrole";

    public IamRole() {
        this.resourceType = RESOURCE_TYPE;

    }

    public IamRole(String accountId, String region, Role role) {
        this.arn = role.arn();
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = role.roleId();
        this.resourceName = role.roleName();
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = role.createDate();
        this.configuration = PayloadUtils.update(role);
    }
}

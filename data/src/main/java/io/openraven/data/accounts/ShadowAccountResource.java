/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.accounts;

import io.openraven.data.interfaces.AWSResource;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = ShadowAccountResource.TABLE_NAME)
public class ShadowAccountResource extends AWSResource {
    public static final String RESOURCE_TYPE = "ORVN::ShadowAccount";
    protected static final String TABLE_NAME = "orvnshadowaccount";
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.accounts;

import io.openraven.data.interfaces.AWSResource;
import jakarta.persistence.InheritanceType;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class AccountResource extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Account";
    protected static final String TABLE_NAME = "awsaccount";

    public AccountResource() {
        this.resourceType = RESOURCE_TYPE;

    }


    public AccountResource(String accountId) {
        this.arn = "arn:aws:organizations::%s".formatted(accountId);
        this.awsAccountId = accountId;
        this.awsRegion = null;
        this.resourceId = accountId;
        this.resourceType = RESOURCE_TYPE;
    }
}

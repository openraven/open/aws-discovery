/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.efs;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.efs.model.FileSystemDescription;
import software.amazon.awssdk.services.efs.model.Tag;

import java.util.stream.Collectors;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EfsFileSystem.TABLE_NAME)
public class EfsFileSystem extends AWSResource {
    public static final String RESOURCE_TYPE = "AWS::EFS::FileSystem";
    protected static final String TABLE_NAME = "awsefsfilesystem";

    @SuppressWarnings("unused")
    public EfsFileSystem() {
    }

    @SuppressWarnings("unused")
    public EfsFileSystem(String region, FileSystemDescription fileSystem) {
        this.resourceId = fileSystem.fileSystemId();
        this.awsAccountId = fileSystem.ownerId();
        this.awsRegion = region;
        this.arn = "arn:aws:elasticfilesystem:%s:%s:file-system/%s".formatted(region, fileSystem.ownerId(), fileSystem.fileSystemId());
        this.resourceName = fileSystem.name();
        this.configuration = PayloadUtils.update(fileSystem);
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = fileSystem.creationTime();

    }

    public EfsFileSystem(String region, FileSystemDescription fileSystem, ObjectMapper objectMapper) {
        this(region, fileSystem);
        this.tags = objectMapper.convertValue(
                fileSystem.tags().stream()
                        .collect(Collectors.toMap(Tag::key, Tag::value)),
                JsonNode.class
        );
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.cloudfront;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.cloudfront.model.DistributionSummary;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = CloudFrontDistribution.TABLE_NAME)
public class CloudFrontDistribution extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::CloudFront::Distribution";
    protected static final String TABLE_NAME = "awscloudfrontdistribution";

    public CloudFrontDistribution() {
        this.resourceType = RESOURCE_TYPE;

    }


    public CloudFrontDistribution(String account, String regionId, DistributionSummary fileSystem) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = fileSystem.arn();
        this.resourceId = fileSystem.id();
        this.resourceName = fileSystem.domainName();
        this.configuration = PayloadUtils.update(fileSystem);
        this.resourceType = RESOURCE_TYPE;
        this.updatedIso = fileSystem.lastModifiedTime();
    }
}

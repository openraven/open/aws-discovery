/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.fsx;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.fsx.model.FileSystem;
import software.amazon.awssdk.services.fsx.model.Tag;

import java.util.stream.Collectors;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = FSxFileSystem.TABLE_NAME)
public class FSxFileSystem extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::FSx::FileSystem";
    protected static final String TABLE_NAME = "awsfsxfilesystem";

    @SuppressWarnings("unused")
    public FSxFileSystem() {
        this.resourceType = RESOURCE_TYPE;
    }

    @SuppressWarnings("unused")
    public FSxFileSystem(String account, String regionId, FileSystem fileSystem) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = fileSystem.resourceARN();
        this.resourceId = fileSystem.fileSystemId();
        this.resourceName = fileSystem.fileSystemId();
        this.configuration = PayloadUtils.update(fileSystem);
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = fileSystem.creationTime();

    }

    public FSxFileSystem(String account, String regionId, FileSystem fileSystem, ObjectMapper objectMapper) {
        this(account, regionId, fileSystem);
        this.tags = objectMapper.convertValue(
                fileSystem.tags().stream()
                        .collect(Collectors.toMap(Tag::key, Tag::value)),
                JsonNode.class
        );
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.lightsail;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.lightsail.model.RelationalDatabase;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = LightsailDatabase.TABLE_NAME)
public class LightsailDatabase extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Lightsail::Database";
    protected static final String TABLE_NAME = "awslightsaildatabase";

    public LightsailDatabase() {
        this.resourceType = RESOURCE_TYPE;

    }


    public LightsailDatabase(String account, String regionId, RelationalDatabase database) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = database.arn();
        this.resourceName = database.name();
        this.configuration = PayloadUtils.update(database);
        this.resourceType = RESOURCE_TYPE;
    }
}

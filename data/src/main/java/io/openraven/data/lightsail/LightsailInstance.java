/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.lightsail;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.lightsail.model.Instance;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = LightsailInstance.TABLE_NAME)
public class LightsailInstance extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Lightsail::Instance";
    protected static final String TABLE_NAME = "awslightsailinstance";

    public LightsailInstance() {
        this.resourceType = RESOURCE_TYPE;

    }


    public LightsailInstance(String account, String regionId, Instance instance) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = instance.arn();
        this.resourceName = instance.name();
        this.configuration = PayloadUtils.update(instance);
        this.resourceType = RESOURCE_TYPE;
    }
}

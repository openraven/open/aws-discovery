/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.lightsail;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.lightsail.model.LoadBalancer;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = LightsailLoadBalancer.TABLE_NAME)
public class LightsailLoadBalancer extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Lightsail::LoadBalancer";
    protected static final String TABLE_NAME = "awslightsailloadbalancer";

    public LightsailLoadBalancer() {
        this.resourceType = RESOURCE_TYPE;

    }


    public LightsailLoadBalancer(String account, String regionId, LoadBalancer loadBalancer) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = loadBalancer.arn();
        this.resourceName = loadBalancer.name();
        this.configuration = PayloadUtils.update(loadBalancer);
        this.resourceType = RESOURCE_TYPE;
    }
}

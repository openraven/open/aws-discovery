/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.redshift;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.redshift.model.Cluster;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = RedshiftCluster.TABLE_NAME)
public class RedshiftCluster extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Redshift::Cluster";
    protected static final String TABLE_NAME = "awsredshiftcluster";

    @SuppressWarnings("unused")
    public RedshiftCluster() {
        this.resourceType = RESOURCE_TYPE;
    }


    public RedshiftCluster(String regionId, Cluster cluster, String accountId) {
        this.awsRegion = regionId;
        this.resourceType = RESOURCE_TYPE;
        this.resourceId = cluster.clusterIdentifier();
        this.arn = "arn:aws:redshift:%s:%s:cluster:%s".formatted(regionId, accountId, cluster.clusterIdentifier());
        this.resourceName = cluster.dbName();
        this.configuration = PayloadUtils.update(cluster);
        this.awsAccountId = accountId;
        this.createdIso = cluster.clusterCreateTime();
    }
}

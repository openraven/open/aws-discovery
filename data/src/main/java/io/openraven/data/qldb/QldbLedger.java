/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.qldb;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.qldb.model.DescribeLedgerResponse;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = QldbLedger.TABLE_NAME)
public class QldbLedger extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Qldb::Ledger";
    protected static final String TABLE_NAME = "awsqldbledger";

    public QldbLedger() {
        this.resourceType = RESOURCE_TYPE;

    }


    public QldbLedger(String account, String regionId, DescribeLedgerResponse ledger) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = ledger.arn();
        this.resourceName = ledger.name();
        this.configuration = PayloadUtils.update(ledger);
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = ledger.creationDateTime();
    }
}

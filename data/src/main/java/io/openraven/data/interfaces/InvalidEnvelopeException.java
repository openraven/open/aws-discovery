/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */


package io.openraven.data.interfaces;

public class InvalidEnvelopeException extends RuntimeException {

    public InvalidEnvelopeException() {
    }

    public InvalidEnvelopeException(String errorMessage) {
        super(errorMessage);
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.interfaces;

import com.fasterxml.jackson.databind.JsonNode;
import io.hypersistence.utils.hibernate.type.json.JsonBinaryType;
import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.Instant;

@Access(AccessType.FIELD)
@MappedSuperclass
@Convert(attributeName = "entityAttribute", converter = JsonBinaryType.class)
@jakarta.persistence.Table(name = AWSResource.TABLE_NAME)
public class AWSResource {
    public static final String TABLE_NAME = "assets";
    @Id
    @Column(name = "documentid", columnDefinition = "TEXT",
            nullable = false, unique = true)
    public String documentId;

    @Column(name = "arn", columnDefinition = "TEXT")
    public String arn;
    @Column(name = "resourcename", columnDefinition = "TEXT")
    public String resourceName;
    @Column(name = "resourceid", columnDefinition = "TEXT")
    public String resourceId;

    // this needs to be updatable = false due to its use a a discriminator
    @Column(name = "resourcetype", columnDefinition = "TEXT", updatable = false)
    public String resourceType;

    @Column(name = "awsregion", columnDefinition = "TEXT")
    public String awsRegion;
    @Column(name = "awsaccountid", columnDefinition = "TEXT")
    public String awsAccountId;
    @Column(name = "creatediso", columnDefinition = "TIMESTAMPTZ")
    public Instant createdIso;
    @Column(name = "updatediso", columnDefinition = "TIMESTAMPTZ")
    public Instant updatedIso;
    @Column(name = "discoverysessionid", columnDefinition = "TEXT")
    public String discoverySessionId;

    @Transient
    public Long maxSizeInBytes;
    @Transient
    public Long sizeInBytes;

    @Column(name = "configuration", columnDefinition = "JSONB")
    @JdbcTypeCode(SqlTypes.JSON)
    public JsonNode configuration;

    @Column(name = "supplementaryconfiguration", columnDefinition = "JSONB")
    @JdbcTypeCode(SqlTypes.JSON)
    public JsonNode supplementaryConfiguration;

    @Column(name = "tags", columnDefinition = "JSONB")
    @JdbcTypeCode(SqlTypes.JSON)
    public JsonNode tags;

    @Column(name = "discoverymeta", columnDefinition = "JSONB")
    @JdbcTypeCode(SqlTypes.JSON)
    public JsonNode discoveryMeta;
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.interfaces;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Envelope {
    @JsonIgnore
    private static final ObjectMapper MAPPER;
    @JsonIgnore
    private static final Logger LOG = LoggerFactory.getLogger(Envelope.class);

    static {
        MAPPER = new ObjectMapper()
                .findAndRegisterModules()
                .activateDefaultTyping(
                        LaissezFaireSubTypeValidator.instance,
                        ObjectMapper.DefaultTyping.NON_FINAL,
                        JsonTypeInfo.As.WRAPPER_ARRAY
                )
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        ;
    }

    private int version = 1; // default the version to 1 if not specified

    private String integration;

    private String discoverySession;

    private String ts;

    private Object contents;

    public Envelope() {
    }

    public Envelope(final String integration, final String discoverySession, final String ts, final AWSResource contents) {
        this(integration, discoverySession, ts, contents, false);
    }

    public Envelope(final String integration, final String discoverySession, final String ts, final AWSResource contents, final boolean compress) {
        this.integration = integration;
        this.discoverySession = discoverySession;
        this.ts = ts;

        // TODO: one day we can deprecate placing discoverySessionId in the root of the document instead of in the discoveryMeta object.
        // Keeping this here for potential backwards compatibility issues
        contents.discoverySessionId = discoverySession;
        DiscoveryMeta metaData = new DiscoveryMeta(discoverySession);
        contents.discoveryMeta = MAPPER.valueToTree(metaData);

        if (!compress) {
            // the version is by default 1, so don't need to set it here
            // this.version = 1;
            this.contents = contents;
        } else {
            this.version = 2;
            this.contents = compressAndEncode(contents);
        }
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getIntegration() {
        return integration;
    }

    @SuppressWarnings("unused")
    public void setIntegration(String integration) {
        this.integration = integration;
    }

    public String getDiscoverySession() {
        return discoverySession;
    }

    public void setDiscoverySession(String discoverySession) {
        this.discoverySession = discoverySession;
    }

    @SuppressWarnings("unused")
    public String getTs() {
        return ts;
    }

    @SuppressWarnings("unused")
    public void setTs(String ts) {
        this.ts = ts;
    }

    public Object getContents() {
        return contents;
    }

    public void setContents(Object contents) {
        this.contents = contents;
    }

    @JsonIgnore
    public AWSResource getAwsResource() {
        if (version == 1) {
            return (AWSResource) this.contents;
        } else if (version == 2) {
            return decodeAndDecompress((String) this.contents);
        }

        throw new InvalidEnvelopeException("Invalid envelope version = " + version);
    }

    @JsonIgnore
    private String compressAndEncode(AWSResource contents) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            MAPPER.writeValue(gzipOutputStream, contents);
            gzipOutputStream.flush();
            var base64EncodedGzipCompressedString = new String(Base64.getEncoder().encode(byteArrayOutputStream.toByteArray()), StandardCharsets.UTF_8);
            LOG.debug("Returning base64 encoded string of length {}", base64EncodedGzipCompressedString.length());
            return base64EncodedGzipCompressedString;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @JsonIgnore
    private AWSResource decodeAndDecompress(String compressedContents) {
        try {
            var base64DecodedGzipCompressedBytes = Base64.getDecoder().decode(compressedContents);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(base64DecodedGzipCompressedBytes);
            GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
            BufferedReader bf = new BufferedReader(new InputStreamReader(gzipInputStream, StandardCharsets.UTF_8));
            final StringBuilder content = new StringBuilder();
            String line;
            while ((line = bf.readLine()) != null) {
                content.append(line);
            }

            return MAPPER.readValue(content.toString(), AWSResource.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static private class DiscoveryMeta {
        public String discoverySessionId;
        public String updatedIso;

        public DiscoveryMeta(final String discoverySessionId) {
            this.discoverySessionId = discoverySessionId;
            this.updatedIso = Instant.now().toString();
        }
    }

}

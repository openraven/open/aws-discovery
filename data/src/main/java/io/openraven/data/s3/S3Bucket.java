/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.s3;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.s3.model.Bucket;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = S3Bucket.TABLE_NAME)
public class S3Bucket extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::S3::Bucket";
    protected static final String TABLE_NAME = "awss3bucket";

    @SuppressWarnings("unused")
    public S3Bucket() {
        this.resourceType = RESOURCE_TYPE;
    }


    public S3Bucket(String account, String region, Bucket bucket) {
        awsAccountId = account;
        awsRegion = region;
        arn = "arn:aws:s3:::" + bucket.name();
        resourceName = bucket.name();
        resourceId = bucket.name();
        configuration = PayloadUtils.update(bucket);
        resourceType = RESOURCE_TYPE;
        createdIso = bucket.creationDate();
    }

}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.elasticache;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.elasticache.model.CacheCluster;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = ElastiCacheCluster.TABLE_NAME)
public class ElastiCacheCluster extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::ElastiCache::Cluster";
    protected static final String TABLE_NAME = "awselasticachecluster";

    public ElastiCacheCluster() {
        this.resourceType = RESOURCE_TYPE;

    }


    public ElastiCacheCluster(String account, String regionId, CacheCluster cluster) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = cluster.arn();
        this.resourceId = cluster.cacheClusterId();
        this.resourceName = cluster.cacheClusterId();
        this.configuration = PayloadUtils.update(cluster);
        this.resourceType = RESOURCE_TYPE;
    }
}

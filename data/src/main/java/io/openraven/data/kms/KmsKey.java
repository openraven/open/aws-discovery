/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.kms;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.kms.model.KeyListEntry;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = KmsKey.TABLE_NAME)
public class KmsKey extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Kms::Key";
    protected static final String TABLE_NAME = "awskmskey";

    public KmsKey() {
        this.resourceType = RESOURCE_TYPE;

    }


    public KmsKey(String account, String regionId, KeyListEntry key) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = key.keyArn();
        this.resourceId = key.keyId();
        this.resourceName = key.toString();
        this.configuration = PayloadUtils.update(key);
        this.resourceType = RESOURCE_TYPE;
    }

}

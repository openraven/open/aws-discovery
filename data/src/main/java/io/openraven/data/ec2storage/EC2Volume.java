/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2storage;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.Volume;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EC2Volume.TABLE_NAME)
public class EC2Volume extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2:Volume";
    protected static final String TABLE_NAME = "awsec2volume";

    @SuppressWarnings("unused")
    public EC2Volume() {
        this.resourceType = RESOURCE_TYPE;
    }


    public EC2Volume(String accountId, String region, Volume volume) {
        this.arn = "arn:aws:ec2:%s:%s:volume/%s".formatted(region, accountId, volume.volumeId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = volume.volumeId();
        this.resourceType = EC2Volume.RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(volume);
    }
}

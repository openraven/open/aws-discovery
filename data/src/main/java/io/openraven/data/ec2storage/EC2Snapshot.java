/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ec2storage;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ec2.model.Snapshot;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EC2Snapshot.TABLE_NAME)
public class EC2Snapshot extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EC2::Snapshot";
    protected static final String TABLE_NAME = "awsec2snapshot";

    @SuppressWarnings("unused")
    public EC2Snapshot() {
        this.resourceType = RESOURCE_TYPE;
    }


    public EC2Snapshot(String accountId, String region, Snapshot snapshot) {
        this.arn = "arn:aws:ec2:%s:%s:snapshot/%s".formatted(region, accountId, snapshot.snapshotId());
        this.awsAccountId = accountId;
        this.awsRegion = region;
        this.resourceId = snapshot.snapshotId();
        this.resourceType = EC2Snapshot.RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(snapshot);
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.cassandra;

import io.openraven.data.interfaces.AWSResource;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = CassandraKeyspace.TABLE_NAME)
public class CassandraKeyspace extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Cassandra::Keyspace";
    protected static final String TABLE_NAME = "awscassandrakeyspace";

    public CassandraKeyspace() {
        this.resourceType = RESOURCE_TYPE;

    }


    public CassandraKeyspace(String regionId, String keyspaceName) {
        this.awsRegion = regionId;
        this.arn = "arn:aws:cassandra:keyspace:%s::%s".formatted(regionId, keyspaceName);
        this.resourceId = keyspaceName;
        this.resourceName = keyspaceName;
        this.resourceType = RESOURCE_TYPE;
    }
}

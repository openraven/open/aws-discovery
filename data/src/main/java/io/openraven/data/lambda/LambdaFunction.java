/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.lambda;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.lambda.model.FunctionConfiguration;

import java.time.Instant;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = LambdaFunction.TABLE_NAME)
public class LambdaFunction extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Lambda::Function";
    protected static final String TABLE_NAME = "awslambdafunction";

    @SuppressWarnings("unused")
    public LambdaFunction() {
        this.resourceType = RESOURCE_TYPE;
    }


    public LambdaFunction(String account, String regionId, FunctionConfiguration function) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = function.functionArn();
        this.resourceId = function.revisionId();
        this.resourceName = function.functionName();
        this.configuration = PayloadUtils.update(function);
        this.resourceType = RESOURCE_TYPE;
        this.updatedIso = Instant.parse(function.lastModified());
    }

}

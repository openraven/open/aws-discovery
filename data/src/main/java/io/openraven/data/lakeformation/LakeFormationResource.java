/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.lakeformation;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.lakeformation.model.ResourceInfo;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = LakeFormationResource.TABLE_NAME)
public class LakeFormationResource extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::LakeFormation::Resource";
    protected static final String TABLE_NAME = "awslakeformationresource";

    public LakeFormationResource() {
        this.resourceType = RESOURCE_TYPE;

    }


    public LakeFormationResource(String account, String regionId, ResourceInfo resource) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = resource.resourceArn();
        this.configuration = PayloadUtils.update(resource);
        this.resourceType = RESOURCE_TYPE;
        this.updatedIso = resource.lastModified();
    }
}

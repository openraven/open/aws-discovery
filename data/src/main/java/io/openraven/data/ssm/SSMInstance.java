/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.ssm;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.ssm.model.InstanceInformation;

@jakarta.persistence.Entity
@jakarta.persistence.Table(name = SSMInstance.TABLE_NAME)
public class SSMInstance extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::SSM::Instance";
    protected static final String TABLE_NAME = "extendawsssminstance";

    public SSMInstance() {
        this.resourceType = RESOURCE_TYPE;
    }

    public SSMInstance(String account, String regionId, InstanceInformation instanceInformation) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = "arn:aws:ec2:%s:instance/%s".formatted(regionId, instanceInformation.instanceId());
        this.resourceName = instanceInformation.name();
        this.resourceId = instanceInformation.instanceId();
        this.configuration = PayloadUtils.update(instanceInformation);
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.rds;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.rds.model.DBInstance;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = RDSInstance.TABLE_NAME)
public class RDSInstance extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::RDS::DBInstance";
    protected static final String TABLE_NAME = "awsrdsdbinstance";

    @SuppressWarnings("unused")
    public RDSInstance() {
        this.resourceType = RESOURCE_TYPE;
    }


    public RDSInstance(String account, String regionId, DBInstance resource) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = resource.dbInstanceArn();
        this.resourceId = resource.dbInstanceArn();
        this.resourceName = resource.dbInstanceIdentifier();
        this.resourceType = RESOURCE_TYPE;
        this.configuration = PayloadUtils.update(resource);
        this.createdIso = resource.instanceCreateTime();
    }

}

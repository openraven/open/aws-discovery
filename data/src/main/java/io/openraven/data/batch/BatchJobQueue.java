/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.batch;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.batch.model.JobQueueDetail;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = BatchJobQueue.TABLE_NAME)
public class BatchJobQueue extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Batch::JobQueue";
    protected static final String TABLE_NAME = "awsbatchjobqueue";

    public BatchJobQueue() {
        this.resourceType = RESOURCE_TYPE;

    }


    public BatchJobQueue(String account, String regionId, JobQueueDetail jobQueue) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = jobQueue.jobQueueArn();
        this.resourceName = jobQueue.jobQueueName();
        this.configuration = PayloadUtils.update(jobQueue);
        this.resourceType = RESOURCE_TYPE;
    }
}

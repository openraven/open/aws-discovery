/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.batch;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.batch.model.JobDefinition;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = BatchJobDefinition.TABLE_NAME)
public class BatchJobDefinition extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Batch::JobDefinition";
    protected static final String TABLE_NAME = "awsbatchjobdefinition";

    public BatchJobDefinition() {
        this.resourceType = RESOURCE_TYPE;

    }


    public BatchJobDefinition(String account, String regionId, JobDefinition jobDefinition) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = jobDefinition.jobDefinitionArn();
        this.resourceName = jobDefinition.jobDefinitionName();
        this.configuration = PayloadUtils.update(jobDefinition);
        this.resourceType = RESOURCE_TYPE;
    }
}

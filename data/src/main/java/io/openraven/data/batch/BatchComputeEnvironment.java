/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.batch;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.batch.model.ComputeEnvironmentDetail;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = BatchComputeEnvironment.TABLE_NAME)
public class BatchComputeEnvironment extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Batch::ComputeEnvironment";
    protected static final String TABLE_NAME = "awsbatchcomputeenvironment";

    public BatchComputeEnvironment() {
        this.resourceType = RESOURCE_TYPE;

    }


    public BatchComputeEnvironment(String account, String regionId, ComputeEnvironmentDetail computeEnvironment) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = computeEnvironment.computeEnvironmentArn();
        this.resourceName = computeEnvironment.computeEnvironmentName();
        this.configuration = PayloadUtils.update(computeEnvironment);
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.neptune;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.neptune.model.DBCluster;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = NeptuneCluster.TABLE_NAME)
public class NeptuneCluster extends AWSResource {
    protected static final String TABLE_NAME = "awsneptunecluster";
    protected static final String RESOURCE_TYPE = "AWS::Neptune::Cluster";

    public NeptuneCluster() {
        this.resourceType = RESOURCE_TYPE;

    }


    public NeptuneCluster(String account, String regionId, DBCluster cluster) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = cluster.dbClusterArn();
        this.resourceName = cluster.databaseName();
        this.resourceId = cluster.dbClusterIdentifier();
        this.configuration = PayloadUtils.update(cluster);
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.neptune;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.neptune.model.DBInstance;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = NeptuneInstance.TABLE_NAME)
public class NeptuneInstance extends AWSResource {
    protected static final String TABLE_NAME = "awsneptuneinstance";
    protected static final String RESOURCE_TYPE = "AWS::Neptune::Instance";

    public NeptuneInstance() {
        this.resourceType = RESOURCE_TYPE;

    }


    public NeptuneInstance(String account, String regionId, DBInstance instance) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = instance.dbInstanceArn();
        this.resourceName = instance.dbName();
        this.resourceId = instance.dbInstanceIdentifier();
        this.configuration = PayloadUtils.update(instance);
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.storagegateway;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.storagegateway.model.GatewayInfo;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = StorageGatewayGateway.TABLE_NAME)
public class StorageGatewayGateway extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::StorageGateway::Gateway";
    protected static final String TABLE_NAME = "awsstoragegatewaygateway";

    public StorageGatewayGateway() {
        this.resourceType = RESOURCE_TYPE;

    }


    public StorageGatewayGateway(String account, String regionId, GatewayInfo gateway) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = gateway.gatewayARN();
        this.resourceName = gateway.gatewayName();
        this.resourceId = gateway.gatewayId();
        this.configuration = PayloadUtils.update(gateway);
        this.resourceType = RESOURCE_TYPE;
    }
}

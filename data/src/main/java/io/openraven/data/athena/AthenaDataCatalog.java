/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.athena;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.athena.model.DataCatalogSummary;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = AthenaDataCatalog.TABLE_NAME)
public class AthenaDataCatalog extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Athena::DataCatalog";
    protected static final String TABLE_NAME = "awsathenadatacatalog";

    public AthenaDataCatalog() {
        this.resourceType = RESOURCE_TYPE;

    }


    public AthenaDataCatalog(String account, String regionId, DataCatalogSummary dataCatalogSummary) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = "arn:aws:athena:%s:%s:datacatalog/%s".formatted(regionId, account, dataCatalogSummary.catalogName());
        this.resourceName = dataCatalogSummary.catalogName();
        this.configuration = PayloadUtils.update(dataCatalogSummary);
        this.resourceType = RESOURCE_TYPE;
    }
}

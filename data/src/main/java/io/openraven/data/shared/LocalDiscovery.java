/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.shared;

@FunctionalInterface
public interface LocalDiscovery<C, D> {
    void discover(C client, D data);
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

package io.openraven.data.shared;

public class Conversions {
    public static long GibToBytes(long gib) {
        return gib * 1074000000L;
    }

    @SuppressWarnings("unused")
    public static long MibToBytes(long mib) {
        return mib * 1049000L;
    }
}

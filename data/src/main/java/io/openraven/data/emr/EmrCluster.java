/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.emr;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.emr.model.ClusterSummary;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EmrCluster.TABLE_NAME)
public class EmrCluster extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EMR::Cluster";
    protected static final String TABLE_NAME = "awsemrcluster";

    public EmrCluster() {
        this.resourceType = RESOURCE_TYPE;

    }


    public EmrCluster(String account, String regionId, ClusterSummary cluster) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = "arn:aws:emr:::cluster/%s".formatted(cluster.id());
        this.resourceId = cluster.id();
        this.resourceName = cluster.name();
        this.configuration = PayloadUtils.update(cluster);
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = cluster.status().timeline().creationDateTime();
    }
}

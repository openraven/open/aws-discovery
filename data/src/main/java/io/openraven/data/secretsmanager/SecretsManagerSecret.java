/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.secretsmanager;

import io.openraven.data.interfaces.AWSResource;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = SecretsManagerSecret.TABLE_NAME)
public class SecretsManagerSecret extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::SecretsManager";
    protected static final String TABLE_NAME = "awssecretsmanager";

    public SecretsManagerSecret() {
        this.resourceType = RESOURCE_TYPE;

    }


    public SecretsManagerSecret(String account, String regionId) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = RESOURCE_TYPE;
        this.resourceName = RESOURCE_TYPE;
        this.resourceId = RESOURCE_TYPE;
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.elb;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.elasticloadbalancing.model.LoadBalancerDescription;


@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = ElasticLoadBalancingLoadBalancer.TABLE_NAME)
public class ElasticLoadBalancingLoadBalancer extends AWSResource {
    public static final String RESOURCE_TYPE = "AWS::ElasticLoadBalancing::LoadBalancer";
    protected static final String TABLE_NAME = "awselasticloadbalancingloadbalancer";
    private static final String ARN_FORMAT = "arn:aws:elasticloadbalancing:%s:%s:loadbalancer/%s";

    @SuppressWarnings("unused")
    public ElasticLoadBalancingLoadBalancer() {
        this.resourceType = RESOURCE_TYPE;
    }


    public ElasticLoadBalancingLoadBalancer(LoadBalancerDescription loadBalancer, String region, String accountId) {
        this.resourceName = loadBalancer.dnsName();
        this.resourceId = loadBalancer.loadBalancerName();
        this.awsRegion = region;
        this.awsAccountId = accountId;
        this.resourceType = RESOURCE_TYPE;
        this.arn = ARN_FORMAT.formatted(region, accountId, loadBalancer.loadBalancerName());
        this.configuration = PayloadUtils.update(loadBalancer);
        this.createdIso = loadBalancer.createdTime();
    }
}

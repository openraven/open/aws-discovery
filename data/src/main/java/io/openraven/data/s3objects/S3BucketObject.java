/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.s3objects;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.util.Map;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = S3BucketObject.TABLE_NAME)
public class S3BucketObject extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::S3::BucketObject";
    public static final String BUCKET_NAME_KEY = "BucketName";
    protected static final String TABLE_NAME = "awss3bucketobject";

    @SuppressWarnings("unused")
    public S3BucketObject() {
        this.resourceType = RESOURCE_TYPE;
    }


    public S3BucketObject(String account, String region, String bucketName, S3Object s3Object) {
        awsAccountId = account;
        awsRegion = region;
        final String objectKey = s3Object.key();
        final String objectLocationWithinBucket = "%s/%s".formatted(bucketName, objectKey);
        arn = "arn:aws:s3:::" + objectLocationWithinBucket;
        resourceName = objectKey;
        resourceId = objectLocationWithinBucket;
        configuration = PayloadUtils.update(s3Object);
        resourceType = RESOURCE_TYPE;
        createdIso = s3Object.lastModified();
        supplementaryConfiguration = PayloadUtils.update(supplementaryConfiguration,
                Map.of(BUCKET_NAME_KEY, bucketName));
    }

}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.eks;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.eks.model.Cluster;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = EksCluster.TABLE_NAME)
public class EksCluster extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::EKS::Cluster";
    protected static final String TABLE_NAME = "awsekscluster";

    public EksCluster() {
        this.resourceType = RESOURCE_TYPE;

    }


    public EksCluster(String account, String regionId, Cluster cluster) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = cluster.arn();
        this.resourceName = cluster.name();
        this.configuration = PayloadUtils.update(cluster);
        this.resourceType = RESOURCE_TYPE;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.cloudtrail;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.cloudtrail.model.TrailInfo;


@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = CloudTrail.TABLE_NAME)
public class CloudTrail extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::CloudTrail::Trail";
    protected static final String TABLE_NAME = "awscloudtrailtrail";

    public CloudTrail() {
        this.resourceType = RESOURCE_TYPE;

    }

    public CloudTrail(String account, String regionId, TrailInfo resource) {
        this.awsRegion = regionId;
        this.awsAccountId = account;
        this.arn = resource.trailARN();
        this.resourceName = resource.name();
        this.configuration = PayloadUtils.update(resource);
        this.resourceType = RESOURCE_TYPE;
    }
}

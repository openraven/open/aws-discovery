/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.glacier;

import io.openraven.data.interfaces.AWSResource;
import io.openraven.data.shared.PayloadUtils;
import software.amazon.awssdk.services.glacier.model.DescribeVaultOutput;

import java.time.Instant;

@jakarta.persistence.Entity
@jakarta.persistence.Inheritance(strategy = jakarta.persistence.InheritanceType.TABLE_PER_CLASS)
@jakarta.persistence.Table(name = GlacierVault.TABLE_NAME)
public class GlacierVault extends AWSResource {

    public static final String RESOURCE_TYPE = "AWS::Glacier::Vault";
    protected static final String TABLE_NAME = "awsglaciervault";

    public GlacierVault() {
        this.resourceType = RESOURCE_TYPE;

    }


    public GlacierVault(String regionId, String accountId, DescribeVaultOutput vault) {
        this.awsRegion = regionId;
        this.awsAccountId = accountId;
        this.arn = vault.vaultARN();
        this.resourceName = vault.vaultName();
        this.configuration = PayloadUtils.update(vault);
        this.resourceType = RESOURCE_TYPE;
        this.createdIso = Instant.parse(vault.creationDate());
    }
}

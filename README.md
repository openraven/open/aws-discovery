# AWS Discovery

Service to discover AWS resources and publish to Postgres

## Prerequisites

* AWS CLI - https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
* Docker Compose - https://docs.docker.com/compose/install
* git - https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

## Clone the repo

```console
$ git clone https://github.com/openraven/aws-api-discovery
```

## Setup AWS permissions and services

You will need read access in the AWS account for the services you'd like to discover.

## Setup kafka, zookeeper

    docker-compose -f docker-compose.yml

(you will need to run PG separatly - see database-schema project)

## Build and run from source

The following will startup the service to discover aws EC2 assets and write them to postgres.

    mvn spring-boot:run -Dspring-boot.run.profiles="default,producer,EC2, consumer" 

Note: EC2 can be replaced with any of the following profiles.

    Accounts
    Athena
    BACKUP
    CloudFront
    CloudSearch
    dynamoDb
    ElasticBeanstalk
    EC2
    ECS
    EFS
    EKS
    ELB
    ELBV2
    EMR
    ESS
    FSX
    Glacier
    KMS
    FSX
    LakeFormation
    Lambda
    Lightsail
    QLDB
    RDS
    RSH
    S3
    SecretsManager
    StorageGateway
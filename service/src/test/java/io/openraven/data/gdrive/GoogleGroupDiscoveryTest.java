/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.Group;
import com.google.api.services.directory.model.Member;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.openraven.consumer.properties.GenericDiscoveryProperties;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.data.shared.GenericAsset;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.magpie.api.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GoogleGroupDiscoveryTest {
    protected static final List<String> EXPECTED_DIRECTORY_SCOPES = List.of(
            DirectoryScopes.ADMIN_DIRECTORY_GROUP_READONLY,
            DirectoryScopes.ADMIN_DIRECTORY_GROUP_MEMBER_READONLY);
    private static final int GROUP_BATCH_SIZE = 3;
    private static final int MEMBER_BATCH_SIZE = 3;

    @Mock
    ClientAdapterFactory clientAdapterFactory;
    @Mock
    GoogleWorkspaceAssetFactory assetFactory;
    @Mock
    MeterRegistry meterRegistry;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    Session session;
    @Mock
    GenericAssetEmitter emitter;
    @Mock
    GenericAssetDiscoveryMetrics groupMetrics;
    @Mock
    GenericAssetDiscoveryMetrics memberMetrics;
    MockedStatic<GenericAssetDiscoveryMetrics> metricsStatic;

    GoogleGroupDiscovery testSubject;

    @BeforeEach
    void setUp() {
        metricsStatic = Mockito.mockStatic(GenericAssetDiscoveryMetrics.class);
        metricsStatic.when(() -> GenericAssetDiscoveryMetrics.create(meterRegistry, "google-group"))
                .thenReturn(groupMetrics);
        metricsStatic.when(() -> GenericAssetDiscoveryMetrics.create(meterRegistry, "google-group-member"))
                .thenReturn(memberMetrics);

        GenericDiscoveryProperties discoveryProperties = mock(GenericDiscoveryProperties.class, RETURNS_DEEP_STUBS);
        when(discoveryProperties.getBatchSize().getGroup()).thenReturn(GROUP_BATCH_SIZE);
        when(discoveryProperties.getBatchSize().getGroupMember()).thenReturn(MEMBER_BATCH_SIZE);

        testSubject = new GoogleGroupDiscovery(clientAdapterFactory,
                assetFactory,
                meterRegistry,
                discoveryProperties);
    }

    @AfterEach
    void tearDown() {
        metricsStatic.close();
    }

    @Test
    void doesNothingWhenCredentialConfigurationAbsent() {
        when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(false);

        testSubject.discover(objectMapper, session, emitter);

        verifyNoMoreInteractions(clientAdapterFactory, meterRegistry);
        verifyNoInteractions(groupMetrics);
        verifyNoInteractions(assetFactory, objectMapper, session, emitter);
    }

    @Nested
    class WhenCredentialConfigurationIsPresent {
        @Mock
        DirectoryClientAdapter directoryClient;
        @Mock
        Timer groupBatchTimer;
        @Mock
        Timer memberBatchTimer;

        @BeforeEach
        void setUp() {
            when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(true);
            when(clientAdapterFactory.createDirectoryAdapterForSuperUser(EXPECTED_DIRECTORY_SCOPES))
                    .thenReturn(directoryClient);
            when(groupMetrics.getBatchProcessingTimer()).thenReturn(groupBatchTimer);
            when(memberMetrics.getBatchProcessingTimer()).thenReturn(memberBatchTimer);
        }

        @Test
        void singleGroup() {
            Group group = new Group().setId("group id");
            when(directoryClient.listAllGroups(GROUP_BATCH_SIZE, groupBatchTimer)).thenReturn(List.of(group));

            GenericAsset groupAsset = new GenericAsset();
            groupAsset.setAssetId("group 1 id");
            when(assetFactory.createGroupResource(group)).thenReturn(groupAsset);

            Member member1 = new Member().setEmail("member1@example.com");
            Member member2 = new Member().setEmail("member2@example.com");
            when(directoryClient.listAllMembers("group id", MEMBER_BATCH_SIZE, memberBatchTimer)).thenReturn(
                    List.of(member1, member2)
            );

            GenericAsset member1Asset = new GenericAsset();
            member1Asset.setAssetId("member 1 id");
            when(assetFactory.createGroupMemberResource(group, member1)).thenReturn(member1Asset);
            GenericAsset member2Asset = new GenericAsset();
            member1Asset.setAssetId("member 2 id");
            when(assetFactory.createGroupMemberResource(group, member2)).thenReturn(member2Asset);

            testSubject.discover(objectMapper, session, emitter);

            verify(emitter).sendEnvelope(GoogleGroupDiscovery.SERVICE, session, groupAsset);
            verify(groupMetrics, times(1)).incrementDiscoveredCount();
            verify(emitter).sendEnvelope(GoogleGroupDiscovery.SERVICE, session, member1Asset);
            verify(emitter).sendEnvelope(GoogleGroupDiscovery.SERVICE, session, member2Asset);
            verify(memberMetrics, times(2)).incrementDiscoveredCount();
            verify(groupMetrics).recordFullDiscoveryTime(anyLong());
            verify(memberMetrics).recordFullDiscoveryTime(anyLong());
        }
    }
}
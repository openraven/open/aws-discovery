/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.Domains;
import io.micrometer.core.instrument.MeterRegistry;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.data.shared.GenericAsset;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.magpie.api.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GoogleDomainDiscoveryTest {
    protected static final List<String> EXPECTED_DIRECTORY_SCOPES = List.of(
            DirectoryScopes.ADMIN_DIRECTORY_DOMAIN_READONLY);

    @Mock
    ClientAdapterFactory clientAdapterFactory;
    @Mock
    GoogleWorkspaceAssetFactory assetFactory;
    @Mock
    MeterRegistry meterRegistry;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    Session session;
    @Mock
    GenericAssetEmitter emitter;
    @Mock
    GenericAssetDiscoveryMetrics metrics;
    MockedStatic<GenericAssetDiscoveryMetrics> metricsStatic;

    GoogleDomainDiscovery testSubject;

    @BeforeEach
    void setUp() {
        metricsStatic = Mockito.mockStatic(GenericAssetDiscoveryMetrics.class);
        metricsStatic.when(() -> GenericAssetDiscoveryMetrics.create(meterRegistry, "google-domain"))
                .thenReturn(metrics);

        testSubject = new GoogleDomainDiscovery(clientAdapterFactory,
                assetFactory,
                meterRegistry);
    }

    @AfterEach
    void tearDown() {
        metricsStatic.close();
    }

    @Test
    void doesNothingWhenCredentialConfigurationAbsent() {
        when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(false);

        testSubject.discover(objectMapper, session, emitter);

        verifyNoMoreInteractions(clientAdapterFactory, meterRegistry);
        verifyNoInteractions(metrics);
        verifyNoInteractions(assetFactory, objectMapper, session, emitter);
    }

    @Nested
    class WhenCredentialConfigurationIsPresent {
        @Mock
        DirectoryClientAdapter directoryClient;

        @BeforeEach
        void setUp() {
            when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(true);
            when(clientAdapterFactory.createDirectoryAdapterForSuperUser(EXPECTED_DIRECTORY_SCOPES))
                    .thenReturn(directoryClient);
        }

        @Test
        void singleDomain() {
            Domains domain = new Domains().setDomainName("example.com");
            when(directoryClient.listDomains()).thenReturn(List.of(domain));

            GenericAsset domainAsset = new GenericAsset();
            domainAsset.setAssetId("domain 1 id");
            when(assetFactory.createDomainResource(domain)).thenReturn(domainAsset);

            testSubject.discover(objectMapper, session, emitter);

            verify(emitter).sendEnvelope(GoogleDomainDiscovery.SERVICE, session, domainAsset);
            verify(metrics, times(1)).incrementDiscoveredCount();
            verify(metrics).recordFullDiscoveryTime(anyLong());
        }
    }
}
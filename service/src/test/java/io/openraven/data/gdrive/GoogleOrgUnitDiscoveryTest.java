/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.OrgUnit;
import com.google.api.services.directory.model.OrgUnits;
import io.micrometer.core.instrument.MeterRegistry;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.data.shared.GenericAsset;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.magpie.api.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GoogleOrgUnitDiscoveryTest {
    protected static final List<String> EXPECTED_SCOPES = List.of(DirectoryScopes.ADMIN_DIRECTORY_ORGUNIT_READONLY);
    @Mock
    ClientAdapterFactory clientAdapterFactory;
    @Mock
    GoogleWorkspaceAssetFactory assetFactory;
    @Mock
    MeterRegistry meterRegistry;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    Session session;
    @Mock
    GenericAssetEmitter emitter;
    @Mock
    GenericAssetDiscoveryMetrics metrics;
    MockedStatic<GenericAssetDiscoveryMetrics> metricsStatic;

    GoogleOrgUnitDiscovery testSubject;

    @BeforeEach
    void setUp() {
        metricsStatic = Mockito.mockStatic(GenericAssetDiscoveryMetrics.class);
        metricsStatic.when(() -> GenericAssetDiscoveryMetrics.create(meterRegistry, "google-org-unit"))
                .thenReturn(metrics);

        testSubject = new GoogleOrgUnitDiscovery(clientAdapterFactory,
                assetFactory,
                meterRegistry);
    }

    @AfterEach
    void tearDown() {
        metricsStatic.close();
    }

    @Test
    void doesNothingWhenCredentialConfigurationAbsent() {
        when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(false);

        testSubject.discover(objectMapper, session, emitter);

        verifyNoMoreInteractions(clientAdapterFactory, meterRegistry);
        verifyNoInteractions(metrics);
        verifyNoInteractions(assetFactory, objectMapper, session, emitter);
    }

    @Test
    void whenRootOrgUnitHasNoChildren() {
        when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(true);
        var client = mock(DirectoryClientAdapter.class);
        when(clientAdapterFactory.createDirectoryAdapterForSuperUser(EXPECTED_SCOPES))
                .thenReturn(client);
        when(client.listOrgUnits("/")).thenReturn(new OrgUnits());

        testSubject.discover(objectMapper, session, emitter);

        verify(metrics, never()).incrementDiscoveredCount();
        verify(emitter, never()).sendEnvelope(eq(GoogleOrgUnitDiscovery.SERVICE), eq(session), any());
        verify(metrics).recordFullDiscoveryTime(anyLong());
    }

    @Test
    void discover_traversesOrgUnitTree() {
        when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(true);
        DirectoryClientAdapter client = mock(DirectoryClientAdapter.class);
        when(clientAdapterFactory.createDirectoryAdapterForSuperUser(EXPECTED_SCOPES))
                .thenReturn(client);

        OrgUnitNode rootOu = new OrgUnitNode("/",
                new OrgUnitNode("random-ou",
                        new OrgUnitNode("nested-random-ou",
                                new OrgUnitNode("unicode-test"),
                                new OrgUnitNode("length-test"),
                                new OrgUnitNode("legal-char-test")
                        )
                ),
                new OrgUnitNode("frontline-users")
        );
        when(client.getOrgUnit("/")).thenReturn(rootOu.ou);
        rootOu.expectApiCalls(client);

        testSubject.discover(objectMapper, session, emitter);
        rootOu.verifyEmitCalls();
        verify(metrics, times(7)).incrementDiscoveredCount();
        verify(metrics, times(7)).recordBatchProcessingTime(anyLong());
        verify(metrics, times(1)).recordFullDiscoveryTime(anyLong());
    }

    private class OrgUnitNode {
        public final OrgUnit ou;
        public final OrgUnitNode[] children;
        public final GenericAsset asset;

        public OrgUnitNode(String ouId, OrgUnitNode... children) {
            this.ou = new OrgUnit().setOrgUnitId(ouId);
            this.children = children;
            for (var child : children) {
                child.ou.setParentOrgUnitId(ouId);
            }
            this.asset = new GenericAsset();
            this.asset.setAssetId(ouId);
        }

        public void expectApiCalls(DirectoryClientAdapter mockAdapter) {
            when(mockAdapter.listOrgUnits(ou.getOrgUnitId())).thenReturn(
                    new OrgUnits().setOrganizationUnits(
                            Arrays.stream(children).map(node -> node.ou).collect(Collectors.toList())
                    ));
            when(assetFactory.createOrgUnitResource(ou)).thenReturn(asset);
            for (OrgUnitNode node : children) {
                node.expectApiCalls(mockAdapter);
            }
        }

        public void verifyEmitCalls() {
            verify(emitter).sendEnvelope(GoogleOrgUnitDiscovery.SERVICE, session, asset);
            for (OrgUnitNode node : children) {
                node.verifyEmitCalls();
            }
        }
    }
}
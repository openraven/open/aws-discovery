/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.User;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.openraven.consumer.properties.GenericDiscoveryProperties;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.data.shared.GenericAsset;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.google.client.DriveClientAdapter;
import io.openraven.magpie.api.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GoogleMyDriveDiscoveryTest {
    protected static final List<String> EXPECTED_DIRECTORY_SCOPES = List.of(DirectoryScopes.ADMIN_DIRECTORY_USER_READONLY);
    protected static final List<String> EXPECTED_DRIVE_SCOPES = List.of(DriveScopes.DRIVE_READONLY);
    private static final int BATCH_SIZE = 3;
    @Mock
    ClientAdapterFactory clientAdapterFactory;
    @Mock
    GoogleWorkspaceAssetFactory assetFactory;
    @Mock
    MeterRegistry meterRegistry;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    Session session;
    @Mock
    GenericAssetEmitter emitter;
    @Mock
    GenericAssetDiscoveryMetrics myDriveMetrics;
    @Mock
    GenericAssetDiscoveryMetrics userMetrics;
    MockedStatic<GenericAssetDiscoveryMetrics> metricsStatic;

    GoogleMyDriveDiscovery testSubject;

    @BeforeEach
    void setUp() {
        metricsStatic = Mockito.mockStatic(GenericAssetDiscoveryMetrics.class);
        metricsStatic.when(() -> GenericAssetDiscoveryMetrics.create(meterRegistry, "google-my-drive"))
                .thenReturn(myDriveMetrics);
        metricsStatic.when(() -> GenericAssetDiscoveryMetrics.create(meterRegistry, "google-user"))
                .thenReturn(userMetrics);

        GenericDiscoveryProperties discoveryProperties = mock(GenericDiscoveryProperties.class, RETURNS_DEEP_STUBS);
        when(discoveryProperties.getBatchSize().getMyDrive()).thenReturn(BATCH_SIZE);

        testSubject = new GoogleMyDriveDiscovery(clientAdapterFactory,
                assetFactory,
                meterRegistry,
                discoveryProperties);
    }

    @AfterEach
    void tearDown() {
        metricsStatic.close();
    }

    @Test
    void doesNothingWhenCredentialConfigurationAbsent() {
        when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(false);

        testSubject.discover(objectMapper, session, emitter);

        verifyNoMoreInteractions(clientAdapterFactory, meterRegistry);
        verifyNoInteractions(myDriveMetrics);
        verifyNoInteractions(assetFactory, objectMapper, session, emitter);
    }

    @Nested
    class WhenCredentialConfigurationIsPresent {
        @Mock
        DirectoryClientAdapter directoryClient;
        @Mock
        Timer timer;

        @BeforeEach
        void setUp() {
            when(clientAdapterFactory.hasCredentialConfiguration()).thenReturn(true);
            when(clientAdapterFactory.createDirectoryAdapterForSuperUser(EXPECTED_DIRECTORY_SCOPES))
                    .thenReturn(directoryClient);
            when(userMetrics.getBatchProcessingTimer()).thenReturn(timer);
        }

        @Test
        void singleUserWithNoDrive() {
            User user = new User().setPrimaryEmail("user1@example.com");
            when(directoryClient.listAllUsers(BATCH_SIZE, timer)).thenReturn(List.of(user));

            GenericAsset userAsset = new GenericAsset();
            userAsset.setAssetId("user 1 id");
            when(assetFactory.createUserResource(user)).thenReturn(userAsset);

            var driveClient = mock(DriveClientAdapter.class);
            when(clientAdapterFactory.createDriveAdapterForUser(anyString(), eq(EXPECTED_DRIVE_SCOPES)))
                    .thenReturn(driveClient);
            when(driveClient.getAbout()).thenReturn(null);

            testSubject.discover(objectMapper, session, emitter);

            verify(emitter).sendEnvelope(GoogleMyDriveDiscovery.SERVICE, session, userAsset);
            verifyNoMoreInteractions(emitter);
            verify(userMetrics).incrementDiscoveredCount();
            verify(userMetrics).recordFullDiscoveryTime(anyLong());
            verify(myDriveMetrics).recordFullDiscoveryTime(anyLong());
            verifyNoMoreInteractions(myDriveMetrics);
        }

        @Test
        void singleUserWithDrive() {
            User user = new User().setPrimaryEmail("user1@example.com");
            when(directoryClient.listAllUsers(BATCH_SIZE, timer)).thenReturn(List.of(user));

            GenericAsset userAsset = new GenericAsset();
            userAsset.setAssetId("user 1 id");
            when(assetFactory.createUserResource(user)).thenReturn(userAsset);

            var driveClient = mock(DriveClientAdapter.class);
            when(clientAdapterFactory.createDriveAdapterForUser(anyString(), eq(EXPECTED_DRIVE_SCOPES)))
                    .thenReturn(driveClient);
            About about = new About();
            when(driveClient.getAbout()).thenReturn(about);

            GenericAsset driveAsset = new GenericAsset();
            driveAsset.setAssetId("user 1 drive id");
            when(assetFactory.createMyDriveResource(user, about)).thenReturn(driveAsset);


            testSubject.discover(objectMapper, session, emitter);

            verify(emitter).sendEnvelope(GoogleMyDriveDiscovery.SERVICE, session, userAsset);
            verify(emitter).sendEnvelope(GoogleMyDriveDiscovery.SERVICE, session, driveAsset);
            verify(userMetrics).incrementDiscoveredCount();
            verify(userMetrics).recordFullDiscoveryTime(anyLong());
            verify(myDriveMetrics).incrementDiscoveredCount();
            verify(myDriveMetrics).recordFullDiscoveryTime(anyLong());
        }
    }
}
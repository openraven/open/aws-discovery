/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.integrationtests;

public class IntegrationTestConstants {
    public static final String DATABASE_IMAGE_TAG_PROPERTY = "openraven.integration-test.database.image-tag=pg-with-schema.1212537262";
}

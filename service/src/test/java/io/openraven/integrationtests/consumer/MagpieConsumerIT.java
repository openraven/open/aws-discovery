/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.integrationtests.consumer;

import io.openraven.consumer.magpie.MagpieConsumer;
import io.openraven.integrationtests.IntegrationTestConstants;
import io.openraven.producer.properties.MagpieOpenravenProperties;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@SpringBootTest(properties = {
        IntegrationTestConstants.DATABASE_IMAGE_TAG_PROPERTY,
        MagpieOpenravenProperties.ZK_ROOT + ".kafka.consumer.topic=topic"
})
@ActiveProfiles({"default", "consumer", "integration-test", "aws"})
@EmbeddedKafka(partitions = 1,
        brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"},
        topics = {
                "topic"
        })
class MagpieConsumerIT {

    @MockBean
    EntityManager entityManager;

    @Autowired
    MagpieConsumer magpieConsumer;


    @Test
    void whenEntityManagerFails_thenHealthCheckFails_AlsoThrowsExceptionPreventingKafkaAck() {
        assertEquals(Health.up().build(), magpieConsumer.health());
        when(entityManager.getTransaction()).thenThrow(new RuntimeException());
        try {
            magpieConsumer.upsert(null);
            fail("Should have thrown runtime exception");
        } catch (RuntimeException e) {
            assertEquals(Health.down().build(), magpieConsumer.health());
        }
    }
}

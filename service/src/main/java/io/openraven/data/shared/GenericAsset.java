/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.shared;

import com.fasterxml.jackson.databind.JsonNode;

import java.time.Instant;
import java.util.Map;
import java.util.Objects;

public class GenericAsset {
    private String assetId;
    private String resourceName;
    private String resourceId;
    private String locationString;
    private String nameLabel;
    private String source;
    private String resourceType;
    private String resourceTypeLabel;
    private Instant createdIso;
    private Instant updatedIso;
    private Long storageInBytes;
    private Map<String, Object> locationBlob;
    private JsonNode configuration;
    private JsonNode supplementaryConfiguration;
    private String consoleLink;

    public GenericAsset() {
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Instant getCreatedIso() {
        return createdIso;
    }

    public void setCreatedIso(Instant createdIso) {
        this.createdIso = createdIso;
    }

    public Instant getUpdatedIso() {
        return updatedIso;
    }

    public void setUpdatedIso(Instant updatedIso) {
        this.updatedIso = updatedIso;
    }

    public JsonNode getConfiguration() {
        return configuration;
    }

    public void setConfiguration(JsonNode configuration) {
        this.configuration = configuration;
    }

    public JsonNode getSupplementaryConfiguration() {
        return supplementaryConfiguration;
    }

    public void setSupplementaryConfiguration(JsonNode supplementaryConfiguration) {
        this.supplementaryConfiguration = supplementaryConfiguration;
    }

    public Long getStorageInBytes() {
        return storageInBytes;
    }

    public void setStorageInBytes(Long storageInBytes) {
        this.storageInBytes = storageInBytes;
    }

    public Map<String, Object> getLocationBlob() {
        return locationBlob;
    }

    public void setLocationBlob(Map<String, Object> locationBlob) {
        this.locationBlob = locationBlob;
    }

    public String getLocationString() {
        return locationString;
    }

    public void setLocationString(String locationString) {
        this.locationString = locationString;
    }

    public String getNameLabel() {
        return nameLabel;
    }

    public void setNameLabel(String nameLabel) {
        this.nameLabel = nameLabel;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceTypeLabel() {
        return resourceTypeLabel;
    }

    public void setResourceTypeLabel(String resourceTypeLabel) {
        this.resourceTypeLabel = resourceTypeLabel;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getConsoleLink() {
        return consoleLink;
    }

    public void setConsoleLink(String consoleLink) {
        this.consoleLink = consoleLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericAsset that = (GenericAsset) o;
        return Objects.equals(assetId, that.assetId) && Objects.equals(resourceName, that.resourceName) && Objects.equals(resourceId, that.resourceId) && Objects.equals(locationString, that.locationString) && Objects.equals(nameLabel, that.nameLabel) && Objects.equals(source, that.source) && Objects.equals(resourceType, that.resourceType) && Objects.equals(resourceTypeLabel, that.resourceTypeLabel) && Objects.equals(createdIso, that.createdIso) && Objects.equals(updatedIso, that.updatedIso) && Objects.equals(storageInBytes, that.storageInBytes) && Objects.equals(locationBlob, that.locationBlob) && Objects.equals(configuration, that.configuration) && Objects.equals(supplementaryConfiguration, that.supplementaryConfiguration) && Objects.equals(consoleLink, that.consoleLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(assetId, resourceName, resourceId, locationString, nameLabel, source, resourceType, resourceTypeLabel, createdIso, updatedIso, storageInBytes, locationBlob, configuration, supplementaryConfiguration, consoleLink);
    }

    @Override
    public String toString() {
        return "GenericAsset{" +
                "assetId='" + assetId + '\'' +
                ", resourceName='" + resourceName + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", locationString='" + locationString + '\'' +
                ", nameLabel='" + nameLabel + '\'' +
                ", source='" + source + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", resourceTypeLabel='" + resourceTypeLabel + '\'' +
                ", createdIso=" + createdIso +
                ", updatedIso=" + updatedIso +
                ", storageInBytes=" + storageInBytes +
                ", locationBlob=" + locationBlob +
                ", configuration=" + configuration +
                ", supplementaryConfiguration=" + supplementaryConfiguration +
                ", consoleLink='" + consoleLink + '\'' +
                '}';
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.net.URL;

@ConfigurationProperties(prefix = SplunkServiceMagpieProperties.CONFIG_PREFIX)
@RefreshScope
public class SplunkServiceMagpieProperties {
    public static final String CONFIG_PREFIX = "openraven.app.v1.services.splunk";

    private URL eventEndpointUrl;

    private String authToken;

    public URL getEventEndpointUrl() {
        return eventEndpointUrl;
    }

    public void setEventEndpointUrl(URL eventEndpointUrl) {
        this.eventEndpointUrl = eventEndpointUrl;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

}

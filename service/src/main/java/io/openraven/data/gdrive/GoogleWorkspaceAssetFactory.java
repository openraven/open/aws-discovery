/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.api.client.json.GenericJson;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.Customer;
import com.google.api.services.directory.model.Domains;
import com.google.api.services.directory.model.Group;
import com.google.api.services.directory.model.Member;
import com.google.api.services.directory.model.OrgUnit;
import com.google.api.services.directory.model.User;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.Drive;
import com.google.api.services.drive.model.TeamDrive;
import io.openraven.data.shared.GenericAsset;
import io.openraven.google.client.ClientAdapterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Map;

@Component
public class GoogleWorkspaceAssetFactory {
    public static final String GOOGLE_WORKSPACE_SOURCE = "google-workspace";
    protected static final List<String> SCOPES = List.of(DirectoryScopes.ADMIN_DIRECTORY_CUSTOMER_READONLY);
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleWorkspaceAssetFactory.class);
    private static final String DRIVE_CONSOLE_LINK = "https://drive.google.com/drive/u/0/folders/%s";
    private final ObjectMapper objectMapper;
    private final ClientAdapterFactory clientAdapterFactory;

    private String workspaceName;

    public GoogleWorkspaceAssetFactory(ObjectMapper objectMapper,
                                       ClientAdapterFactory clientAdapterFactory) {
        this.objectMapper = objectMapper;
        this.clientAdapterFactory = clientAdapterFactory;
    }

    public GenericAsset createMyDriveResource(User user, About about) {
        var asset = new GenericAsset();
        asset.setAssetId("google:my-drive:%s".formatted(user.getId()));
        String nameLabel = "My Drive %s".formatted(user.getPrimaryEmail());
        asset.setResourceName(nameLabel);
        asset.setResourceId(user.getId());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(nameLabel);
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::MyDrive");
        asset.setResourceTypeLabel("Google My Drive");
        asset.setCreatedIso(Instant.ofEpochMilli(user.getCreationTime().getValue()));
        asset.setConfiguration(asJsonNode(user));
        ObjectNode supplementaryConfiguration = objectMapper.createObjectNode();
        supplementaryConfiguration.set("about", asJsonNode(about));
        asset.setStorageInBytes(about.getStorageQuota().getUsageInDrive());
        asset.setSupplementaryConfiguration(supplementaryConfiguration);
        asset.setConsoleLink("https://drive.google.com/drive/u/0/my-drive");
        return asset;
    }

    public GenericAsset createSharedDriveResource(Drive drive) {
        var asset = new GenericAsset();
        asset.setAssetId("google:shared-drive:%s".formatted(drive.getId()));
        asset.setResourceName(drive.getName());
        asset.setResourceId(drive.getId());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(drive.getName());
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::SharedDrive");
        asset.setResourceTypeLabel("Google Shared Drive");
        asset.setCreatedIso(Instant.ofEpochMilli(drive.getCreatedTime().getValue()));
        asset.setConfiguration(asJsonNode(drive));
        asset.setConsoleLink(DRIVE_CONSOLE_LINK.formatted(drive.getId()));
        return asset;
    }

    public GenericAsset createTeamDriveResource(TeamDrive drive) {
        var asset = new GenericAsset();
        asset.setAssetId("google:team-drive:%s".formatted(drive.getId()));
        asset.setResourceName(drive.getName());
        asset.setResourceId(drive.getId());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(drive.getName());
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::TeamDrive");
        asset.setResourceTypeLabel("Google Team Drive");
        asset.setCreatedIso(Instant.ofEpochMilli(drive.getCreatedTime().getValue()));
        asset.setConfiguration(asJsonNode(drive));
        asset.setConsoleLink(DRIVE_CONSOLE_LINK.formatted(drive.getId()));
        return asset;
    }

    private String getLocationString() {
        return "google-workspace:%s".formatted(getWorkspaceName());
    }

    private String getWorkspaceName() {
        // Cache workspaceName as it is unlikely to change during a single discovery
        if (workspaceName == null) {
            var client = clientAdapterFactory.createDirectoryAdapterForSuperUser(SCOPES);
            Customer myCustomer = client.getMyCustomer();
            if (myCustomer == null) {
                throw new RuntimeException("Failed to retrieve customer info for workspace name");
            }
            workspaceName = myCustomer.getPostalAddress().getOrganizationName();
        }
        return workspaceName;
    }

    public GenericAsset createOrgUnitResource(OrgUnit ou) {
        var asset = new GenericAsset();
        asset.setAssetId("google:org-unit:%s".formatted(ou.getOrgUnitId()));
        asset.setResourceName(ou.getName());
        asset.setResourceId(ou.getOrgUnitId());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(ou.getName());
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::OrgUnit");
        asset.setResourceTypeLabel("Google Workspace Organizational Unit");
        asset.setConfiguration(asJsonNode(ou));
        asset.setLocationBlob(Map.of("orgUnitPath", ou.getOrgUnitPath()));
        return asset;
    }

    public GenericAsset createUserResource(User user) {
        var asset = new GenericAsset();
        asset.setAssetId("google:user:%s".formatted(user.getId()));
        String nameLabel = "User %s".formatted(user.getPrimaryEmail());
        asset.setResourceName(nameLabel);
        asset.setResourceId(user.getId());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(nameLabel);
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::User");
        asset.setResourceTypeLabel("Google User");
        asset.setCreatedIso(Instant.ofEpochMilli(user.getCreationTime().getValue()));
        asset.setConfiguration(asJsonNode(user));
        return asset;
    }

    public GenericAsset createGroupResource(Group group) {
        var asset = new GenericAsset();
        asset.setAssetId("google:group:%s".formatted(group.getId()));
        String nameLabel = "Group %s".formatted(group.getName());
        asset.setResourceName(nameLabel);
        asset.setResourceId(group.getId());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(nameLabel);
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::Group");
        asset.setResourceTypeLabel("Google Group");
        asset.setConfiguration(asJsonNode(group));
        return asset;
    }

    public GenericAsset createGroupMemberResource(Group group, Member member) {
        var asset = new GenericAsset();
        asset.setAssetId("google:group-member:%s:%s".formatted(group.getId(), member.getId()));
        String nameLabel = "Group %s Member %s".formatted(group.getName(), member.getEmail());
        asset.setResourceName(nameLabel);
        asset.setResourceId(member.getId());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(nameLabel);
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::GroupMember");
        asset.setResourceTypeLabel("Google Group Member");
        asset.setConfiguration(asJsonNode(member));
        asset.setSupplementaryConfiguration(asJsonNode(group));
        return asset;
    }

    public GenericAsset createDomainResource(Domains domain) {
        var asset = new GenericAsset();
        asset.setAssetId("google:domain:%s".formatted(domain.getDomainName()));
        String nameLabel = "Domain %s".formatted(domain.getDomainName());
        asset.setResourceName(nameLabel);
        asset.setResourceId(domain.getDomainName());
        asset.setLocationString(getLocationString());
        asset.setNameLabel(nameLabel);
        asset.setSource(GOOGLE_WORKSPACE_SOURCE);
        asset.setResourceType("GOOGLEWORKSPACE::Domain");
        asset.setResourceTypeLabel("Google Domain");
        asset.setCreatedIso(Instant.ofEpochMilli(domain.getCreationTime()));
        asset.setConfiguration(asJsonNode(domain));
        return asset;
    }

    private JsonNode asJsonNode(GenericJson input) {
        try {
            String inputJson = input.toPrettyString();
            LOGGER.debug("{}.toPrettyString = {}", input.getClass().getSimpleName(), inputJson);
            return objectMapper.readTree(inputJson);
        } catch (IOException e) {
            throw new RuntimeException("failed to transcode json from google to jackson", e);
        }
    }
}

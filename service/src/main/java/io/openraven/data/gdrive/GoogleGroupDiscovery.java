/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.Group;
import io.micrometer.core.instrument.MeterRegistry;
import io.openraven.consumer.properties.GenericDiscoveryProperties;
import io.openraven.data.genericasset.GenericAssetDiscovery;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.magpie.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile(GoogleGroupDiscovery.SERVICE)
public class GoogleGroupDiscovery implements GenericAssetDiscovery {
    public static final String SERVICE = "googleGroup";
    protected static final List<String> SCOPES = List.of(
            DirectoryScopes.ADMIN_DIRECTORY_GROUP_READONLY,
            DirectoryScopes.ADMIN_DIRECTORY_GROUP_MEMBER_READONLY);
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleGroupDiscovery.class);
    private final ClientAdapterFactory clientAdapterFactory;
    private final GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory;
    private final GenericAssetDiscoveryMetrics groupMetrics;
    private final GenericAssetDiscoveryMetrics memberMetrics;
    private final int groupBatchSize;
    private final int memberBatchSize;

    public GoogleGroupDiscovery(
            ClientAdapterFactory clientAdapterFactory,
            GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory,
            MeterRegistry meterRegistry,
            GenericDiscoveryProperties genericDiscoveryProperties
    ) {
        this.clientAdapterFactory = clientAdapterFactory;
        this.googleWorkspaceAssetFactory = googleWorkspaceAssetFactory;
        this.groupMetrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-group");
        this.memberMetrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-group-member");
        this.groupBatchSize = genericDiscoveryProperties.getBatchSize().getGroup();
        this.memberBatchSize = genericDiscoveryProperties.getBatchSize().getGroupMember();
    }

    @Override
    public String service() {
        return SERVICE;
    }

    @Override
    public void discover(ObjectMapper mapper, Session session, GenericAssetEmitter emitter) {
        if (!clientAdapterFactory.hasCredentialConfiguration()) {
            LOGGER.debug("No GCP credentials configured, skipping discovery");
            return;
        }
        long startTime = System.currentTimeMillis();
        var client = clientAdapterFactory.createDirectoryAdapterForSuperUser(SCOPES);
        discoverGroups(client, session, emitter);
        long elapsedMillis = System.currentTimeMillis() - startTime;
        groupMetrics.recordFullDiscoveryTime(elapsedMillis);
        memberMetrics.recordFullDiscoveryTime(elapsedMillis);
    }

    private void discoverGroups(DirectoryClientAdapter client, Session session, GenericAssetEmitter emitter) {
        for (var group : client.listAllGroups(groupBatchSize, groupMetrics.getBatchProcessingTimer())) {
            emitter.sendEnvelope(service(), session, googleWorkspaceAssetFactory.createGroupResource(group));
            groupMetrics.incrementDiscoveredCount();
            discoverGroupMembers(client, session, emitter, group);
        }
    }

    private void discoverGroupMembers(DirectoryClientAdapter client, Session session, GenericAssetEmitter emitter, Group group) {
        for (var member : client.listAllMembers(group.getId(), memberBatchSize, memberMetrics.getBatchProcessingTimer())) {
            emitter.sendEnvelope(service(), session, googleWorkspaceAssetFactory.createGroupMemberResource(group, member));
            memberMetrics.incrementDiscoveredCount();
        }
    }
}

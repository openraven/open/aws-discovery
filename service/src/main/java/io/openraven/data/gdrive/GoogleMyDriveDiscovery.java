/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import io.micrometer.core.instrument.MeterRegistry;
import io.openraven.consumer.properties.GenericDiscoveryProperties;
import io.openraven.data.genericasset.GenericAssetDiscovery;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.magpie.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@Profile(GoogleMyDriveDiscovery.SERVICE)
public class GoogleMyDriveDiscovery implements GenericAssetDiscovery {
    public static final String SERVICE = "googleMyDrive";
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleMyDriveDiscovery.class);

    private final ClientAdapterFactory clientAdapterFactory;
    private final GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory;
    private final GenericAssetDiscoveryMetrics userMetrics;
    private final GenericAssetDiscoveryMetrics myDriveMetrics;
    private final int batchSize;

    public GoogleMyDriveDiscovery(
            ClientAdapterFactory clientAdapterFactory,
            GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory,
            MeterRegistry meterRegistry,
            GenericDiscoveryProperties genericDiscoveryProperties
    ) {
        this.clientAdapterFactory = clientAdapterFactory;
        this.googleWorkspaceAssetFactory = googleWorkspaceAssetFactory;
        this.userMetrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-user");
        this.myDriveMetrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-my-drive");
        this.batchSize = genericDiscoveryProperties.getBatchSize().getMyDrive();
    }

    @Override
    public String service() {
        return SERVICE;
    }

    @Override
    public void discover(ObjectMapper mapper, Session session, GenericAssetEmitter emitter) {
        if (!clientAdapterFactory.hasCredentialConfiguration()) {
            LOGGER.debug("No GCP credentials configured, skipping discovery");
            return;
        }
        long fullDiscoveryStartTime = System.currentTimeMillis();
        var client = clientAdapterFactory.createDirectoryAdapterForSuperUser(List.of(DirectoryScopes.ADMIN_DIRECTORY_USER_READONLY));
        discoverMyDrives(client, session, emitter);
        long fullDiscoveryEndTime = System.currentTimeMillis() - fullDiscoveryStartTime;
        userMetrics.recordFullDiscoveryTime(fullDiscoveryEndTime);
        myDriveMetrics.recordFullDiscoveryTime(fullDiscoveryEndTime);
    }

    private void discoverMyDrives(DirectoryClientAdapter client, Session session, GenericAssetEmitter emitter) {
        for (var u : client.listAllUsers(batchSize, userMetrics.getBatchProcessingTimer())) {
            emitter.sendEnvelope(service(), session, googleWorkspaceAssetFactory.createUserResource(u));
            userMetrics.incrementDiscoveredCount();
            About userAbout = clientAdapterFactory.createDriveAdapterForUser(
                    u.getPrimaryEmail(),
                    Collections.singletonList(DriveScopes.DRIVE_READONLY)
            ).getAbout();
            if (userAbout != null) {
                emitter.sendEnvelope(service(),
                        session,
                        googleWorkspaceAssetFactory.createMyDriveResource(u, userAbout));
                myDriveMetrics.incrementDiscoveredCount();
            }
        }
    }
}

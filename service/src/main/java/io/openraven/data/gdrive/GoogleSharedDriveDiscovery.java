/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.drive.DriveScopes;
import io.micrometer.core.instrument.MeterRegistry;
import io.openraven.consumer.properties.GenericDiscoveryProperties;
import io.openraven.data.genericasset.GenericAssetDiscovery;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DriveClientAdapter;
import io.openraven.magpie.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile(GoogleSharedDriveDiscovery.SERVICE)
public class GoogleSharedDriveDiscovery implements GenericAssetDiscovery {
    public static final String SERVICE = "googleSharedDrive";
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleSharedDriveDiscovery.class);

    private final ClientAdapterFactory clientAdapterFactory;
    private final GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory;
    private final GenericAssetDiscoveryMetrics sharedDriveMetrics;
    private final GenericAssetDiscoveryMetrics teamDriveMetrics;
    private final int batchSize;

    public GoogleSharedDriveDiscovery(
            ClientAdapterFactory clientAdapterFactory,
            GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory,
            MeterRegistry meterRegistry,
            GenericDiscoveryProperties genericDiscoveryProperties
    ) {
        this.clientAdapterFactory = clientAdapterFactory;
        this.googleWorkspaceAssetFactory = googleWorkspaceAssetFactory;
        this.sharedDriveMetrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-shared-drive");
        this.teamDriveMetrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-team-drive");
        this.batchSize = genericDiscoveryProperties.getBatchSize().getSharedDrive();
    }

    @Override
    public String service() {
        return SERVICE;
    }

    @Override
    public void discover(ObjectMapper mapper, Session session, GenericAssetEmitter emitter) {
        if (!clientAdapterFactory.hasCredentialConfiguration()) {
            LOGGER.debug("No GCP credentials configured, skipping discovery");
            return;
        }
        long startTime = System.currentTimeMillis();
        var client = clientAdapterFactory.createDriveAdapterForSuperUser(List.of(DriveScopes.DRIVE_READONLY));
        discoverSharedDrives(client, session, emitter);
        sharedDriveMetrics.recordFullDiscoveryTime(System.currentTimeMillis() - startTime);
        startTime = System.currentTimeMillis();
        discoverTeamDrives(client, session, emitter);
        teamDriveMetrics.recordFullDiscoveryTime(System.currentTimeMillis() - startTime);
    }

    private void discoverSharedDrives(DriveClientAdapter client, Session session, GenericAssetEmitter emitter) {
        for (var d : client.listAllDrives(batchSize, sharedDriveMetrics.getBatchProcessingTimer())) {
            emitter.sendEnvelope(service(), session, googleWorkspaceAssetFactory.createSharedDriveResource(d));
            sharedDriveMetrics.incrementDiscoveredCount();
        }
    }

    private void discoverTeamDrives(DriveClientAdapter client, Session session, GenericAssetEmitter emitter) {
        for (var d : client.listAllTeamDrives(batchSize, teamDriveMetrics.getBatchProcessingTimer())) {
            emitter.sendEnvelope(service(), session, googleWorkspaceAssetFactory.createTeamDriveResource(d));
            teamDriveMetrics.incrementDiscoveredCount();
        }
    }

}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.OrgUnit;
import com.google.api.services.directory.model.OrgUnits;
import io.micrometer.core.instrument.MeterRegistry;
import io.openraven.data.genericasset.GenericAssetDiscovery;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.magpie.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile(GoogleOrgUnitDiscovery.SERVICE)
public class GoogleOrgUnitDiscovery implements GenericAssetDiscovery {
    public static final String SERVICE = "googleOrgUnit";
    protected static final String ROOT_OU_PATH = "/";
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleOrgUnitDiscovery.class);
    private final ClientAdapterFactory clientAdapterFactory;
    private final GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory;
    private final GenericAssetDiscoveryMetrics metrics;

    public GoogleOrgUnitDiscovery(
            ClientAdapterFactory clientAdapterFactory,
            GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory,
            MeterRegistry meterRegistry
    ) {
        this.clientAdapterFactory = clientAdapterFactory;
        this.googleWorkspaceAssetFactory = googleWorkspaceAssetFactory;
        this.metrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-org-unit");
    }

    @Override
    public String service() {
        return SERVICE;
    }

    @Override
    public void discover(ObjectMapper mapper, Session session, GenericAssetEmitter emitter) {
        if (!clientAdapterFactory.hasCredentialConfiguration()) {
            LOGGER.debug("No GCP credentials configured, skipping discovery");
            return;
        }
        long fullDiscoveryStartTime = System.currentTimeMillis();
        var client = clientAdapterFactory.createDirectoryAdapterForSuperUser(List.of(DirectoryScopes.ADMIN_DIRECTORY_ORGUNIT_READONLY));
        LOGGER.debug("Starting discovery of org units, getting children of root OU");
        discoverChildOrgUnits(client, session, emitter, ROOT_OU_PATH);
        metrics.recordFullDiscoveryTime(System.currentTimeMillis() - fullDiscoveryStartTime);
    }

    private void discoverChildOrgUnits(DirectoryClientAdapter client,
                                       Session session,
                                       GenericAssetEmitter emitter,
                                       String orgUnitId) {
        long batchStartTime = System.currentTimeMillis();
        LOGGER.debug("Listing orgUnits under id: {}", orgUnitId);
        OrgUnits ouList = client.listOrgUnits(orgUnitId);
        // If we get a null response, the provided orgUnitId has no children
        if (ouList == null || ouList.getOrganizationUnits() == null) {
            if (orgUnitId.equals(ROOT_OU_PATH)) {
                LOGGER.warn("No child OUs found at root. Root OU info cannot be discovered.");
            }
            return;
        }
        LOGGER.debug("children of {} - {}", orgUnitId, ouList);
        // The only way to get the OU info for the root OU is if you already know its id,
        // so we must handle root as a special case here
        if (orgUnitId.equals(ROOT_OU_PATH)) {
            String parentOrgUnitId = ouList.getOrganizationUnits().get(0).getParentOrgUnitId();
            LOGGER.debug("Looking up root OU {}", parentOrgUnitId);
            var rootOu = client.getOrgUnit(parentOrgUnitId);
            emitOrgUnit(session, emitter, rootOu);
        }
        for (var ou : ouList.getOrganizationUnits()) {
            emitOrgUnit(session, emitter, ou);
            discoverChildOrgUnits(client, session, emitter, ou.getOrgUnitId());
        }
        metrics.recordBatchProcessingTime(System.currentTimeMillis() - batchStartTime);
    }

    private void emitOrgUnit(Session session, GenericAssetEmitter emitter, OrgUnit ou) {
        emitter.sendEnvelope(service(), session, googleWorkspaceAssetFactory.createOrgUnitResource(ou));
        metrics.incrementDiscoveredCount();
    }
}

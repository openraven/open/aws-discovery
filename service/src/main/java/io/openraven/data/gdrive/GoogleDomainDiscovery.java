/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.gdrive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.directory.DirectoryScopes;
import com.google.api.services.directory.model.Domains;
import io.micrometer.core.instrument.MeterRegistry;
import io.openraven.data.genericasset.GenericAssetDiscovery;
import io.openraven.data.genericasset.GenericAssetDiscoveryMetrics;
import io.openraven.data.genericasset.GenericAssetEmitter;
import io.openraven.google.client.ClientAdapterFactory;
import io.openraven.google.client.DirectoryClientAdapter;
import io.openraven.magpie.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile(GoogleDomainDiscovery.SERVICE)
public class GoogleDomainDiscovery implements GenericAssetDiscovery {
    public static final String SERVICE = "googleDomain";
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleDomainDiscovery.class);

    private final ClientAdapterFactory clientAdapterFactory;
    private final GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory;
    private final GenericAssetDiscoveryMetrics metrics;

    public GoogleDomainDiscovery(
            ClientAdapterFactory clientAdapterFactory,
            GoogleWorkspaceAssetFactory googleWorkspaceAssetFactory,
            MeterRegistry meterRegistry
    ) {
        this.clientAdapterFactory = clientAdapterFactory;
        this.googleWorkspaceAssetFactory = googleWorkspaceAssetFactory;
        this.metrics = GenericAssetDiscoveryMetrics.create(meterRegistry, "google-domain");
    }

    @Override
    public String service() {
        return SERVICE;
    }

    @Override
    public void discover(ObjectMapper mapper, Session session, GenericAssetEmitter emitter) {
        if (!clientAdapterFactory.hasCredentialConfiguration()) {
            LOGGER.debug("No GCP credentials configured, skipping discovery");
            return;
        }
        long fullDiscoveryStartTime = System.currentTimeMillis();
        var client = clientAdapterFactory.createDirectoryAdapterForSuperUser(List.of(DirectoryScopes.ADMIN_DIRECTORY_DOMAIN_READONLY));
        LOGGER.debug("Starting discovery of domains");
        discoverDomains(client, session, emitter);
        metrics.recordFullDiscoveryTime(System.currentTimeMillis() - fullDiscoveryStartTime);
    }

    private void discoverDomains(DirectoryClientAdapter client,
                                 Session session,
                                 GenericAssetEmitter emitter) {
        LOGGER.debug("Listing domains");
        List<Domains> domains = client.listDomains();
        // If we get a null response, there are no domains
        if (domains == null) {
            return;
        }
        for (var domain : domains) {
            emitter.sendEnvelope(service(), session, googleWorkspaceAssetFactory.createDomainResource(domain));
            metrics.incrementDiscoveredCount();
        }
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.genericasset;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.openraven.consumer.properties.GenericAssetsConsumerProperties;
import io.openraven.data.shared.GenericAsset;
import io.openraven.magpie.api.MagpieEnvelope;
import io.openraven.magpie.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Map;

@Component
public class GenericAssetEmitter {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericAssetEmitter.class);

    private final ObjectMapper objectMapper;
    private final KafkaOperations<Long, String> kafkaOperations;
    private final GenericAssetsConsumerProperties properties;

    public GenericAssetEmitter(
            ObjectMapper objectMapper,
            KafkaOperations<Long, String> kafkaOperations,
            GenericAssetsConsumerProperties properties
    ) {
        this.objectMapper = objectMapper;
        this.kafkaOperations = kafkaOperations;
        this.properties = properties;
    }

    public void sendEnvelope(String service, Session discoverySession, GenericAsset data) {
        data.setUpdatedIso(Instant.now());
        var retVal = new MagpieEnvelope();
        retVal.setSession(discoverySession);
        var metadata = Map.of("service", service);
        retVal.setMetadata(metadata);
        var contents = this.objectMapper.convertValue(data, ObjectNode.class);
        retVal.setContents(contents);
        try {
            kafkaOperations.send(properties.getConsumer().getTopic(), objectMapper.writeValueAsString(retVal));
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to serialize generic asset {}", data.getAssetId(), e);
        }
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.genericasset;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.openraven.magpie.api.Session;
import io.openraven.producer.services.DiscoveryServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GenericAssetOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericAssetOperator.class);
    private final DiscoveryServices discoveryServices;
    private final GenericAssetEmitter genericAssetEmitter;
    private final ObjectMapper objectMapper;
    private final Session session;
    private final List<GenericAssetDiscovery> discoveryList;

    public GenericAssetOperator(
            List<GenericAssetDiscovery> discoveryList,
            GenericAssetEmitter genericAssetEmitter,
            DiscoveryServices discoveryServices,
            ObjectMapper objectMapper
    ) {
        this.session = new Session();
        this.genericAssetEmitter = genericAssetEmitter;
        this.discoveryServices = discoveryServices;
        this.objectMapper = objectMapper;
        this.discoveryList = discoveryList;
    }

    public void discoveryRun() {
        try {
            if (discoveryList.isEmpty()) {
                LOGGER.debug("No generic asset profiles to discover");
            } else {
                LOGGER.info(
                        "Starting generic asset discovery for profiles {}",
                        discoveryList.stream()
                                .map(GenericAssetDiscovery::service)
                                .collect(Collectors.joining(", "))
                );
                discoveryList.forEach(discovery -> discovery.discover(objectMapper, session, genericAssetEmitter));
            }
        } finally {
            discoveryServices.flushKafka();
        }
    }
}

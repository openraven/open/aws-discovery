/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.genericasset;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;

import java.util.concurrent.TimeUnit;

public class GenericAssetDiscoveryMetrics {
    private final Timer batchProcessingTimer;
    private final Counter discoveredCounter;
    private final Timer fullDiscoveryTimer;
    private GenericAssetDiscoveryMetrics(MeterRegistry meterRegistry, String service) {
        this.batchProcessingTimer = meterRegistry.timer(

                "aws-discovery.generic-discovery.%s.batch-processing-time".formatted(
                        service
                )
        );
        this.discoveredCounter = meterRegistry.counter(

                "aws-discovery.generic-discovery.%s.discovered-counter".formatted(
                        service
                )
        );
        this.fullDiscoveryTimer = meterRegistry.timer(


                "aws-discovery.generic-discovery.%s.full-discovery-time".formatted(
                        service
                ).formatted()
        );
    }

    public static GenericAssetDiscoveryMetrics create(MeterRegistry meterRegistry, String service) {
        return new GenericAssetDiscoveryMetrics(meterRegistry, service);
    }

    public void recordBatchProcessingTime(long millis) {
        batchProcessingTimer.record(millis, TimeUnit.MILLISECONDS);
    }

    public void incrementDiscoveredCount() {
        discoveredCounter.increment();
    }

    public void recordFullDiscoveryTime(long millis) {
        fullDiscoveryTimer.record(millis, TimeUnit.MILLISECONDS);
    }

    public Timer getBatchProcessingTimer() {
        return batchProcessingTimer;
    }
}

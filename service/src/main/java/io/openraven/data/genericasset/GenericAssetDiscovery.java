/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.data.genericasset;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.openraven.magpie.api.Session;

public interface GenericAssetDiscovery {
    void discover(
            ObjectMapper mapper,
            Session session,
            GenericAssetEmitter emitter
    );

    String service();
}

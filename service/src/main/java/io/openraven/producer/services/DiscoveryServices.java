/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.micrometer.core.instrument.Counter;
import io.openraven.magpie.api.MagpieEnvelope;
import io.sentry.Sentry;
import io.sentry.SentryEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Component
public class DiscoveryServices {
    private static final Logger LOG = LoggerFactory.getLogger(DiscoveryServices.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final ObjectWriter OBJECT_WRITER;

    static {
        MAPPER
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true)
        ;
        MAPPER.registerModule(new JavaTimeModule());
        OBJECT_WRITER = MAPPER.writer();
    }

    private final KafkaOperations<Long, String> producer;
    private final boolean synchronousSend;

    @Autowired
    private DiscoveryServices(final KafkaOperations<Long, String> kafka) {
        this(kafka, true);
    }

    DiscoveryServices(KafkaOperations<Long, String> kafka, boolean synchronousSend) {
        this.producer = kafka;
        this.synchronousSend = synchronousSend;
    }

    public void sendToKafka(MagpieEnvelope envelope, String resourceID, Counter counter) {
        final String json;
        counter.increment();
        try {
            json = OBJECT_WRITER.writeValueAsString(envelope);
            LOG.trace("Sending envelope to Kafka - {}", json);

            final Future<SendResult<Long, String>> future = producer.sendDefault(json);
            if (synchronousSend) {
                future.get(10, TimeUnit.SECONDS);
            }
        } catch (JsonProcessingException | ExecutionException | TimeoutException | InterruptedException e) {
            LOG.error("Error processing resource {} with ex:", resourceID, e);
            Sentry.captureEvent(new SentryEvent(e));
        }
    }

    public void flushKafka() {
        producer.flush();
    }


}

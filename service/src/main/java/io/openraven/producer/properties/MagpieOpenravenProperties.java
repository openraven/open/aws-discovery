/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.properties;

import io.openraven.consumer.properties.AwsAssetsConsumerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
@ConfigurationProperties(MagpieOpenravenProperties.ZK_ROOT)
public class MagpieOpenravenProperties {
    public static final String ZK_ROOT = "openraven.app.v1.magpie";

    AwsAssetsConsumerProperties kafka;

    AwsAssetsConsumerProperties gcpKafka;

    public AwsAssetsConsumerProperties getKafka() {
        return kafka;
    }

    public void setKafka(AwsAssetsConsumerProperties kafka) {
        this.kafka = kafka;
    }

    public AwsAssetsConsumerProperties getGcpKafka() {
        return gcpKafka;
    }

    public void setGcpKafka(AwsAssetsConsumerProperties gcpKafka) {
        this.gcpKafka = gcpKafka;
    }
}

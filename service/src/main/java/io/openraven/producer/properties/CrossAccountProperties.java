/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@ConfigurationProperties(CrossAccountProperties.CONFIG_PREFIX)
@RefreshScope
public class CrossAccountProperties {
    public static final String CONFIG_PREFIX = "openraven.accounts.crossaccount.config";

    String externalID;

    String proxyExternalID;

    String proxyRoleArn;

    public CrossAccountProperties() {
    }

    public String getExternalID() {
        return externalID;
    }

    public void setExternalID(final String externalID) {
        this.externalID = externalID;
    }

    public String getProxyExternalID() {
        return proxyExternalID;
    }

    public void setProxyExternalID(String proxyExternalID) {
        this.proxyExternalID = proxyExternalID;
    }

    public String getProxyRoleArn() {
        return proxyRoleArn;
    }

    public void setProxyRoleArn(String proxyRoleArn) {
        this.proxyRoleArn = proxyRoleArn;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@ConfigurationProperties(prefix = "openraven.app.v1.object-discovery")
@RefreshScope
public class ObjectDiscoveryProperties {

    private String[] additionalTypeDetection = new String[0];

    public String[] getAdditionalTypeDetection() {
        return additionalTypeDetection;
    }

    public void setAdditionalTypeDetection(String[] additionalTypeDetection) {
        this.additionalTypeDetection = additionalTypeDetection;
    }
}
/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.scheduling;

import io.openraven.producer.properties.SchedulingProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Profile("producer")
public class SchedulingEventsListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulingEventsListener.class);

    private final OpenravenSchedulerConfigurer schedulerConfigurer;
    private final SchedulingProperties schedulingProperties;
    private final ScheduledDiscovery scheduledDiscovery;
    private final TaskExecutor taskExecutor;

    public SchedulingEventsListener(OpenravenSchedulerConfigurer schedulerConfigurer,
                                    SchedulingProperties schedulingProperties,
                                    ScheduledDiscovery scheduledDiscovery,
                                    TaskExecutor taskExecutor) {
        this.schedulerConfigurer = schedulerConfigurer;
        this.schedulingProperties = schedulingProperties;
        this.taskExecutor = taskExecutor;
        this.scheduledDiscovery = scheduledDiscovery;
    }

    @EventListener({ApplicationReadyEvent.class})
    public void refresh() {
        if (!schedulingProperties.isExitAfterFirstRun()) {
            schedulerConfigurer.schedule();
        }
    }

    @EventListener(ApplicationReadyEvent.class)
    public void discoverOnStartMaybe(ApplicationReadyEvent event) {
        final ConfigurableApplicationContext applicationContext = event.getApplicationContext();
        if (Arrays.asList(applicationContext.getEnvironment().getActiveProfiles()).contains("consumer")) {
            LOGGER.info("Avoiding the run-on-start,exit-on-start business due to running in 'consumer' mode");
            return;
        }
        if (schedulingProperties.isRunOnStart()) {
            taskExecutor.execute(() -> {
                if (schedulingProperties.isExitAfterFirstRun()) {
                    scheduledDiscovery.discover();
                    System.exit(SpringApplication.exit(applicationContext));
                } else {
                    scheduledDiscovery.discoverAsync();
                }
            });
        }
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.scheduling;

import io.openraven.data.genericasset.GenericAssetOperator;
import io.openraven.producer.magpie.MagpieOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

@Component
@Profile("cronJob")
public class ScheduledDiscovery {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduledDiscovery.class);

    private final AsyncTaskExecutor taskExecutor;
    private final MagpieOperations magpieOperator;
    private final GenericAssetOperator genericAssetOperator;

    boolean running = false;

    public ScheduledDiscovery(
            AsyncTaskExecutor taskExecutor,
            MagpieOperations magpieOperator,
            @Autowired(required = false)
            GenericAssetOperator genericAssetOperator) {
        this.taskExecutor = taskExecutor;
        this.magpieOperator = magpieOperator;
        this.genericAssetOperator = genericAssetOperator;
    }

    public void discover() {
        doDiscovery(true);
    }

    private void doDiscovery(boolean blocking) {
        synchronized (this) {
            if (running) {
                LOG.debug("Not running cause currently running");
                return;
            }
            LOG.debug("Setting running to true");
            running = true;
        }
        try {
            LOG.debug("Running discovery from scheduled service");
            final Future<?> submit = taskExecutor.submit(this::schedulingRunnable);
            if (blocking) {
                LOG.debug("Waiting on future");
                submit.get();
                LOG.debug("Discovery complete");
            } else {
                LOG.debug("Not waiting for discovery to complete");
            }
        } catch (Exception e) {
            LOG.error("Failed to complete discovery during scheduled execution", e);
        }
    }

    public void discoverAsync() {
        this.doDiscovery(false);
    }

    private void schedulingRunnable() {
        magpieOperator.discover();
        genericAssetOperator.discoveryRun();
        synchronized (this) {
            LOG.debug("Setting running to false");
            running = false;
        }
    }
}

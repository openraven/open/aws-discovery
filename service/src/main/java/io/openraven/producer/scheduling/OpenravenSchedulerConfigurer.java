/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.scheduling;

import io.openraven.producer.properties.SchedulingProperties;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZonedDateTime;

@Component
@RefreshScope
@Profile("cronJob")
public class OpenravenSchedulerConfigurer implements SchedulingConfigurer {

    private static final Logger LOG = LoggerFactory.getLogger(OpenravenSchedulerConfigurer.class);
    private final SchedulingProperties schedulingProperties;
    private final ScheduledDiscovery scheduledDiscovery;
    private ScheduledTaskRegistrar scheduledTaskRegistrar;


    public OpenravenSchedulerConfigurer(SchedulingProperties schedulingProperties,
                                        ScheduledDiscovery scheduledDiscovery) {
        this.schedulingProperties = schedulingProperties;
        this.scheduledDiscovery = scheduledDiscovery;
    }

    public void schedule() {
        String cronExpression = this.schedulingProperties.getCron();
        final CronTask task = new CronTask(scheduledDiscovery::discover, new CronTrigger(cronExpression) {
            @Override
            public Instant nextExecution(@Nonnull TriggerContext triggerContext) {
                var date = triggerContext.lastCompletion();
                if (date != null) {
                    var scheduled = triggerContext.lastScheduledExecution();
                    if (scheduled != null && date.isBefore(scheduled)) {
                        // Previous task apparently executed too early...
                        // Let's simply use the last calculated execution time then,
                        // in order to prevent accidental re-fires in the same second.
                        date = scheduled;
                    }
                } else {
                    date = Instant.now();
                }
                ZonedDateTime zonedDateTime = ZonedDateTime.from(date);
                final String cron = schedulingProperties.getCron();
                LOG.debug("nextExecutionTime with cron expression {}", cron);
                final var next = CronExpression.parse(cron).next(zonedDateTime);
                LOG.debug("next scheduled execution date is {}", next);
                return next.toInstant();
            }
        });
        scheduledTaskRegistrar.scheduleCronTask(task);
        final var lastScheduledRun = CronExpression.parse(cronExpression).next(ZonedDateTime.now());
        LOG.debug("Scheduled task at {}", lastScheduledRun);
    }

    @Override
    public void configureTasks(@Nonnull ScheduledTaskRegistrar taskRegistrar) {
        scheduledTaskRegistrar = taskRegistrar;
    }


}

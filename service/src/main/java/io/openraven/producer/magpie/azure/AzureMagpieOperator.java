/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.magpie.azure;

import com.google.common.collect.Sets;
import io.openraven.App;
import io.openraven.consumer.properties.AzureAssetsConsumerProperties;
import io.openraven.magpie.api.Session;
import io.openraven.magpie.core.Orchestrator;
import io.openraven.magpie.core.config.FifoConfig;
import io.openraven.magpie.core.config.LayerConfig;
import io.openraven.magpie.core.config.MagpieConfig;
import io.openraven.magpie.core.config.PluginConfig;
import io.openraven.magpie.plugins.azure.discovery.AzureDiscoveryConfig;
import io.openraven.producer.magpie.MagpieOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Profile("azure")
public class AzureMagpieOperator implements MagpieOperations {
    public static final String ENUMERATION_LAYER_FIFO_QUEUE_NAME = "kafka_destination";
    public static final String ENUMERATION_LAYER_FIFO_QUEUE_TYPE = "kafka";
    private static final Logger LOGGER = LoggerFactory.getLogger(AzureMagpieOperator.class);
    private static final Set<String> NON_SERVICE_PROFILES = Set.of("default", "producer", "prod", "saas", "cronJob", "local", "gcp", "aws", "azure");
    private final Environment environment;
    private final AzureCredentialProviderConverter azureCredentialProviderConverter;
    private final Map<String, Object> magpieKafkaProperties;

    public AzureMagpieOperator(Environment environment,
                               AzureCredentialProviderConverter azureCredentialProviderConverter,
                               AzureAssetsConsumerProperties azureAssetsConsumerProperties,
                               KafkaProperties kafkaProperties) {
        this.environment = environment;
        this.azureCredentialProviderConverter = azureCredentialProviderConverter;
        this.magpieKafkaProperties = App.Config.constructMagpieKafkaProperties(
                azureAssetsConsumerProperties.getConsumer(),
                kafkaProperties
        );
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }

    public void discover() {
        LOGGER.info("magpieKafkaProperties:{}", magpieKafkaProperties);
        var activeProfiles = Set.of(environment.getActiveProfiles());
        LOGGER.info("activeProfiles:{}", String.join(",", activeProfiles));
        var serviceNames = new ArrayList<>(Sets.difference(activeProfiles, NON_SERVICE_PROFILES));
        LOGGER.info("serviceNames:{}", String.join(",", serviceNames));


        final var start = Instant.now();
        var magpieConfig = new MagpieConfig();
        var magpieAzurePluginConfig = new PluginConfig<>();
        final var azureDiscoveryConfig = new AzureDiscoveryConfig();

        azureDiscoveryConfig.setServices(serviceNames);
        azureDiscoveryConfig.setCredentialsProvider(azureCredentialProviderConverter::convertCredentials);
        magpieAzurePluginConfig.setConfig(azureDiscoveryConfig);
        magpieConfig.setPlugins(Map.of("magpie.azure.discovery", magpieAzurePluginConfig));
        final LayerConfig enumerationLayerConfig = new LayerConfig();
        enumerationLayerConfig.setQueue(ENUMERATION_LAYER_FIFO_QUEUE_NAME);
        enumerationLayerConfig.setEnabled(true);
        enumerationLayerConfig.setType("origin");
        enumerationLayerConfig.setPlugins(List.of("magpie.azure.discovery"));
        magpieConfig.setLayers(Map.of("enumeration", enumerationLayerConfig));
        FifoConfig kafkaFifoConfig = new FifoConfig();
        kafkaFifoConfig.setType(ENUMERATION_LAYER_FIFO_QUEUE_TYPE);
        kafkaFifoConfig.setEnabled(true);
        kafkaFifoConfig.setProperties(magpieKafkaProperties);
        magpieConfig.setFifos(Map.of(ENUMERATION_LAYER_FIFO_QUEUE_NAME, kafkaFifoConfig));
        LOGGER.info("OSS Discovery. Classpath={}", System.getProperties().get("java.class.path"));
        try {
            new Orchestrator(magpieConfig, new Session()).scan();
        } catch (Throwable t) {
            LOGGER.info("Error during discovery", t);
        }
        LOGGER.info("Discovery completed in {}", humanReadableFormat(Duration.between(start, Instant.now())));
    }

}

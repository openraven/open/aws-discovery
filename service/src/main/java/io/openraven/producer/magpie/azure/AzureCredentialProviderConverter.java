/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.magpie.azure;

import io.openraven.models.AzureSubscription;
import io.openraven.services.AzureCredentialProvider;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Profile("azure")
public class AzureCredentialProviderConverter {
    private final AzureCredentialProvider credentialProvider;

    public AzureCredentialProviderConverter(AzureCredentialProvider credentialProvider) {
        this.credentialProvider = credentialProvider;
    }

    List<Map<String, Object>> convertCredentials() {
        return credentialProvider.get().stream().map(aCD ->
        {
            var retVal = new HashMap<String, Object>();
            retVal.put("credentialsSupplier", aCD.getConfigurationAndTokenCredentialSupplier());
            retVal.put("configurationBuilder", aCD.getConfigurationBuilder());
            final AzureSubscription subscription = aCD.getSubscription();
            retVal.put("tenant-id", subscription.getTenantId());
            retVal.put("subscription-id", subscription.getSubscriptionId());
            return retVal;
        }).collect(Collectors.toList());
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.magpie.gcp;

import com.google.common.collect.Sets;
import io.openraven.App;
import io.openraven.consumer.properties.GcpAssetsConsumerProperties;
import io.openraven.gcp.auth.GoogleCredentialsProvider;
import io.openraven.magpie.api.Session;
import io.openraven.magpie.core.Orchestrator;
import io.openraven.magpie.core.config.FifoConfig;
import io.openraven.magpie.core.config.LayerConfig;
import io.openraven.magpie.core.config.MagpieConfig;
import io.openraven.magpie.core.config.PluginConfig;
import io.openraven.magpie.plugins.gcp.discovery.GCPDiscoveryConfig;
import io.openraven.producer.magpie.MagpieOperations;
import io.openraven.properties.CrossAccountServiceProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
@Profile("gcp")
public class GCPMagpieOperator implements MagpieOperations {
    public static final String ENUMERATION_LAYER_FIFO_QUEUE_NAME = "kafka_destination";
    public static final String ENUMERATION_LAYER_FIFO_QUEUE_TYPE = "kafka";
    private static final Logger LOGGER = LoggerFactory.getLogger(GCPMagpieOperator.class);
    private static final Set<String> NON_SERVICE_PROFILES = Set.of("default", "producer", "prod", "saas", "cronJob", "local", "gcp", "aws", "azure");
    private final Environment environment;
    private final Map<String, Object> magpieKafkaProperties;
    private final CrossAccountServiceProperties crossAccountServiceProperties;
    private final RestOperations restOperations;
    private final GoogleCredentialsProvider googleCredentialsProvider;

    public GCPMagpieOperator(Environment environment,
                             CrossAccountServiceProperties crossAccountServiceProperties,
                             GcpAssetsConsumerProperties gcpAssetsConsumerProperties,
                             KafkaProperties kafkaProperties,
                             GoogleCredentialsProvider googleCredentialsProvider,
                             RestOperations restOperations) {
        this.environment = environment;
        this.crossAccountServiceProperties = crossAccountServiceProperties;
        this.restOperations = restOperations;
        this.magpieKafkaProperties = App.Config.constructMagpieKafkaProperties(
                gcpAssetsConsumerProperties.getConsumer(),
                kafkaProperties
        );
        this.googleCredentialsProvider = googleCredentialsProvider;
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }

    public void discover() {
        var activeProfiles = Set.of(environment.getActiveProfiles());
        LOGGER.debug("activeProfiles:{}", String.join(",", activeProfiles));
        var serviceNames = new ArrayList<>(Sets.difference(activeProfiles, NON_SERVICE_PROFILES));
        LOGGER.debug("serviceNames:{}", String.join(",", serviceNames));
        if (!googleCredentialsProvider.hasCredentialConfiguration()) {
            LOGGER.info("Skipping GCP discovery because no credentials are configured");
            return;
        }

        final var start = Instant.now();
        var magpieConfig = new MagpieConfig();
        var magpieGcpPluginConfig = new PluginConfig<>();
        final GCPDiscoveryConfig gcpDiscoveryConfig = new GCPDiscoveryConfig();
        gcpDiscoveryConfig.setCredentialsProvider(googleCredentialsProvider);
        gcpDiscoveryConfig.setServices(serviceNames);
        gcpDiscoveryConfig.setProjectListProvider(() -> {
            var result = restOperations.getForEntity(crossAccountServiceProperties.getProjectIdsUrl(), String[].class);
            final String[] body = result.getBody();
            var projectIds = Optional.ofNullable(body).map(Arrays::asList).orElse(Collections.emptyList());
            LOGGER.debug("projectIds:{}", projectIds);
            return projectIds;
        });

        magpieGcpPluginConfig.setConfig(gcpDiscoveryConfig);
        magpieConfig.setPlugins(Map.of("magpie.gcp.discovery", magpieGcpPluginConfig));
        final LayerConfig enumerationLayerConfig = new LayerConfig();
        enumerationLayerConfig.setQueue(ENUMERATION_LAYER_FIFO_QUEUE_NAME);
        enumerationLayerConfig.setEnabled(true);
        enumerationLayerConfig.setType("origin");
        enumerationLayerConfig.setPlugins(List.of("magpie.gcp.discovery"));
        magpieConfig.setLayers(Map.of("enumeration", enumerationLayerConfig));
        FifoConfig kafkaFifoConfig = new FifoConfig();
        kafkaFifoConfig.setType(ENUMERATION_LAYER_FIFO_QUEUE_TYPE);
        kafkaFifoConfig.setEnabled(true);
        kafkaFifoConfig.setProperties(magpieKafkaProperties);
        magpieConfig.setFifos(Map.of(ENUMERATION_LAYER_FIFO_QUEUE_NAME, kafkaFifoConfig));
        LOGGER.info("OSS Discovery. Classpath={}", System.getProperties().get("java.class.path"));
        try {
            new Orchestrator(magpieConfig, new Session()).scan();
        } catch (Throwable t) {
            LOGGER.info("Error during scan", t);
        }
        LOGGER.info("Discovery completed in {}", humanReadableFormat(Duration.between(start, Instant.now())));
    }
}

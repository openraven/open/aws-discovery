/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.producer.magpie.aws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Sets;
import io.openraven.App;
import io.openraven.consumer.properties.AwsAssetsConsumerProperties;
import io.openraven.magpie.api.Session;
import io.openraven.magpie.core.Orchestrator;
import io.openraven.magpie.core.config.ConfigUtils;
import io.openraven.magpie.core.config.FifoConfig;
import io.openraven.magpie.core.config.LayerConfig;
import io.openraven.magpie.core.config.MagpieConfig;
import io.openraven.magpie.core.config.PluginConfig;
import io.openraven.magpie.plugins.aws.discovery.AWSDiscoveryConfig;
import io.openraven.producer.magpie.MagpieOperations;
import io.openraven.producer.properties.CrossAccountAccessConfig;
import io.openraven.producer.properties.CrossAccountProperties;
import io.openraven.producer.properties.DiscoveryProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.regions.Region;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Profile("aws")
public class AWSMagpieOperator implements MagpieOperations {
    public static final String ENUMERATION_LAYER_FIFO_QUEUE_NAME = "kafka_destination";
    public static final String ENUMERATION_LAYER_FIFO_QUEUE_TYPE = "kafka";
    private static final Logger LOGGER = LoggerFactory.getLogger(AWSMagpieOperator.class);
    private static final Set<String> SERVICES_NEEDING_GLOBAL_REGION = Set.of("iam", "cloudfront", "route53", "organization");
    private static final String AWS_GLOBAL_REGION = Region.AWS_GLOBAL.id();
    private static final Set<String> NON_SERVICE_PROFILES = Set.of("default", "producer", "prod", "saas", "cronJob", "local", "azure", "gcp");
    private final CrossAccountAccessConfig crossAccountAccessConfig;
    private final DiscoveryProperties discoveryProperties;
    private final Environment environment;
    private final Map<String, Object> magpieKafkaProperties;
    private final CrossAccountProperties crossAccountProperties;

    public AWSMagpieOperator(CrossAccountAccessConfig crossAccountAccessConfig,
                             DiscoveryProperties discoveryProperties,
                             Environment environment,
                             AwsAssetsConsumerProperties awsAssetsConsumerProperties,
                             KafkaProperties kafkaProperties,
                             CrossAccountProperties crossAccountProperties) {
        this.crossAccountAccessConfig = crossAccountAccessConfig;
        this.discoveryProperties = discoveryProperties;
        this.environment = environment;
        this.magpieKafkaProperties = App.Config.constructMagpieKafkaProperties(
                awsAssetsConsumerProperties.getConsumer(),
                kafkaProperties
        );
        this.crossAccountProperties = crossAccountProperties;
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }

    public void discover() {
        var activeProfiles = Set.of(environment.getActiveProfiles());
        LOGGER.debug("activeProfiles:{}", String.join(",", activeProfiles));
        var serviceNames = new ArrayList<>(Sets.difference(activeProfiles, NON_SERVICE_PROFILES));
        LOGGER.debug("serviceNames:{}", String.join(",", serviceNames));

        final var start = Instant.now();
        var magpieConfig = new MagpieConfig();
        var magpieAwsPluginConfig = new PluginConfig<>();
        final AWSDiscoveryConfig awsDiscoveryConfig = new AWSDiscoveryConfig();

        awsDiscoveryConfig.setAssumedRoles(this.crossAccountAccessConfig.getRoleArns());

        List<String> regions = new ArrayList<>(List.of(discoveryProperties.getAws()));
        if (serviceNames.stream().anyMatch(x -> SERVICES_NEEDING_GLOBAL_REGION.contains(x.toLowerCase()))) {
            /*
             * Hacky fix to get service profiles needing aws-global to work. Needs to be fixed in a better way later.
             *
             * Magpie determines the regions to run discovery over by taking the intersection of:
             * - regions set by us in awsDiscoveryConfig
             * - list of valid regions per service according to AWS SDK where the services are
             *   things like IAM, EC2, S3, etc.
             *
             * The only eligible IAM regions are:
             * aws-global, aws-global-fips, aws-cn-global,
             * aws-us-gov-global, aws-us-gov-global-fips, aws-iso-global,
             * and aws-iso-b-global
             *
             * Eligible CloudFront regions:
             * aws-global, aws-cn-global
             *
             * Eligible Route 53 regions:
             * aws-global, aws-global-fips, aws-cn-global,
             * aws-us-gov-global, aws-us-gov-global-fips, aws-iso-global,
             * and aws-iso-b-global
             *
             * None of these global regions are configured in the ZK config that powers the list of regions
             * we send to Magpie because they weren't needed in legacy discovery. Legacy discovery hard coded
             * aws-global for these services.
             */
            regions.add(AWS_GLOBAL_REGION);
        }

        awsDiscoveryConfig.setRegions(regions);
        awsDiscoveryConfig.setExternalId(crossAccountProperties.getExternalID());
        awsDiscoveryConfig.setIgnoredRegions(Collections.emptyList());
        awsDiscoveryConfig.setServices(serviceNames);

        final var proxyRoleArn = crossAccountProperties.getProxyRoleArn();
        if (proxyRoleArn != null && !proxyRoleArn.isEmpty()) {
            final var proxyExternalID = crossAccountProperties.getProxyExternalID();

            if (proxyExternalID == null || proxyExternalID.isEmpty()) {
                throw new IllegalArgumentException("Cannot have a null/empty proxyExternalID");
            }

            final var proxyRoleConfig = new AWSDiscoveryConfig.ProxyRoleConfig();
            proxyRoleConfig.setArn(proxyRoleArn);
            proxyRoleConfig.setExternalId(proxyExternalID);
            awsDiscoveryConfig.setProxyRoleConfig(proxyRoleConfig);
        }

        magpieAwsPluginConfig.setConfig(awsDiscoveryConfig);
        magpieConfig.setPlugins(Map.of("magpie.aws.discovery", magpieAwsPluginConfig));
        final LayerConfig enumerationLayerConfig = new LayerConfig();
        enumerationLayerConfig.setQueue(ENUMERATION_LAYER_FIFO_QUEUE_NAME);
        enumerationLayerConfig.setEnabled(true);
        enumerationLayerConfig.setType("origin");
        enumerationLayerConfig.setPlugins(List.of("magpie.aws.discovery"));
        magpieConfig.setLayers(Map.of("enumeration", enumerationLayerConfig));
        FifoConfig kafkaFifoConfig = new FifoConfig();
        kafkaFifoConfig.setType(ENUMERATION_LAYER_FIFO_QUEUE_TYPE);
        kafkaFifoConfig.setEnabled(true);
        kafkaFifoConfig.setProperties(magpieKafkaProperties);
        magpieConfig.setFifos(Map.of(ENUMERATION_LAYER_FIFO_QUEUE_NAME, kafkaFifoConfig));
        final MagpieConfig config;
        try {
            config = ConfigUtils.merge(magpieConfig, System.getenv());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getMessage());
        }
        LOGGER.info("OSS Discovery. Classpath={}", System.getProperties().get("java.class.path"));
        new Orchestrator(config, new Session()).scan();
        LOGGER.info("Discovery completed in {}", humanReadableFormat(Duration.between(start, Instant.now())));
    }
}

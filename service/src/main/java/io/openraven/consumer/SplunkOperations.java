/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.openraven.consumer.properties.AwsAssetsConsumerProperties;
import io.openraven.data.properties.SplunkServiceMagpieProperties;
import io.openraven.data.shared.GenericAsset;
import io.openraven.magpie.data.aws.AWSResource;
import io.openraven.magpie.data.gcp.GCPResource;
import io.openraven.magpie.data.utils.JacksonMapper;
import io.sentry.Sentry;
import io.sentry.SentryEvent;
import org.slf4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

import static org.slf4j.LoggerFactory.getLogger;

@Component
@Profile("splunk_consumer")
public class SplunkOperations implements DisposableBean {
    // Required by our Splunk OEM contract
    public static final String SOURCETYPE_PREFIX = "open_raven_";
    private static final Logger LOG = getLogger(SplunkOperations.class);
    private static final ObjectMapper MAPPER = JacksonMapper.getJacksonMapper();

    static {
        MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    private final AwsAssetsConsumerProperties assetsConsumerProperties;
    private final List<JsonNode> buffer = new ArrayList<>();
    private final ReentrantLock mutex = new ReentrantLock(false);
    private final InsecureRestOperations httpReq;
    private final HttpHeaders headers;
    private final String hecURL;


    public SplunkOperations(SplunkServiceMagpieProperties splunkServiceProperties,
                            AwsAssetsConsumerProperties assetsConsumerProperties,
                            InsecureRestOperations httpReq) {
        this.assetsConsumerProperties = assetsConsumerProperties;

        this.httpReq = httpReq;
        this.headers = new HttpHeaders();
        // See https://docs.splunk.com/Documentation/Splunk/8.1.2/Data/HECExamples for examples
        this.headers.set("Authorization", "Splunk " + splunkServiceProperties.getAuthToken());
        this.hecURL = splunkServiceProperties.getEventEndpointUrl().toString();

    }

    public boolean save(AWSResource resource) {
        return saveHelper(
                this::createPayload,
                resource
        );
    }

    public boolean save(GCPResource resource) {
        return saveHelper(
                this::createPayload,
                resource
        );
    }

    public boolean save(GenericAsset resource) {
        return saveHelper(
                this::createPayload,
                resource
        );
    }

    private <T> boolean saveHelper(
            Function<T, JsonNode> createPayload,
            T resource
    ) {
        final int size = buffer.size();
        if (size >= assetsConsumerProperties.getSplunkConsumer().getPersistenceBatchSize()) {
            flush();
        }

        try {
            mutex.lock();
            return buffer.add(createPayload.apply(resource));
        } finally {
            mutex.unlock();
        }
    }

    private JsonNode createPayload(GCPResource resource) {
        JsonNode resourceNode = MAPPER.valueToTree(resource);
        Optional.ofNullable(resourceNode.get("discoveryMeta")).ifPresent(discoveryMeta -> {
                    var objectNode = (ObjectNode) discoveryMeta.get(1); // Index 0 is the class type
                    if (objectNode != null) {
                        objectNode.put("discoverySource", "magpie");
                    }
                }
        );
        ObjectNode payload = MAPPER.createObjectNode();
        String sourceType = SOURCETYPE_PREFIX + resource.resourceType.replace(":", "").toLowerCase();
        payload.put("sourcetype", sourceType);
        payload.set("event", resourceNode);
        LOG.debug("payload:{}", payload);
        LOG.debug("resourceNode:{}", resourceNode);

        return payload;
    }

    private JsonNode createPayload(GenericAsset resource) {
        JsonNode resourceNode = MAPPER.valueToTree(resource);
        Optional.ofNullable(resourceNode.get("discoveryMeta")).ifPresent(discoveryMeta -> {
                    var objectNode = (ObjectNode) discoveryMeta.get(1); // Index 0 is the class type
                    if (objectNode != null) {
                        objectNode.put("discoverySource", "genericdiscovery");
                    }
                }
        );
        ObjectNode payload = MAPPER.createObjectNode();
        String sourceType = "%s%s".formatted(SOURCETYPE_PREFIX, resource.getSource());
        payload.put("sourcetype", sourceType);
        payload.set("event", resourceNode);
        LOG.debug("payload:{}", payload);
        LOG.debug("resourceNode:{}", resourceNode);

        return payload;
    }

    private JsonNode createPayload(AWSResource resource) {
        JsonNode resourceNode = MAPPER.valueToTree(resource);
        Optional.ofNullable(resourceNode.get("discoveryMeta")).ifPresent(discoveryMeta -> {
                    var objectNode = (ObjectNode) discoveryMeta.get(1); // Index 0 is the class type
                    if (objectNode != null) {
                        objectNode.put("discoverySource", "magpie");
                    }
                }
        );
        ObjectNode payload = MAPPER.createObjectNode();
        String sourceType = SOURCETYPE_PREFIX + resource.resourceType.replace(":", "").toLowerCase();
        payload.put("sourcetype", sourceType);
        payload.set("event", resourceNode);
        LOG.debug("payload:{}", payload);
        LOG.debug("resourceNode:{}", resourceNode);

        return payload;
    }

    @Scheduled(fixedDelay = 10000, initialDelay = 10000)
    public void flush() {
        final int size = buffer.size();
        if (size > 0) {
            LOG.debug("Flushing log, current buffer size {}", size);
            try {
                mutex.lock();
                sendEventBufferToSplunk();
                buffer.clear();
            } catch (Exception e) {
                LOG.error("Failure attempting to flush {} resources to Splunk with message {}", size, e.getMessage());
                Sentry.captureEvent(new SentryEvent(e));
            } finally {
                mutex.unlock();
            }
        }
    }

    private void sendEventBufferToSplunk() {
        try {
            HttpEntity<String> postPayload = new HttpEntity<>(MAPPER.writeValueAsString(buffer), headers);
            ResponseEntity<String> response = httpReq.postForEntity(hecURL, postPayload, String.class);
            LOG.trace("Sent {} event(s) to Splunk - response: {}", buffer.size(), response);
        } catch (Exception ex) {
            LOG.error("Error {} event(s) to Splunk", buffer.size(), ex);
        }
    }

    @Override
    public void destroy() {
        flush();
    }

}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer;

import jakarta.annotation.Nonnull;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Set;

@Component
public class InsecureRestOperations implements RestOperations {
    private final RestOperations inner;

    public InsecureRestOperations(RestTemplateBuilder builder) throws NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }

                    public void checkClientTrusted(
                            X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            X509Certificate[] certs, String authType) {
                    }
                }
        };

        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new SecureRandom());
        SSLConnectionSocketFactory sslConFactory = new SSLConnectionSocketFactory(sslContext, (hostname, session) -> true);
        HttpClientConnectionManager cm = PoolingHttpClientConnectionManagerBuilder.create()
                .setSSLSocketFactory(sslConFactory)
                .build();
        var httpClient = HttpClients.custom()
                .setConnectionManager(cm)
                .build();
        final var customRequestFactory = new HttpComponentsClientHttpRequestFactory();
        customRequestFactory.setHttpClient(httpClient);
        inner = builder.requestFactory(() -> customRequestFactory).build();
    }

    /**
     * Retrieve a representation by doing a GET on the specified URL.
     * The response (if any) is converted and returned.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url          the URL
     * @param responseType the type of the return value
     * @param uriVariables the variables to expand the template
     * @return the converted object
     */
    @Override
    @Nullable
    public <T> T getForObject(@Nonnull String url, @Nonnull Class<T> responseType, @Nonnull Object... uriVariables) throws RestClientException {
        return inner.getForObject(url, responseType, uriVariables);
    }

    /**
     * Retrieve a representation by doing a GET on the URI template.
     * The response (if any) is converted and returned.
     * <p>URI Template variables are expanded using the given map.
     *
     * @param url          the URL
     * @param responseType the type of the return value
     * @param uriVariables the map containing variables for the URI template
     * @return the converted object
     */
    @Override
    @Nullable
    public <T> T getForObject(@Nonnull String url,
                              @Nonnull Class<T> responseType,
                              @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.getForObject(url, responseType, uriVariables);
    }

    /**
     * Retrieve a representation by doing a GET on the URL .
     * The response (if any) is converted and returned.
     *
     * @param url          the URL
     * @param responseType the type of the return value
     * @return the converted object
     */
    @Override
    @Nullable
    public <T> T getForObject(@Nonnull URI url, @Nonnull Class<T> responseType) throws RestClientException {
        return inner.getForObject(url, responseType);
    }

    /**
     * Retrieve an entity by doing a GET on the specified URL.
     * The response is converted and stored in an {@link ResponseEntity}.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url          the URL
     * @param responseType the type of the return value
     * @param uriVariables the variables to expand the template
     * @return the entity
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> getForEntity(@Nonnull String url,
                                              @Nonnull Class<T> responseType,
                                              @Nonnull Object... uriVariables) throws RestClientException {
        return inner.getForEntity(url, responseType, uriVariables);
    }

    /**
     * Retrieve a representation by doing a GET on the URI template.
     * The response is converted and stored in an {@link ResponseEntity}.
     * <p>URI Template variables are expanded using the given map.
     *
     * @param url          the URL
     * @param responseType the type of the return value
     * @param uriVariables the map containing variables for the URI template
     * @return the converted object
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> getForEntity(@Nonnull String url,
                                              @Nonnull Class<T> responseType,
                                              @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.getForEntity(url, responseType, uriVariables);
    }

    /**
     * Retrieve a representation by doing a GET on the URL .
     * The response is converted and stored in an {@link ResponseEntity}.
     *
     * @param url          the URL
     * @param responseType the type of the return value
     * @return the converted object
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> getForEntity(@Nonnull URI url, @Nonnull Class<T> responseType) throws RestClientException {
        return inner.getForEntity(url, responseType);
    }

    /**
     * Retrieve all headers of the resource specified by the URI template.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url          the URL
     * @param uriVariables the variables to expand the template
     * @return all HTTP headers of that resource
     */
    @Override
    @Nonnull
    public HttpHeaders headForHeaders(@Nonnull String url, @Nonnull Object... uriVariables) throws RestClientException {
        return inner.headForHeaders(url, uriVariables);
    }

    /**
     * Retrieve all headers of the resource specified by the URI template.
     * <p>URI Template variables are expanded using the given map.
     *
     * @param url          the URL
     * @param uriVariables the map containing variables for the URI template
     * @return all HTTP headers of that resource
     */
    @Override
    @Nonnull
    public HttpHeaders headForHeaders(@Nonnull String url, @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.headForHeaders(url, uriVariables);
    }

    /**
     * Retrieve all headers of the resource specified by the URL.
     *
     * @param url the URL
     * @return all HTTP headers of that resource
     */
    @Override
    @Nonnull
    public HttpHeaders headForHeaders(@Nonnull URI url) throws RestClientException {
        return inner.headForHeaders(url);
    }

    /**
     * Create a new resource by POSTing the given object to the URI template, and returns the value of
     * the {@code Location} header. This header typically indicates where the new resource is stored.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param uriVariables the variables to expand the template
     * @return the value for the {@code Location} header
     * @see HttpEntity
     */
    @Override
    @Nullable
    public URI postForLocation(@Nonnull String url, Object request, @Nonnull Object... uriVariables) throws RestClientException {
        return inner.postForLocation(url, request, uriVariables);
    }

    /**
     * Create a new resource by POSTing the given object to the URI template, and returns the value of
     * the {@code Location} header. This header typically indicates where the new resource is stored.
     * <p>URI Template variables are expanded using the given map.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param uriVariables the variables to expand the template
     * @return the value for the {@code Location} header
     * @see HttpEntity
     */
    @Override
    @Nullable
    public URI postForLocation(@Nonnull String url, Object request, @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.postForLocation(url, request, uriVariables);
    }

    /**
     * Create a new resource by POSTing the given object to the URL, and returns the value of the
     * {@code Location} header. This header typically indicates where the new resource is stored.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url     the URL
     * @param request the Object to be POSTed (may be {@code null})
     * @return the value for the {@code Location} header
     * @see HttpEntity
     */
    @Override
    @Nullable
    public URI postForLocation(@Nonnull URI url, Object request) throws RestClientException {
        return inner.postForLocation(url, request);
    }

    /**
     * Create a new resource by POSTing the given object to the URI template,
     * and returns the representation found in the response.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param responseType the type of the return value
     * @param uriVariables the variables to expand the template
     * @return the converted object
     * @see HttpEntity
     */
    @Override
    @Nullable
    public <T> T postForObject(@Nonnull String url,
                               Object request,
                               @Nonnull Class<T> responseType,
                               @Nonnull Object... uriVariables) throws RestClientException {
        return inner.postForObject(url, request, responseType, uriVariables);
    }

    /**
     * Create a new resource by POSTing the given object to the URI template,
     * and returns the representation found in the response.
     * <p>URI Template variables are expanded using the given map.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param responseType the type of the return value
     * @param uriVariables the variables to expand the template
     * @return the converted object
     * @see HttpEntity
     */
    @Override
    @Nullable
    public <T> T postForObject(@Nonnull String url,
                               Object request,
                               @Nonnull Class<T> responseType,
                               @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.postForObject(url, request, responseType, uriVariables);
    }

    /**
     * Create a new resource by POSTing the given object to the URL,
     * and returns the representation found in the response.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param responseType the type of the return value
     * @return the converted object
     * @see HttpEntity
     */
    @Override
    @Nullable
    public <T> T postForObject(@Nonnull URI url, Object request, @Nonnull Class<T> responseType) throws RestClientException {
        return inner.postForObject(url, request, responseType);
    }

    /**
     * Create a new resource by POSTing the given object to the URI template,
     * and returns the response as {@link ResponseEntity}.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param responseType the response type
     * @param uriVariables the variables to expand the template
     * @return the converted object
     * @see HttpEntity
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> postForEntity(@Nonnull String url,
                                               Object request,
                                               @Nonnull Class<T> responseType,
                                               @Nonnull Object... uriVariables) throws RestClientException {
        return inner.postForEntity(url, request, responseType, uriVariables);
    }

    /**
     * Create a new resource by POSTing the given object to the URI template,
     * and returns the response as {@link HttpEntity}.
     * <p>URI Template variables are expanded using the given map.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param responseType the response type
     * @param uriVariables the variables to expand the template
     * @return the converted object
     * @see HttpEntity
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> postForEntity(@Nonnull String url,
                                               Object request,
                                               @Nonnull Class<T> responseType,
                                               @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.postForEntity(url, request, responseType, uriVariables);
    }

    /**
     * Create a new resource by POSTing the given object to the URL,
     * and returns the response as {@link ResponseEntity}.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p>The body of the entity, or {@code request} itself, can be a
     * {@link MultiValueMap MultiValueMap} to create a multipart request.
     * The values in the {@code MultiValueMap} can be any Object representing the body of the part,
     * or an {@link HttpEntity HttpEntity} representing a part with body
     * and headers.
     *
     * @param url          the URL
     * @param request      the Object to be POSTed (may be {@code null})
     * @param responseType the response type
     * @return the converted object
     * @see HttpEntity
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> postForEntity(@Nonnull URI url,
                                               Object request,
                                               @Nonnull Class<T> responseType) throws RestClientException {
        return inner.postForEntity(url, request, responseType);
    }

    /**
     * Create or update a resource by PUTting the given object to the URI.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     *
     * @param url          the URL
     * @param request      the Object to be PUT (may be {@code null})
     * @param uriVariables the variables to expand the template
     * @see HttpEntity
     */
    @Override
    public void put(@Nonnull String url, Object request, @Nonnull Object... uriVariables) throws RestClientException {
        inner.put(url, request, uriVariables);
    }

    /**
     * Creates a new resource by PUTting the given object to URI template.
     * <p>URI Template variables are expanded using the given map.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     *
     * @param url          the URL
     * @param request      the Object to be PUT (may be {@code null})
     * @param uriVariables the variables to expand the template
     * @see HttpEntity
     */
    @Override
    public void put(@Nonnull String url, Object request, @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        inner.put(url, request, uriVariables);
    }

    /**
     * Creates a new resource by PUTting the given object to URL.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     *
     * @param url     the URL
     * @param request the Object to be PUT (may be {@code null})
     * @see HttpEntity
     */
    @Override
    public void put(@Nonnull URI url, Object request) throws RestClientException {
        inner.put(url, request);
    }

    /**
     * Update a resource by PATCHing the given object to the URI template,
     * and return the representation found in the response.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p><b>NOTE: The standard JDK HTTP library does not support HTTP PATCH.
     * You need to use the Apache HttpComponents or OkHttp request factory.</b>
     *
     * @param url          the URL
     * @param request      the object to be PATCHed (may be {@code null})
     * @param responseType the type of the return value
     * @param uriVariables the variables to expand the template
     * @return the converted object
     * @see HttpEntity
     * @see RestTemplate#setRequestFactory
     * @since 4.3.5
     */
    @Override
    @Nullable
    public <T> T patchForObject(@Nonnull String url,
                                Object request,
                                @Nonnull Class<T> responseType,
                                @Nonnull Object... uriVariables) throws RestClientException {
        return inner.patchForObject(url, request, responseType, uriVariables);
    }

    /**
     * Update a resource by PATCHing the given object to the URI template,
     * and return the representation found in the response.
     * <p>URI Template variables are expanded using the given map.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p><b>NOTE: The standard JDK HTTP library does not support HTTP PATCH.
     * You need to use the Apache HttpComponents or OkHttp request factory.</b>
     *
     * @param url          the URL
     * @param request      the object to be PATCHed (may be {@code null})
     * @param responseType the type of the return value
     * @param uriVariables the variables to expand the template
     * @return the converted object
     * @see HttpEntity
     * @see RestTemplate#setRequestFactory
     * @since 4.3.5
     */
    @Override
    @Nullable
    public <T> T patchForObject(@Nonnull String url,
                                Object request,
                                @Nonnull Class<T> responseType,
                                @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.patchForObject(url, request, responseType, uriVariables);
    }

    /**
     * Update a resource by PATCHing the given object to the URL,
     * and return the representation found in the response.
     * <p>The {@code request} parameter can be a {@link HttpEntity} in order to
     * add additional HTTP headers to the request.
     * <p><b>NOTE: The standard JDK HTTP library does not support HTTP PATCH.
     * You need to use the Apache HttpComponents or OkHttp request factory.</b>
     *
     * @param url          the URL
     * @param request      the object to be PATCHed (may be {@code null})
     * @param responseType the type of the return value
     * @return the converted object
     * @see HttpEntity
     * @see RestTemplate#setRequestFactory
     * @since 4.3.5
     */
    @Override
    @Nullable
    public <T> T patchForObject(@Nonnull URI url, Object request, @Nonnull Class<T> responseType) throws RestClientException {
        return inner.patchForObject(url, request, responseType);
    }

    /**
     * Delete the resources at the specified URI.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url          the URL
     * @param uriVariables the variables to expand in the template
     */
    @Override
    public void delete(@Nonnull String url, @Nonnull Object... uriVariables) throws RestClientException {
        inner.delete(url, uriVariables);
    }

    /**
     * Delete the resources at the specified URI.
     * <p>URI Template variables are expanded using the given map.
     *
     * @param url          the URL
     * @param uriVariables the variables to expand the template
     */
    @Override
    public void delete(@Nonnull String url, @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        inner.delete(url, uriVariables);
    }

    /**
     * Delete the resources at the specified URL.
     *
     * @param url the URL
     */
    @Override
    public void delete(@Nonnull URI url) throws RestClientException {
        inner.delete(url);
    }

    /**
     * Return the value of the {@code Allow} header for the given URI.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url          the URL
     * @param uriVariables the variables to expand in the template
     * @return the value of the {@code Allow} header
     */
    @Override
    @Nonnull
    public Set<HttpMethod> optionsForAllow(@Nonnull String url, @Nonnull Object... uriVariables) throws RestClientException {
        return inner.optionsForAllow(url, uriVariables);
    }

    /**
     * Return the value of the {@code Allow} header for the given URI.
     * <p>URI Template variables are expanded using the given map.
     *
     * @param url          the URL
     * @param uriVariables the variables to expand in the template
     * @return the value of the {@code Allow} header
     */
    @Override
    @Nonnull
    public Set<HttpMethod> optionsForAllow(@Nonnull String url, @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.optionsForAllow(url, uriVariables);
    }

    /**
     * Return the value of the {@code Allow} header for the given URL.
     *
     * @param url the URL
     * @return the value of the {@code Allow} header
     */
    @Override
    @Nonnull
    public Set<HttpMethod> optionsForAllow(@Nonnull URI url) throws RestClientException {
        return inner.optionsForAllow(url);
    }

    /**
     * Execute the HTTP method to the given URI template, writing the given request entity to the request, and
     * returns the response as {@link ResponseEntity}.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url           the URL
     * @param method        the HTTP method (GET, POST, etc)
     * @param requestEntity the entity (headers and/or body) to write to the request
     *                      may be {@code null})
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @param uriVariables  the variables to expand in the template
     * @return the response as entity
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull String url,
                                          @Nonnull HttpMethod method,
                                          HttpEntity<?> requestEntity,
                                          @Nonnull Class<T> responseType,
                                          @Nonnull Object... uriVariables) throws RestClientException {
        return inner.exchange(url, method, requestEntity, responseType, uriVariables);
    }

    /**
     * Execute the HTTP method to the given URI template, writing the given request entity to the request, and
     * returns the response as {@link ResponseEntity}.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url           the URL
     * @param method        the HTTP method (GET, POST, etc)
     * @param requestEntity the entity (headers and/or body) to write to the request
     *                      (may be {@code null})
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @param uriVariables  the variables to expand in the template
     * @return the response as entity
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull String url,
                                          @Nonnull HttpMethod method,
                                          HttpEntity<?> requestEntity,
                                          @Nonnull Class<T> responseType,
                                          @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.exchange(url, method, requestEntity, responseType, uriVariables);
    }

    /**
     * Execute the HTTP method to the given URI template, writing the given request entity to the request, and
     * returns the response as {@link ResponseEntity}.
     *
     * @param url           the URL
     * @param method        the HTTP method (GET, POST, etc)
     * @param requestEntity the entity (headers and/or body) to write to the request
     *                      (may be {@code null})
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @return the response as entity
     * @since 3.0.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull URI url,
                                          @Nonnull HttpMethod method,
                                          HttpEntity<?> requestEntity,
                                          @Nonnull Class<T> responseType) throws RestClientException {
        return inner.exchange(url, method, requestEntity, responseType);
    }

    /**
     * Execute the HTTP method to the given URI template, writing the given
     * request entity to the request, and returns the response as {@link ResponseEntity}.
     * The given {@link ParameterizedTypeReference} is used to pass generic type information:
     * <pre class="code">
     * ParameterizedTypeReference&lt;List&lt;MyBean&gt;&gt; myBean =
     *     new ParameterizedTypeReference&lt;List&lt;MyBean&gt;&gt;() {};
     * ResponseEntity&lt;List&lt;MyBean&gt;&gt; response =
     *     template.exchange(&quot;https://example.com&quot;,HttpMethod.GET, null, myBean);
     * </pre>
     *
     * @param url           the URL
     * @param method        the HTTP method (GET, POST, etc)
     * @param requestEntity the entity (headers and/or body) to write to the
     *                      request (maybe {@code null})
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @param uriVariables  the variables to expand in the template
     * @return the response as entity
     * @since 3.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull String url,
                                          @Nonnull HttpMethod method,
                                          HttpEntity<?> requestEntity,
                                          @Nonnull ParameterizedTypeReference<T> responseType,
                                          @Nonnull Object... uriVariables) throws RestClientException {
        return inner.exchange(url, method, requestEntity, responseType, uriVariables);
    }

    /**
     * Execute the HTTP method to the given URI template, writing the given
     * request entity to the request, and returns the response as {@link ResponseEntity}.
     * The given {@link ParameterizedTypeReference} is used to pass generic type information:
     * <pre class="code">
     * ParameterizedTypeReference&lt;List&lt;MyBean&gt;&gt; myBean =
     *     new ParameterizedTypeReference&lt;List&lt;MyBean&gt;&gt;() {};
     * ResponseEntity&lt;List&lt;MyBean&gt;&gt; response =
     *     template.exchange(&quot;https://example.com&quot;,HttpMethod.GET, null, myBean);
     * </pre>
     *
     * @param url           the URL
     * @param method        the HTTP method (GET, POST, etc)
     * @param requestEntity the entity (headers and/or body) to write to the request
     *                      (may be {@code null})
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @param uriVariables  the variables to expand in the template
     * @return the response as entity
     * @since 3.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull String url,
                                          @Nonnull HttpMethod method,
                                          HttpEntity<?> requestEntity,
                                          @Nonnull ParameterizedTypeReference<T> responseType,
                                          @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.exchange(url, method, requestEntity, responseType, uriVariables);
    }

    /**
     * Execute the HTTP method to the given URI template, writing the given
     * request entity to the request, and returns the response as {@link ResponseEntity}.
     * The given {@link ParameterizedTypeReference} is used to pass generic type information:
     * <pre class="code">
     * ParameterizedTypeReference&lt;List&lt;MyBean&gt;&gt; myBean =
     *     new ParameterizedTypeReference&lt;List&lt;MyBean&gt;&gt;() {};
     * ResponseEntity&lt;List&lt;MyBean&gt;&gt; response =
     *     template.exchange(&quot;https://example.com&quot;,HttpMethod.GET, null, myBean);
     * </pre>
     *
     * @param url           the URL
     * @param method        the HTTP method (GET, POST, etc)
     * @param requestEntity the entity (headers and/or body) to write to the request
     *                      (maybe {@code null})
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @return the response as entity
     * @since 3.2
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull URI url,
                                          @Nonnull HttpMethod method,
                                          HttpEntity<?> requestEntity,
                                          @Nonnull ParameterizedTypeReference<T> responseType) throws RestClientException {
        return inner.exchange(url, method, requestEntity, responseType);
    }

    /**
     * Execute the request specified in the given {@link RequestEntity} and return
     * the response as {@link ResponseEntity}. Typically used in combination
     * with the static builder methods on {@code RequestEntity}, for instance:
     * <pre class="code">
     * MyRequest body = ...
     * RequestEntity request = RequestEntity
     *     .post(new URI(&quot;https://example.com/foo&quot;))
     *     .accept(MediaType.APPLICATION_JSON)
     *     .body(body);
     * ResponseEntity&lt;MyResponse&gt; response = template.exchange(request, MyResponse.class);
     * </pre>
     *
     * @param requestEntity the entity to write to the request
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @return the response as entity
     * @since 4.1
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull RequestEntity<?> requestEntity,
                                          @Nonnull Class<T> responseType) throws RestClientException {
        return inner.exchange(requestEntity, responseType);
    }

    /**
     * Execute the request specified in the given {@link RequestEntity} and return
     * the response as {@link ResponseEntity}. The given
     * {@link ParameterizedTypeReference} is used to pass generic type information:
     * <pre class="code">
     * MyRequest body = ...
     * RequestEntity request = RequestEntity
     *     .post(new URI(&quot;https://example.com/foo&quot;))
     *     .accept(MediaType.APPLICATION_JSON)
     *     .body(body);
     * ParameterizedTypeReference&lt;List&lt;MyResponse&gt;&gt; myBean =
     *     new ParameterizedTypeReference&lt;List&lt;MyResponse&gt;&gt;() {};
     * ResponseEntity&lt;List&lt;MyResponse&gt;&gt; response = template.exchange(request, myBean);
     * </pre>
     *
     * @param requestEntity the entity to write to the request
     * @param responseType  the type to convert the response to, or {@code Void.class} for no body
     * @return the response as entity
     * @since 4.1
     */
    @Override
    @Nonnull
    public <T> ResponseEntity<T> exchange(@Nonnull RequestEntity<?> requestEntity,
                                          @Nonnull ParameterizedTypeReference<T> responseType) throws RestClientException {
        return inner.exchange(requestEntity, responseType);
    }

    /**
     * Execute the HTTP method to the given URI template, preparing the request with the
     * {@link RequestCallback}, and reading the response with a {@link ResponseExtractor}.
     * <p>URI Template variables are expanded using the given URI variables, if any.
     *
     * @param url               the URL
     * @param method            the HTTP method (GET, POST, etc)
     * @param requestCallback   object that prepares the request
     * @param responseExtractor object that extracts the return value from the response
     * @param uriVariables      the variables to expand in the template
     * @return an arbitrary object, as returned by the {@link ResponseExtractor}
     */
    @Override
    @Nullable
    public <T> T execute(@Nonnull String url,
                         @Nonnull HttpMethod method,
                         RequestCallback requestCallback,
                         ResponseExtractor<T> responseExtractor,
                         @Nonnull Object... uriVariables) throws RestClientException {
        return inner.execute(url, method, requestCallback, responseExtractor, uriVariables);
    }

    /**
     * Execute the HTTP method to the given URI template, preparing the request with the
     * {@link RequestCallback}, and reading the response with a {@link ResponseExtractor}.
     * <p>URI Template variables are expanded using the given URI variables map.
     *
     * @param url               the URL
     * @param method            the HTTP method (GET, POST, etc)
     * @param requestCallback   object that prepares the request
     * @param responseExtractor object that extracts the return value from the response
     * @param uriVariables      the variables to expand in the template
     * @return an arbitrary object, as returned by the {@link ResponseExtractor}
     */
    @Override
    @Nullable
    public <T> T execute(@Nonnull String url,
                         @Nonnull HttpMethod method,
                         RequestCallback requestCallback,
                         ResponseExtractor<T> responseExtractor,
                         @Nonnull Map<String, ?> uriVariables) throws RestClientException {
        return inner.execute(url, method, requestCallback, responseExtractor, uriVariables);
    }

    /**
     * Execute the HTTP method to the given URL, preparing the request with the
     * {@link RequestCallback}, and reading the response with a {@link ResponseExtractor}.
     *
     * @param url               the URL
     * @param method            the HTTP method (GET, POST, etc)
     * @param requestCallback   object that prepares the request
     * @param responseExtractor object that extracts the return value from the response
     * @return an arbitrary object, as returned by the {@link ResponseExtractor}
     */
    @Override
    @Nullable
    public <T> T execute(@Nonnull URI url,
                         @Nonnull HttpMethod method,
                         RequestCallback requestCallback,
                         ResponseExtractor<T> responseExtractor) throws RestClientException {
        return inner.execute(url, method, requestCallback, responseExtractor);
    }
}

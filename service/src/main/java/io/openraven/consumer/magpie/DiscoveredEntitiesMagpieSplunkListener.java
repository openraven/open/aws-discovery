/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.magpie;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.openraven.consumer.SplunkOperations;
import io.openraven.consumer.properties.AwsAssetsConsumerProperties;
import io.openraven.consumer.properties.GcpAssetsConsumerProperties;
import io.openraven.consumer.properties.GenericAssetsConsumerProperties;
import io.openraven.data.shared.GenericAsset;
import io.openraven.magpie.api.MagpieEnvelope;
import io.openraven.magpie.data.Resource;
import io.openraven.magpie.data.aws.AWSResource;
import io.openraven.magpie.data.gcp.GCPResource;
import io.sentry.Sentry;
import io.sentry.SentryEvent;
import io.sentry.SentryLevel;
import io.sentry.protocol.Message;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.hibernate.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
@Profile("splunk_consumer")
public class DiscoveredEntitiesMagpieSplunkListener {

    public static final String AWS_SPLUNK_CONSUMER_CONFIG_PROP = AwsAssetsConsumerProperties.CONFIG_PREFIX + ".splunk-consumer";
    public static final String GCP_SPLUNK_CONSUMER_CONFIG_PROP = GcpAssetsConsumerProperties.CONFIG_PREFIX + ".splunk-consumer";
    public static final String GENERIC_ASSET_CONSUMER_CONFIG_PROP = GenericAssetsConsumerProperties.CONFIG_PREFIX + ".splunk-consumer";
    private static final Logger LOG = LoggerFactory.getLogger(DiscoveredEntitiesMagpieSplunkListener.class);
    private static final String NULL_ENTITY_ID = "unknown";
    private final SplunkOperations splunkOperations;
    private final ApplicationContext applicationContext;
    private final ObjectMapper objectMapper;

    public DiscoveredEntitiesMagpieSplunkListener(SplunkOperations splunkOperations, ApplicationContext applicationContext, ObjectMapper objectMapper) {
        this.splunkOperations = splunkOperations;
        this.applicationContext = applicationContext;
        this.objectMapper = objectMapper.findAndRegisterModules();

    }

    @KafkaListener(
            id = "discoveredEntitiesListener",
            groupId = "${" + AWS_SPLUNK_CONSUMER_CONFIG_PROP + ".group}",
            topics = {"${" + AWS_SPLUNK_CONSUMER_CONFIG_PROP + ".topic}", "${" + GCP_SPLUNK_CONSUMER_CONFIG_PROP + ".topic}"}
    )
    public void onAwsOrGcpMessages(final List<ConsumerRecord<Long, String>> records) {
        records.forEach(this::onAwsOrGcpMessage);
    }

    public void onAwsOrGcpMessage(final ConsumerRecord<Long, String> record) {
        String recordValue = record.value();
        final MagpieEnvelope envelope;
        try {
            envelope = objectMapper.reader().forType(MagpieEnvelope.class).readValue(recordValue);
        } catch (Exception e) {
            LOG.error("Unable to deserialize record value: {}", recordValue, e);
            return;
        }
        listenerHelper(
                recordValue,
                envelope,
                (objectNode) -> {
                    var resource = objectMapper.treeToValue(objectNode, Resource.class);
                    final boolean saveOK;
                    if (resource instanceof AWSResource sResource) {
                        saveOK = splunkOperations.save(sResource);
                    } else if (resource instanceof GCPResource pResource) {
                        saveOK = splunkOperations.save(pResource);
                    } else {
                        saveOK = false;
                    }
                    return saveOK;
                }
        );
    }

    @KafkaListener(
            id = "discoveredGenericEntitiesListener",
            groupId = "${" + GENERIC_ASSET_CONSUMER_CONFIG_PROP + ".group}",
            topics = {"${" + GENERIC_ASSET_CONSUMER_CONFIG_PROP + ".topic}"}
    )
    public void onGenericAssetMessages(final List<ConsumerRecord<Long, String>> records) {
        records.forEach(this::onGenericAssetMessage);
    }

    public void onGenericAssetMessage(final ConsumerRecord<Long, String> record) {
        String recordValue = record.value();
        final MagpieEnvelope envelope;
        try {
            envelope = objectMapper.reader().forType(MagpieEnvelope.class).readValue(recordValue);
        } catch (Exception e) {
            LOG.error("Unable to deserialize record value: {}", recordValue, e);
            return;
        }
        listenerHelper(
                recordValue,
                envelope,
                (objectNode) -> {
                    var resource = objectMapper.treeToValue(objectNode, GenericAsset.class);
                    return splunkOperations.save(resource);
                }
        );
    }

    public void sendErrorToSentry(MagpieEnvelope dataEnvelope, String entityId, Throwable ex) {
        var sentryEvent = new SentryEvent(ex);
        if (dataEnvelope != null) {
            final String resourceType = dataEnvelope.getContents().get("resourceType").asText();
            var message = new Message();
            message.setMessage("Exception while saving record");
            sentryEvent.setMessage(message);
            sentryEvent.setFingerprints(List.of(resourceType, ex.getMessage()));
            sentryEvent.setExtra("ResourceType", resourceType);
        }
        sentryEvent.setLevel(SentryLevel.WARNING);
        sentryEvent.setExtra("EntityId", entityId);
        Sentry.captureEvent(sentryEvent);
    }

    private void listenerHelper(
            String recordValue,
            MagpieEnvelope envelope,
            SplunkSaver handler
    ) {
        String entityId = NULL_ENTITY_ID;
        try {
            LOG.debug("resource:{}", envelope.getContents());
            entityId = envelope.getContents().get("resourceId").asText();
            final String resourceType = envelope.getContents().get("resourceType").asText();
            final String sessionId = envelope.getSession().getId();
            LOG.trace("Received Record of type {} in discovery session {}", resourceType, sessionId);

            final boolean saveOK = handler.apply(envelope.getContents());
            // send to Splunk
            if (saveOK) {
                LOG.debug("Queued resource id {} in discovery session {} for saving to Splunk", entityId, sessionId);
            } else {
                LOG.error("Was unable to queue resource with id {} in discovery session {} for saving to Splunk", entityId, sessionId);
                var sentryEvent = new SentryEvent();
                var message = new Message();
                message.setMessage("Unable to queue record for saving to Splunk");
                sentryEvent.setMessage(message);
                sentryEvent.setLevel(SentryLevel.WARNING);
                sentryEvent.setFingerprints(List.of(resourceType));
                sentryEvent.setExtra("EntityId", entityId);
                Sentry.captureEvent(sentryEvent);
                //sendToDeadLetterQueue(recordValue);
            }
        } catch (MappingException mappingException) {
            LOG.warn("Encountered mapping exception for unknown mapping {}", mappingException.getMessage());
            var sentryEvent = new SentryEvent(mappingException);
            var message = new Message();
            message.setMessage("Encountered mapping exception for unknown mapping " + mappingException.getMessage());
            sentryEvent.setMessage(message);
            sentryEvent.setLevel(SentryLevel.WARNING);
            sentryEvent.setExtra("RecordValue", recordValue);
            Sentry.captureEvent(sentryEvent);
        } catch (OutOfMemoryError ex) {
            try {
                sendErrorToSentry(envelope, entityId, ex);
            } catch (Throwable t) {
                LOG.error("Failed to send telemetry on OutOfMemoryError", t);
            }
            LOG.error("Exception attempting to save resource with id {} in discovery session {}", entityId, envelope.getSession().getId(), ex);

            System.exit(SpringApplication.exit(applicationContext));

        } catch (IOException e) {
            LOG.warn("Bogus Envelope JSON: '{}'", recordValue, e);
            var sentryEvent = new SentryEvent(e);
            var message = new Message();
            message.setMessage("Bogus Envelope JSON to save");
            sentryEvent.setMessage(message);
            sentryEvent.setLevel(SentryLevel.WARNING);
            sentryEvent.setFingerprints(List.of(recordValue));
            sentryEvent.setExtra("RecordValue", recordValue);
            Sentry.captureEvent(sentryEvent);
            //sendToDeadLetterQueue(recordValue);
        } catch (Exception ex) {
            LOG.debug("Discovery exception: {}", ExceptionUtils.getStackTrace(ex));
            LOG.error("Exception attempting to save resource with id {} in discovery session {}", entityId, envelope == null ? "unknown session" : envelope.getSession(), ex);
            sendErrorToSentry(envelope, entityId, ex);
            //sendToDeadLetterQueue(recordValue);
        }
    }

    @FunctionalInterface
    public interface SplunkSaver {
        Boolean apply(ObjectNode node) throws IOException;
    }
}

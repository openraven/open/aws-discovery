/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.magpie;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.openraven.consumer.properties.AwsAssetsConsumerProperties;
import io.openraven.consumer.properties.GcpAssetsConsumerProperties;
import io.openraven.magpie.api.MagpieEnvelope;
import io.openraven.magpie.data.Resource;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Profile("consumer")
@Component
public class MagpieConsumer implements HealthIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MagpieConsumer.class);
    private final ObjectMapper objectMapper;
    private final Counter savedObjectsCounter;
    private final Timer savedObjectsTimer;
    private final Counter savedObjectsErrorCounter;

    private final EntityManager entityManager;
    AtomicBoolean lastWriteSuccessful = new AtomicBoolean(true);


    public MagpieConsumer(ObjectMapper objectMapper,
                          @NotNull MeterRegistry meterRegistry,
                          @Qualifier("awsDiscoveryEntityManager") EntityManager awsDiscoveryEntityManager) {
        this.objectMapper = objectMapper;
        this.savedObjectsCounter = meterRegistry.counter("magpie_consumer.saved_objects.count");
        this.savedObjectsTimer = meterRegistry.timer("magpie_consumer.saved_objects.time");
        this.savedObjectsErrorCounter = meterRegistry.counter("magpie_consumer.saved_objects.error.count");
        this.entityManager = awsDiscoveryEntityManager;
    }

    @KafkaListener(id = "discoveredEntitiesListener",
            groupId = "${" + AwsAssetsConsumerProperties.CONFIG_PREFIX + ".consumer.group}",
            topics = {"${" + AwsAssetsConsumerProperties.CONFIG_PREFIX + ".consumer.topic}", "${" + GcpAssetsConsumerProperties.CONFIG_PREFIX + ".consumer.topic}"})
    public void onMessage(final List<ConsumerRecord<Long, String>> records) {

        List<Resource> assets = new ArrayList<>();
        records.forEach(record -> {
            try {
                MagpieEnvelope envelope = this.objectMapper.reader().forType(MagpieEnvelope.class).readValue(record.value());
                Resource asset = objectMapper.treeToValue(envelope.getContents(), Resource.class);
                assets.add(asset);
            } catch (JsonProcessingException e) {
                LOGGER.warn("Failed to create asset from consumed record {}", record.value(), e);
                savedObjectsErrorCounter.increment();
            }
        });
        savedObjectsTimer.wrap(() -> upsert(assets)).run();

        savedObjectsCounter.increment(assets.size());

    }

    public void upsert(List<Resource> resources) {
        try {
            entityManager.getTransaction().begin();

            resources.forEach(entityManager::merge);

            entityManager.flush();
            entityManager.getTransaction().commit();
            entityManager.clear();
            lastWriteSuccessful.set(true);
        } catch (Throwable e) {
            lastWriteSuccessful.set(false);
            LOGGER.error("Rolling back transaction failed due to: " + e.getMessage());
            LOGGER.debug("Details", e);
            try {
                entityManager.getTransaction().rollback();
            } catch (Throwable innerException) {
                LOGGER.error("Rollback failed: {}", innerException.getMessage());
            }
            throw e;
        }
    }

    @Override
    public Health health() {
        return lastWriteSuccessful.get() ? Health.up().build() : Health.down().build();
    }
}

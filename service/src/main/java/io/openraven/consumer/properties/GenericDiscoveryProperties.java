/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@ConfigurationProperties(prefix = GenericDiscoveryProperties.CONFIG_PREFIX)
@RefreshScope
public class GenericDiscoveryProperties {
    public static final String CONFIG_PREFIX = "openraven.app.v1.generic-discovery";

    private BatchSize batchSize;

    public BatchSize getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(BatchSize batchSize) {
        this.batchSize = batchSize;
    }

    public static class BatchSize {
        private int sharedDrive;
        private int myDrive;
        private int group;
        private int groupMember;

        public int getSharedDrive() {
            return sharedDrive;
        }

        public void setSharedDrive(int sharedDrive) {
            this.sharedDrive = sharedDrive;
        }

        public int getMyDrive() {
            return myDrive;
        }

        public void setMyDrive(int myDrive) {
            this.myDrive = myDrive;
        }

        public int getGroup() {
            return group;
        }

        public void setGroup(int group) {
            this.group = group;
        }

        public int getGroupMember() {
            return groupMember;
        }

        public void setGroupMember(int groupMember) {
            this.groupMember = groupMember;
        }
    }
}

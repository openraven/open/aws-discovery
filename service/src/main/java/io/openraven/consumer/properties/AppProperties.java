/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

package io.openraven.consumer.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@ConfigurationProperties(prefix = AppProperties.CONFIG_PREFIX)
@RefreshScope
public class AppProperties {
    public static final String CONFIG_PREFIX = "openraven.app.v1";

    private boolean eniDiscovery;

    public boolean isEniDiscovery() {
        return eniDiscovery;
    }

    public void setEniDiscovery(boolean eniDiscovery) {
        this.eniDiscovery = eniDiscovery;
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

package io.openraven.consumer.properties;

public abstract class ConsumerProperties {
    private Consumer consumer = new Consumer();

    private DeadLetter deadLetter = new DeadLetter();

    private SplunkConsumer splunkConsumer = new SplunkConsumer();

    public ConsumerProperties() {
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(final Consumer consumer) {
        this.consumer = consumer;
    }

    public DeadLetter getDeadLetter() {
        return deadLetter;
    }

    public void setDeadLetter(final DeadLetter deadLetter) {
        this.deadLetter = deadLetter;
    }

    public SplunkConsumer getSplunkConsumer() {
        return splunkConsumer;
    }

    public void setSplunkConsumer(final SplunkConsumer splunkConsumer) {
        this.splunkConsumer = splunkConsumer;
    }


    /**
     * Values related to how one handles dead letter messages.
     */
    public static class DeadLetter {

        private String topic;

        public String getTopic() {
            return topic;
        }

        public void setTopic(final String topic) {
            this.topic = topic;
        }

    }

    /**
     * OpenRaven specific configuration of Kafka consumers.
     */
    public static class Consumer {
        private String group;

        private String topic;

        private int concurrency;

        private int persistenceBatchSize;

        public String getGroup() {
            return group;
        }

        public void setGroup(final String group) {
            this.group = group;
        }

        public String getTopic() {
            return topic;
        }

        public void setTopic(final String topic) {
            this.topic = topic;
        }

        public int getConcurrency() {
            return concurrency;
        }

        /**
         * The number of concurrent {@code S3BucktetObject} discovery threads that run.
         */
        public void setConcurrency(int concurrency) {
            this.concurrency = concurrency;
        }

        public int getPersistenceBatchSize() {
            return persistenceBatchSize;
        }

        /**
         * Sets how many messages are buffered before a flush happens.
         */
        public void setPersistenceBatchSize(int persistenceBatchSize) {
            this.persistenceBatchSize = persistenceBatchSize;
        }
    }

    /**
     * OpenRaven specific configuration of Kafka consumers.
     */
    public static class SplunkConsumer {

        private String group;

        private String topic;

        private int concurrency;

        private int persistenceBatchSize;

        public String getGroup() {
            return group;
        }

        public void setGroup(final String group) {
            this.group = group;
        }

        public String getTopic() {
            return topic;
        }

        public void setTopic(final String topic) {
            this.topic = topic;
        }

        public int getConcurrency() {
            return concurrency;
        }

        /**
         * The number of concurrent {@code S3BucktetObject} discovery threads that run.
         */
        public void setConcurrency(int concurrency) {
            this.concurrency = concurrency;
        }

        public int getPersistenceBatchSize() {
            return persistenceBatchSize;
        }

        /**
         * Sets how many messages are buffered before a flush happens
         */
        public void setPersistenceBatchSize(int persistenceBatchSize) {
            this.persistenceBatchSize = persistenceBatchSize;
        }

    }
}

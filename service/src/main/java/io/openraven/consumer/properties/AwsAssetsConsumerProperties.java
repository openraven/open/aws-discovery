/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Primary;

@ConfigurationProperties(prefix = AwsAssetsConsumerProperties.CONFIG_PREFIX)
@RefreshScope
/*
 We mark this with primary because the "splunk_consumer" activate SplunkOperations which
 has weird injection conflicts with AwsAssetsConsumerProperties
*/
@Primary
public class AwsAssetsConsumerProperties extends ConsumerProperties {
    public static final String CONFIG_PREFIX = "openraven.app.v1.kafka";
}

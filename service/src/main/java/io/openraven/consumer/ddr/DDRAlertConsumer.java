/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.ddr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Profile("ddr_splunk_alert_consumer")
public class DDRAlertConsumer extends DDRSplunkBaseConsumer<AlertMessage> {
    public DDRAlertConsumer(SplunkDDROperations splunkDDROperations, ApplicationContext applicationContext, ObjectMapper objectMapper) {
        super(splunkDDROperations, applicationContext, objectMapper);
    }

    @KafkaListener(
            id = "alertMessageListener",
            groupId = "${" + DDR_SPLUNK_CONSUMER_CONFIG_PROP + ".group}",
            topics = {"ddr-alerts"}
    )
    public void onDDRMessage(String record) {
        onMessage(record);
    }

    @Override
    AlertMessage messageToObject(String message) throws JsonProcessingException {
        return objectMapper.readValue(message, AlertMessage.class);
    }
}

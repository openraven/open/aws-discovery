/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

package io.openraven.consumer.ddr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.openraven.consumer.properties.DDRConsumerProperties;
import io.sentry.Sentry;
import io.sentry.SentryEvent;
import io.sentry.SentryLevel;
import io.sentry.protocol.Message;
import org.hibernate.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.List;

abstract public class DDRSplunkBaseConsumer<T extends DDRBaseMessage> {

    public static final String DDR_SPLUNK_CONSUMER_CONFIG_PROP = DDRConsumerProperties.CONFIG_PREFIX + ".splunk-consumer";
    private static final Logger LOG = LoggerFactory.getLogger(DDRSplunkBaseConsumer.class);
    protected final ObjectMapper objectMapper;
    private final SplunkDDROperations splunkDDROperations;
    private final ApplicationContext applicationContext;

    protected DDRSplunkBaseConsumer(SplunkDDROperations splunkDDROperations, ApplicationContext applicationContext, ObjectMapper objectMapper) {
        this.splunkDDROperations = splunkDDROperations;
        this.applicationContext = applicationContext;
        this.objectMapper = objectMapper.findAndRegisterModules();
    }

    abstract T messageToObject(String message) throws JsonProcessingException;

    public void onMessage(final String record) {
        LOG.trace("Received record: {}", record);
        try {
            LOG.debug("recordValue:{}", record);

            final T msg = messageToObject(record);
            final var messageId = msg.id();
            final var sourceType = SplunkDDROperations.SOURCETYPE_PREFIX + msg.sourceType();

            final boolean saveOK = splunkDDROperations.saveDdr(msg, sourceType);

            if (saveOK) {
                LOG.debug("Queued DDR message with id={} for saving to Splunk", messageId);
            } else {
                LOG.error("Unable to queue DDR message with id {} for saving to Splunk", messageId);
                var sentryEvent = new SentryEvent();
                var message = new Message();
                message.setMessage("Unable to queue record for saving to Splunk");
                sentryEvent.setMessage(message);
                sentryEvent.setLevel(SentryLevel.WARNING);
                sentryEvent.setFingerprints(List.of(messageId));
                Sentry.captureEvent(sentryEvent);
            }
        } catch (MappingException mappingException) {
            LOG.warn("Encountered mapping exception for unknown mapping {}", mappingException.getMessage());
            var sentryEvent = new SentryEvent(mappingException);
            var message = new Message();
            message.setMessage("Encountered mapping exception for unknown mapping " + mappingException.getMessage());
            sentryEvent.setMessage(message);
            sentryEvent.setLevel(SentryLevel.WARNING);
            sentryEvent.setExtra("RecordValue", record);
            Sentry.captureEvent(sentryEvent);
        } catch (OutOfMemoryError ex) {
            try {
                Sentry.captureException(ex);
            } catch (Throwable t) {
                LOG.error("Failed to send telemetry on OutOfMemoryError", t);
            }
            LOG.error("Exception attempting to save resource", ex);

            System.exit(SpringApplication.exit(applicationContext));

        } catch (IOException e) {
            LOG.warn("Bogus Envelope JSON: '{}'", record, e);
            var sentryEvent = new SentryEvent(e);
            var message = new Message();
            message.setMessage("Bogus Envelope JSON to save");
            sentryEvent.setMessage(message);
            sentryEvent.setLevel(SentryLevel.WARNING);
            sentryEvent.setFingerprints(List.of(record));
            sentryEvent.setExtra("RecordValue", record);
            Sentry.captureEvent(sentryEvent);
        } catch (Exception ex) {
            LOG.info("DDR Consumer exception", ex);
            Sentry.captureException(ex);
        }
    }
}

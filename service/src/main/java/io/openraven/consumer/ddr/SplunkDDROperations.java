/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.ddr;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.openraven.magpie.data.utils.JacksonMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

@Component
@Profile("ddr_splunk_consumer")
public class SplunkDDROperations {
    // Required by our Splunk OEM contract
    public static final String SOURCETYPE_PREFIX = "open_raven_";
    private static final Logger LOG = getLogger(SplunkDDROperations.class);
    private static final ObjectMapper MAPPER = JacksonMapper.getJacksonMapper();

    static {
        MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    private final RestOperations httpReq;
    private final Map<String, HttpHeaders> headers = new HashMap<>();
    private final String hecURL;


    public SplunkDDROperations(SplunkServiceDDRProperties splunkServiceDDRProperties, @Qualifier("insecureRestOperations") RestOperations httpReq) {

        this.httpReq = httpReq;

        // See https://docs.splunk.com/Documentation/Splunk/8.1.2/Data/HECExamples for examples

        var awsHeaders = new HttpHeaders();
        awsHeaders.set("Authorization", "Splunk " + splunkServiceDDRProperties.getAwsAuthToken());
        headers.put("aws", awsHeaders);

        var gcpHeaders = new HttpHeaders();
        gcpHeaders.set("Authorization", "Splunk " + splunkServiceDDRProperties.getGcpAuthToken());
        headers.put("gcp", gcpHeaders);

        this.hecURL = splunkServiceDDRProperties.getEventEndpointUrl().toString();
    }

    public boolean saveDdr(Object resource, String sourceType) {
        return postToSpunk(createDDRPayload(resource, sourceType), sourceType);
    }


    private JsonNode createDDRPayload(Object message, String sourceType) {
        JsonNode resourceNode = MAPPER.valueToTree(message);

        ObjectNode payload = MAPPER.createObjectNode();
        payload.put("sourcetype", sourceType);
        payload.set("event", resourceNode);
        LOG.debug("payload:{}", payload);
        LOG.debug("resourceNode:{}", resourceNode);

        return payload;
    }

    private boolean postToSpunk(JsonNode msg, String sourceType) {
        try {
            if ("open_raven_ddr_asset_coverage".equalsIgnoreCase(sourceType) || "open_raven_ddr_coverage".equalsIgnoreCase(sourceType)) {
                final var postTarget = msg.get("event").get("cloudProvider").asText();
                HttpEntity<String> postPayload = new HttpEntity<>(MAPPER.writeValueAsString(List.of(msg)), headers.get(postTarget));
                ResponseEntity<String> response = httpReq.postForEntity(hecURL, postPayload, String.class);
                LOG.trace("Posted event to splunk. cloud={}, msg={} to splunk", postTarget, msg);
                return response.getStatusCode() == HttpStatus.OK;
            } else {
                final var logPattern = "arn:aws";
                final var postTarget = msg.get("event").get("sourceLog").asText(logPattern).startsWith(logPattern) ? "aws" : "gcp";
                HttpEntity<String> postPayload = new HttpEntity<>(MAPPER.writeValueAsString(List.of(msg)), headers.get(postTarget));
                ResponseEntity<String> response = httpReq.postForEntity(hecURL, postPayload, String.class);
                LOG.trace("Posted event to splunk. cloud={}, msg={} to splunk", postTarget, msg);
                return response.getStatusCode() == HttpStatus.OK;
            }
        } catch (Exception ex) {
            LOG.error("Error posting to Splunk", ex);
        }

        return false;
    }
}

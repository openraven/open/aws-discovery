/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

package io.openraven.consumer.ddr;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventMessage {
    public JsonNode contents;
    public String eventType;

    public JsonNode get(String fieldName) {
        return contents.get(fieldName);
    }

    public String getString(String fieldName) {
        JsonNode field = contents.get(fieldName);
        if (field == null) {
            return null;
        } else {
            return field.textValue();
        }
    }

    @JsonIgnore
    public List<String> getResourceARNs() {
        if (get("resources") == null) {
            return new ArrayList<>();
        } else {
            List<String> ARNs = new LinkedList<>();
            for (JsonNode resource : get("resources")) {
                ARNs.add(resource.get("ARN").textValue());
            }
            return ARNs;
        }
    }

    @JsonIgnore
    public String getBucketName() {
        if (get("requestParameters") == null) {
            return "unknown";
        } else {
            JsonNode bucketName = get("requestParameters").get("bucketName");
            if (bucketName == null) {
                return "unknown";
            } else {
                return bucketName.textValue();
            }
        }
    }

    @JsonIgnore
    public String getRequestParameterUserName() {
        if (get("requestParameters") == null) {
            return "unknown";
        } else {
            JsonNode userName = get("requestParameters").get("userName");
            if (userName == null) {
                return "unknown";
            } else {
                return userName.textValue();
            }
        }
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.ddr;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.LinkedList;
import java.util.List;

public class AnnotationMessage extends DDRBaseMessage {

    public String logIdentifier;
    public String accountId;
    public boolean isLogging;
    public boolean isOrg;
    public String bucketName;
    public boolean canAccessBucket;
    public String noBucketAccessReason;
    public String lastAccessedBucket;
    public List<String> coverage = new LinkedList<>();
    public JsonNode config;
    public String messageType;
    public String cloudProvider;

    @Override
    public String id() {
        return logIdentifier;
    }


    @Override
    public String sourceType() {
        return "ddr_" + messageType.toLowerCase();
    }
}

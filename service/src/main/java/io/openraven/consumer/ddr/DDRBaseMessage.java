/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.ddr;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class DDRBaseMessage {

    @JsonIgnore
    abstract public String id();

    @JsonIgnore
    abstract public String sourceType();
}

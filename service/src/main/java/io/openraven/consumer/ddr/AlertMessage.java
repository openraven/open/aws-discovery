/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

package io.openraven.consumer.ddr;

import com.fasterxml.jackson.databind.JsonNode;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;

public class AlertMessage extends DDRBaseMessage {
    // Unique ID for each message
    public String id;
    public String sourceLog;
    public String alertRuleId;
    public String alertRuleName;
    public String alertRuleDescription;
    public AlertLevel alertLevel;
    public Instant alertGenerationTimestamp;
    public String text;
    public List<JsonNode> events;
    public HashMap<String, Object> alertParameters = new HashMap<>();

    @Override
    public String id() {
        return id;
    }

    @Override
    public String sourceType() {
        return "ddr_alert_" + alertRuleName.toLowerCase().replaceAll("\\s", "_");
    }

    public enum AlertLevel {
        INFO, LOW, MEDIUM, HIGH
    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

package io.openraven.consumer.ddr;

import com.fasterxml.jackson.databind.JsonNode;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;

public class StatsMessage extends DDRBaseMessage {
    public String id;
    public String sourceLog;
    public Instant statsGenerationTimestamp;
    public String text;
    public List<JsonNode> events;
    public HashMap<String, Object> statsParameters = new HashMap<>();

    @Override
    public String id() {
        return id;
    }

    @Override
    public String sourceType() {
        return "ddr_stats_general";
    }

    public enum AlertLevel {
        INFO, LOW, MEDIUM, HIGH
    }
}

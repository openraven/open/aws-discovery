/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven.consumer.ddr;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.net.URL;

@ConfigurationProperties(prefix = SplunkServiceDDRProperties.CONFIG_PREFIX)
@RefreshScope
public class SplunkServiceDDRProperties {
    public static final String CONFIG_PREFIX = "openraven.app.v1.services.ddr";

    private URL eventEndpointUrl;

    private String awsAuthToken;
    private String gcpAuthToken;

    public String getGcpAuthToken() {
        return gcpAuthToken;
    }

    public void setGcpAuthToken(String gcpAuthToken) {
        this.gcpAuthToken = gcpAuthToken;
    }

    public URL getEventEndpointUrl() {
        return eventEndpointUrl;
    }

    public void setEventEndpointUrl(URL eventEndpointUrl) {
        this.eventEndpointUrl = eventEndpointUrl;
    }

    public String getAwsAuthToken() {
        return awsAuthToken;
    }

    public void setAwsAuthToken(String awsAuthToken) {
        this.awsAuthToken = awsAuthToken;
    }

}

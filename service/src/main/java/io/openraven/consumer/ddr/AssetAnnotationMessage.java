package io.openraven.consumer.ddr;

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */

import java.util.ArrayList;

public class AssetAnnotationMessage extends DDRBaseMessage {

    public String assetId;
    public ArrayList<AssetLevelAnnotation> assetLevelAnnotation;
    public String messageType;
    public String cloudProvider;

    @Override
    public String id() {
        return assetId;
    }

    @Override
    public String sourceType() {
        return "ddr_" + messageType.toLowerCase();
    }

    public static class AssetLevelAnnotation {

        public String logIdentifier;
        public ArrayList<String> managementCoverage;
        public ArrayList<String> dataCoverage;
        public Boolean isLogging;
        public Boolean canAccessBucket;
        public String lastAccessedBucket;

    }
}

/*
 * ***********************************************************
 * Copyright, 2020, Open Raven Inc.
 * APACHE LICENSE, VERSION 2.0
 * https://www.openraven.com/legal/apache-2-license
 * *********************************************************
 */
package io.openraven;

import io.openraven.consumer.ddr.SplunkServiceDDRProperties;
import io.openraven.consumer.properties.AppProperties;
import io.openraven.consumer.properties.AwsAssetsConsumerProperties;
import io.openraven.consumer.properties.AzureAssetsConsumerProperties;
import io.openraven.consumer.properties.ConsumerProperties;
import io.openraven.consumer.properties.DDRConsumerProperties;
import io.openraven.consumer.properties.ElasticSearchServiceProperties;
import io.openraven.consumer.properties.GcpAssetsConsumerProperties;
import io.openraven.consumer.properties.GenericAssetsConsumerProperties;
import io.openraven.consumer.properties.GenericDiscoveryProperties;
import io.openraven.data.properties.SplunkServiceMagpieProperties;
import io.openraven.magpie.plugins.persist.AssetsRepo;
import io.openraven.magpie.plugins.persist.PersistConfig;
import io.openraven.magpie.plugins.persist.config.PostgresPersistenceProvider;
import io.openraven.magpie.plugins.persist.impl.HibernateAssetsRepoImpl;
import io.openraven.producer.properties.CrossAccountAccessConfig;
import io.openraven.producer.properties.CrossAccountProperties;
import io.openraven.producer.properties.DiscoveryProperties;
import io.openraven.producer.properties.ObjectDiscoveryProperties;
import io.openraven.producer.properties.SchedulingProperties;
import io.openraven.properties.CrossAccountServiceProperties;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.ssl.SslBundles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.util.ObjectUtils.isEmpty;

@SpringBootApplication
public class App {

    public static final int JDBC_PROTOCOL_LENGTH = 5;

    public static void main(String[] args) {
        new SpringApplicationBuilder(App.class).bannerMode(Banner.Mode.OFF)
                .web(WebApplicationType.SERVLET).run(args);
    }

    @Configuration
    @EnableConfigurationProperties({
            AwsAssetsConsumerProperties.class,
            GcpAssetsConsumerProperties.class,
            CrossAccountProperties.class,
            DiscoveryProperties.class,
            ElasticSearchServiceProperties.class,
            ObjectDiscoveryProperties.class,
            CrossAccountAccessConfig.class,
            SchedulingProperties.class,
            SplunkServiceMagpieProperties.class,
            AppProperties.class,
            GenericAssetsConsumerProperties.class,
            DDRConsumerProperties.class,
            GenericDiscoveryProperties.class,
            SplunkServiceDDRProperties.class,
            AzureAssetsConsumerProperties.class,
            CrossAccountServiceProperties.class,
    })
    @EnableAutoConfiguration(exclude = {CassandraDataAutoConfiguration.class,
            CassandraAutoConfiguration.class})
    @ComponentScan(basePackages = {"io.openraven"})
    @EnableKafka
    @EnableScheduling
    @EnableJpaRepositories()
    public static class Config {

        public static Map<String, Object> constructMagpieKafkaProperties(ConsumerProperties.Consumer consumer, KafkaProperties kafkaProperties) {
            var kafkaPropertiesMap = kafkaProperties.getProperties();
            // Add openraven.app.v1.magpie.kafka with correct topic (that is not AWS_DISCOVERED_ENTITIES)
            // Interrogate the magpieOpenravenProperties for its kafka details
            var magpieKafkaPropertiesMap = new HashMap<>(Map.<String, Object>of(
                    "topic", consumer.getTopic()
            ));
            magpieKafkaPropertiesMap.put("bootstrap.servers", kafkaProperties.getBootstrapServers());
            magpieKafkaPropertiesMap.putAll(kafkaPropertiesMap);
            return magpieKafkaPropertiesMap;
        }

        @Bean
        @Profile("producer")
        public ProducerFactory<Long, String> producerFactory(KafkaProperties properties, SslBundles bundles)
                throws Exception {
            final KafkaProperties.Producer kppProps = properties.getProducer();
            @SuppressWarnings("unchecked") final Class<Serializer<Long>> keySerClass = (Class<Serializer<Long>>) kppProps
                    .getKeySerializer();
            @SuppressWarnings("unchecked") final Class<Serializer<String>> valueSerClass = (Class<Serializer<String>>) kppProps
                    .getValueSerializer();
            final Serializer<Long> keySer = keySerClass.getDeclaredConstructor().newInstance();
            final Serializer<String> valueSer = valueSerClass.getDeclaredConstructor().newInstance();
            final DefaultKafkaProducerFactory<Long, String> result = new DefaultKafkaProducerFactory<>(
                    properties.buildProducerProperties(bundles));
            result.setKeySerializer(keySer);
            result.setValueSerializer(valueSer);
            return result;
        }

        @Bean
        public KafkaTemplate<Long, String> deadLetterOperations(ProducerFactory<Long, String> producer,
                                                                KafkaProperties kafkaProperties) {
            final KafkaTemplate<Long, String> result = new KafkaTemplate<>(producer);
            final String defaultTopic = kafkaProperties.getTemplate().getDefaultTopic();
            if (!isEmpty(defaultTopic)) {
                result.setDefaultTopic(defaultTopic);
            }
            return result;
        }

        /**
         * Be careful, this method/Bean name is magick, see:
         * <tt>org/springframework/boot/autoconfigure/kafka/KafkaAnnotationDrivenConfiguration.java:85</tt>
         */
        @Bean
        @Profile("consumer")
        public ConcurrentKafkaListenerContainerFactory<Long, String> kafkaListenerContainerFactory(
                ConsumerFactory<Long, String> consumerFactory) {
            final ConcurrentKafkaListenerContainerFactory<Long, String> result = new ConcurrentKafkaListenerContainerFactory<>();
            result.setConsumerFactory(consumerFactory);
            result.setBatchListener(true);
            return result;
        }

        @Bean
        @Profile("consumer")
        @SuppressWarnings("unchecked")
        public ConsumerFactory<Long, String> consumerFactory(KafkaProperties properties, SslBundles bundles)
                throws Exception {
            final KafkaProperties.Consumer kpcProps = properties.getConsumer();
            final Class<Deserializer<Long>> keyDeserClass = (Class<Deserializer<Long>>) kpcProps
                    .getKeyDeserializer();
            final Class<Deserializer<String>> valueDeserClass = (Class<Deserializer<String>>) kpcProps
                    .getValueDeserializer();
            final Deserializer<Long> keyDeser = keyDeserClass.getDeclaredConstructor().newInstance();
            final Deserializer<String> valueDeser = valueDeserClass.getDeclaredConstructor()
                    .newInstance();
            final DefaultKafkaConsumerFactory<Long, String> result = new DefaultKafkaConsumerFactory<>(
                    properties.buildConsumerProperties(bundles));
            result.setKeyDeserializer(keyDeser);
            result.setValueDeserializer(valueDeser);
            return result;
        }

        @Bean
        public ConversionService conversionService() {
            return new DefaultConversionService();
        }

        @Bean
        @Profile("consumer")
        public AssetsRepo assetsRepo(@NotNull PersistConfig persistConfig) {
            return new HibernateAssetsRepoImpl(persistConfig);
        }

        @Bean
        @Profile("consumer")
        public EntityManager awsDiscoveryEntityManager(PersistConfig persistConfig) {
            return PostgresPersistenceProvider.getEntityManager(persistConfig);
        }

        @Bean
        @Profile("consumer")
        public PersistConfig persistConfig(@NotNull DataSourceProperties dataSourceProperties) {
            var persistConfig = new PersistConfig();
            persistConfig.setMigrateDB(false);
            persistConfig.setPassword(dataSourceProperties.getPassword());
            persistConfig.setSchema("public");
            persistConfig.setDdlAuto("none");

            try {
                final URI dataSourcePropertiesURI = new URI(dataSourceProperties.getUrl().substring(JDBC_PROTOCOL_LENGTH));
                persistConfig.setDatabaseName(dataSourcePropertiesURI.getPath().substring(1));
                persistConfig.setHostname(dataSourcePropertiesURI.getHost());
                int port = dataSourcePropertiesURI.getPort();
                if (port == -1) {
                    port = 5432;
                }
                persistConfig.setPort(Integer.toString(port));
                persistConfig.setUser(dataSourceProperties.getUsername());
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
            return persistConfig;
        }
    }
}

-- drop table aws cascade ;
create table aws
(
    documentid                 text primary key not null,

    arn                        text,
    resourcename               text,
    resourceid                 text,
    resourcetype               text,
    awsregion                  text,
    awsaccountid               text,
    creatediso                 timestamptz,
    updatediso                 timestamptz,
    discoverysessionid         text,
    tags                       jsonb,
    configuration              jsonb,
    supplementaryConfiguration jsonb,
    discoverymeta              jsonb,

    -- awss3bucket
    dataanalysis               jsonb,
    maxSizeInBytes             int8,
    sizeInBytes                int8
);
create index aws_resource_type ON aws (resourcetype);

-- the '[ ]' business is to keep the grep from matching itself
-- git grep -hEo 'TABLE_NAME[ ]=."[^;]*' | sed 's/^[^"]*"\([^"]*\)"/CREATE TABLE \1 () INHERITS (aws);/' | sort
CREATE TABLE awsaccount () INHERITS (aws);
CREATE TABLE awsathenadatacatalog () INHERITS (aws);
CREATE TABLE awsbackupbackupvault () INHERITS (aws);
CREATE TABLE awsbatchcomputeenvironment () INHERITS (aws);
CREATE TABLE awsbatchjobdefinition () INHERITS (aws);
CREATE TABLE awsbatchjobqueue () INHERITS (aws);
CREATE TABLE awscassandrakeyspace () INHERITS (aws);
CREATE TABLE awscloudfrontdistribution () INHERITS (aws);
CREATE TABLE awscloudsearchdomain () INHERITS (aws);
CREATE TABLE awscredentialreport () INHERITS (aws);
CREATE TABLE awsdynamodbglobaltable () INHERITS (aws);
CREATE TABLE awsdynamodbtable () INHERITS (aws);
CREATE TABLE awsec2eip () INHERITS (aws);
CREATE TABLE awsec2instance () INHERITS (aws);
CREATE TABLE awsec2networkacl () INHERITS (aws);
CREATE TABLE awsec2networkinterface () INHERITS (aws);
CREATE TABLE awsec2securitygroup () INHERITS (aws);
CREATE TABLE awsec2volume () INHERITS (aws);
CREATE TABLE awsec2vpc () INHERITS (aws);
CREATE TABLE awsec2vpcpeeringconnection () INHERITS (aws);
CREATE TABLE awsecscluster () INHERITS (aws);
CREATE TABLE awsefsfilesystem () INHERITS (aws);
CREATE TABLE awsekscluster () INHERITS (aws);
CREATE TABLE awselasticachecluster () INHERITS (aws);
CREATE TABLE awselasticbeanstalk () INHERITS (aws);
CREATE TABLE awselasticloadbalancingloadbalancer () INHERITS (aws);
CREATE TABLE awselasticloadbalancingv2loadbalancer () INHERITS (aws);
CREATE TABLE awselasticsearchdomain () INHERITS (aws);
CREATE TABLE awsemrcluster () INHERITS (aws);
CREATE TABLE awsfsxfilesystem () INHERITS (aws);
CREATE TABLE awsglaciervault () INHERITS (aws);
CREATE TABLE awsiamgroup () INHERITS (aws);
CREATE TABLE awsiampolicy () INHERITS (aws);
CREATE TABLE awsiamrole () INHERITS (aws);
CREATE TABLE awsiamuser () INHERITS (aws);
CREATE TABLE awskmskey () INHERITS (aws);
CREATE TABLE awslakeformationresource () INHERITS (aws);
CREATE TABLE awslambdafunction () INHERITS (aws);
CREATE TABLE awslightsaildatabase () INHERITS (aws);
CREATE TABLE awslightsailinstance () INHERITS (aws);
CREATE TABLE awslightsailloadbalancer () INHERITS (aws);
CREATE TABLE awsneptunecluster () INHERITS (aws);
CREATE TABLE awsneptuneinstance () INHERITS (aws);
CREATE TABLE awsqldbledger () INHERITS (aws);
CREATE TABLE awsrdsdbinstance () INHERITS (aws);
CREATE TABLE awsredshiftcluster () INHERITS (aws);
CREATE TABLE awsregion () INHERITS (aws);
CREATE TABLE awsroute53hostedzone () INHERITS (aws);
CREATE TABLE awss3bucket () INHERITS (aws);
CREATE TABLE awss3bucketobject () INHERITS (aws);
CREATE TABLE awssecretsmanager () INHERITS (aws);
CREATE TABLE awsstoragegatewaygateway () INHERITS (aws);
CREATE TABLE orvnshadowaccount () INHERITS (aws);

/*
-- one cannot use "partitioned table" since the PG jdbc driver
-- does not return it as a "TABLE" causing JPA to :fu:

create table aws
(
    documentid                 text primary key not null,
    arn                        text,
    resourcename               text,
    resourceid                 text,
    resourcetype               text,
    awsregion                  text,
    awsaccountid               text,
    creatediso                 timestamptz,
    updatediso                 timestamptz,
    discoverysessionid         text,
    tags                       jsonb,
    configuration              jsonb,
    supplementaryConfiguration jsonb,
    discoverymeta              jsonb
) partition by list (resourcetype);
create table awsec2eip partition of aws for values in ('AWS::EC2::EIP');
create table awsregion partition of aws for values in ('AWS::Region');
create table awsec2vpc partition of aws for values in ('AWS::EC2::VPC');
create table awsec2securitygroup partition of aws for values in ('AWS::EC2::SecurityGroup');
create table awsec2networkinterface partition of aws for values in ('AWS::EC2::NetworkInterface');
create table awsec2instance partition of aws for values in ('AWS::EC2::Instance');
*/

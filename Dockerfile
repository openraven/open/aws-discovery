FROM registry.gitlab.com/openraven/open/orvn-java:17

# the path, relative to the repo root, of the service jar
ARG JAR_FILE
ADD ${JAR_FILE} /root/service.jar
RUN if [ ! -f   /root/service.jar ]; then \
      echo "Expected /root/service.jar to be a FILE" >&2 ;\
      exit 1 ;\
    fi
